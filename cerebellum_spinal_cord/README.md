# Cerebellum SpinalCord Controller
Cerebellar-SNN integrated with a spinal cord model to control a musculoskeletal arm model in OpenSim. 
This is a nrp-core experiment that integrates a cerebellar SNN (using the EDLUT engine), a spinal cord model, a musculoskeletal arm model, and the configuration files needed. 
This work is a port tested in nrp-core version 1.4.0 and is based on a collaboration between the [Applied Computational Neuroscience UGR](http://acn.ugr.es/) group (Prof. Eduardo Ros, UGR) and the [Biorobotics Laboratory](https://www.epfl.ch/labs/biorob/) (Prof. Auke Ijspeert, EPFL) in the context of Work Package 3 of the [HBP](https://www.humanbrainproject.eu/en/).
For more information, check [here](https://doi.org/10.1101/2023.03.08.531839).

##  Preliminary Requirements to Run the Experiment
* A computer with a minimum of 8GB of RAM, a multicore CPU, and optionally, an NVIDIA GPU with CUDA support for GPU parallelization of the EDLUT simulation. 
* An OpenSim installation: https://github.com/opensim-org/opensim-core#on-ubuntu-using-unix-makefiles
* Install the following modules for spinal cord network: 
  * farms_pylog: https://gitlab.com/farmsim/farms_pylog
    * $ cd farms_pylog 
    * $ pip install -e .
  * $ pip install cython
  * farms_container: https://gitlab.com/farmsim/farms_container
    * $ cd farms_container 
    * $ pip install -e .
  * farms_network: https://gitlab.com/farmsim/farms_network
    * $ cd farms_network 
    * pip install -e .

## Demo experiment
To launch a demo simulation use one of the following commands:

    # launch a demo using CPU
    NRPCoreSim -c simulation_config.json
    
    # launch a demo using GPU
    NRPCoreSim -c simulation_config_cuda.json

The demo demonstrates how the integrated spino-cerebellar model controls a simulated musculoskeletal upper limb, enabling it to perform a motor task involving two degrees of freedom, specifically a flexion-extension trajectory. The spino-cerebellar model achieves fast convergence in motor learning, leading to improved task accuracy through simple cerebellar synaptic weight distributions.  


