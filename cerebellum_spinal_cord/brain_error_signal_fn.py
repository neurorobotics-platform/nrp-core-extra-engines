from nrp_core import *
from nrp_core.data.nrp_protobuf import *
from nrp_core.event_loop import *

from to_spikes import *

import math
import random

sampling_frequency = 500.0
joint_list = ['r_shoulder_elev', 'r_elbow_flexion']
error_pos_gain = [3.0, 3.0]
error_vel_gain = [1.0, 1.0]
delay = 0.05

e_min_neuron_index_pos = [80, 180]
e_max_neuron_index_pos = [129, 229]
e_min_neuron_index_neg = [130, 230]
e_max_neuron_index_neg = [179, 279]

min_value = [0.001, 0.001]
max_value = [0.08, 0.08]
max_spike_frequency = [10.0, 10.0]
min_spike_frequency = [1.0, 1.0]

time_step = 1.0 / sampling_frequency

num_filters_pos = [e_max_neuron_index_pos[0]-e_min_neuron_index_pos[0] + 1,e_max_neuron_index_pos[1]-e_min_neuron_index_pos[1] + 1]
num_filters_neg = [e_max_neuron_index_neg[0]-e_min_neuron_index_neg[0] + 1, e_max_neuron_index_neg[1]-e_min_neuron_index_neg[1] + 1]

overlapping_factor = [0.500000000001, 0.500000000001]
max_spike_frequency_list = [500.0, 500.0]

rbf_cp = spikesConverter(joint_list,[10,10],[10, 50],[19, 59],[0.09, 0.17], [0.87, 2.09], sampling_frequency,overlapping_factor,max_spike_frequency_list)
rbf_cv = spikesConverter(joint_list,[10,10],[30, 70],[39, 79],[-2.45 , -6],[2.45, 6], sampling_frequency,overlapping_factor,max_spike_frequency_list)
rbf_dp = spikesConverter(joint_list,[10,10],[0, 40],[9, 49],[0.09, 0.17], [0.87, 2.09],sampling_frequency,overlapping_factor,max_spike_frequency_list)
rbf_dv = spikesConverter(joint_list,[10,10],[20, 60],[29, 69],[-2.45 , -6],[2.45, 6],sampling_frequency,overlapping_factor,max_spike_frequency_list)


def generate_activity(input_signal_value, current_time, joint, new_sampling_period, delay_m,init_neuron,end_neuron):
    index = 0
    time_array = []
    neuron_index_array = []
    id_neuron = init_neuron[joint]
    step = (0.08-0.001)/49
    while(id_neuron <= end_neuron[joint]):

        norm_firing_rate = (math.tanh((input_signal_value-(0.001+step*index))/step)+1.0)*0.5
        sp_firing_rate = min_spike_frequency[joint] + norm_firing_rate * (max_spike_frequency[joint]-min_spike_frequency[joint])

        if(random.random() < sp_firing_rate*new_sampling_period):
            n_spikes = input_signal_value / (0.001+(step*index))
            if(n_spikes<1):
                n_spikes = 1
            if(n_spikes>6):
                n_spikes = 6
            for i in range(int(n_spikes)):
                time_array.append(current_time + i * 0.002 + delay_m)
                neuron_index_array.append(id_neuron)

        index += 1
        id_neuron = id_neuron + 1

    return neuron_index_array, time_array


@Clock("clock_in")
@ToEngine(keyword="spikes_datapack", address="/edlut")
@FromEngine(keyword='positions', address='/opensim/positions')
@FromEngine(keyword='velocities', address='/opensim/velocities')
@FromFunctionalNode('target_state', '/read_trajectory/target_joint_state')
@FunctionalNode(name="spikes_function", outputs=['spikes_datapack'])
def spikes_function( positions, velocities, target_state, clock_in):

    assert positions is not None
    assert velocities is not None
    assert target_state is not None

    current_time = float(clock_in/1e3)
    err_datapack = EdlutDataSpikesDataPack("spikes_datapack", "edlut")
    error = dict()

    global joint_list
    global error_pos_gain
    global error_vel_gain

    cposition = dict(positions.data)
    cvelocity = dict(velocities.data)

    for k,i in zip(cposition.keys(),range(len(joint_list)-1,-1,-1)):
        error[k] = error_pos_gain[i]*(target_state['positions'][k]-cposition[k]) + error_vel_gain[i]*(target_state['velocities'][k]-cvelocity[k])


    spike_group_neuron_index = []
    spike_group_time = []

    global e_min_neuron_index_pos
    global e_max_neuron_index_pos
    global e_min_neuron_index_neg
    global e_max_neuron_index_neg
    global delay
    global time_step

    # Compute full state error to spikes
    for k,i in zip(error.keys(),range(len(joint_list)-1,-1,-1)):
        if(error[k]>0):
            neuron_index_arr, spikes_time = generate_activity(error[k],current_time, i, time_step, delay,e_min_neuron_index_pos,e_max_neuron_index_pos)

        else:
            neuron_index_arr, spikes_time = generate_activity(-error[k],current_time, i, time_step, delay,e_min_neuron_index_neg,e_max_neuron_index_neg)

        spike_group_neuron_index.extend(neuron_index_arr)
        spike_group_time.extend(spikes_time)

    err_datapack.data.neuron_indexes.extend(spike_group_neuron_index)
    err_datapack.data.spikes_time.extend(spike_group_time)

    global rbf_cp
    global rbf_cv
    global rbf_dp
    global rbf_dv


    # Get full state in spikes
    rbf_cp.generate_activity(cposition, current_time, delay)
    rbf_cv.generate_activity(cvelocity, current_time, delay)
    rbf_dp.generate_activity(dict(target_state['positions']), current_time, delay)
    rbf_dv.generate_activity(dict(target_state['velocities']), current_time, delay)

    spike_group_neuron_index = []
    spike_group_time = []


    spike_group_neuron_index.extend(rbf_cp.neuron_index_array)
    spike_group_time.extend(rbf_cp.time_array)
    rbf_cp.neuron_index_array.clear()
    rbf_cp.time_array.clear()

    spike_group_neuron_index.extend(rbf_cv.neuron_index_array)
    spike_group_time.extend(rbf_cv.time_array)
    rbf_cv.neuron_index_array.clear()
    rbf_cv.time_array.clear()

    spike_group_neuron_index.extend(rbf_dp.neuron_index_array)
    spike_group_time.extend(rbf_dp.time_array)
    rbf_dp.neuron_index_array.clear()
    rbf_dp.time_array.clear()

    spike_group_neuron_index.extend(rbf_dv.neuron_index_array)
    spike_group_time.extend(rbf_dv.time_array)
    rbf_dv.neuron_index_array.clear()
    rbf_dv.time_array.clear()

    err_datapack.data.neuron_indexes.extend(spike_group_neuron_index)
    err_datapack.data.spikes_time.extend(spike_group_time)

    return [err_datapack]

