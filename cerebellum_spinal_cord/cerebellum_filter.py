import numpy as np
from copy import deepcopy

class CerebFilter:
    def __init__(self, max_past_buffer_size, min_future_samples,joint_list):
        self.joint_list = joint_list

        self.cereb_output_mean_agonist = np.zeros(len(joint_list))
        self.cereb_output_mean_antagonist = np.zeros(len(joint_list))

        self.max_past_buffer_size = max_past_buffer_size
        self.min_future_samples = min_future_samples

        self.future_cerebellar_torque_buffer = []
        self.past_cerebellar_torque_buffer = []

    def clean_cereb_buffer(self, end_time):

        mean_agonist = np.zeros(len(self.joint_list))
        mean_antagonist = np.zeros(len(self.joint_list))
        samples = np.zeros(len(self.joint_list))

        if(bool(self.future_cerebellar_torque_buffer)):
            top_value = self.future_cerebellar_torque_buffer[-1]
            while(bool(self.future_cerebellar_torque_buffer) and top_value['stamp']<end_time):
                self.insert_and_order_cereb_buffer_past(top_value)
                if(len(self.past_cerebellar_torque_buffer)>self.max_past_buffer_size):
                    self.past_cerebellar_torque_buffer.pop()
                self.future_cerebellar_torque_buffer.pop()
                if(bool(self.future_cerebellar_torque_buffer)):
                    top_value = self.future_cerebellar_torque_buffer[-1]

        future_samples = 0
        past_samples = 0


        if(bool(self.future_cerebellar_torque_buffer)):
            if(len(self.future_cerebellar_torque_buffer) > self.max_past_buffer_size):
                future_samples = self.max_past_buffer_size + 1
            else:
                future_samples = len(self.future_cerebellar_torque_buffer)

        #If the future buffer stores at least min_future_samples + 1 samples (i.e. future samples + present sample)
        #proceed with the past samples and compute the mean filter. Otherwise the generated torque equals the last
        # commanded torque reduced by a factor.
        if(future_samples>= (self.min_future_samples +1)):
            if(bool(self.past_cerebellar_torque_buffer)):
                if(len(self.past_cerebellar_torque_buffer)>= future_samples -1):
                    past_samples = future_samples - 1
                else:
                    past_samples = len(self.past_cerebellar_torque_buffer)

            #Compute future and past samples
            last_future_sample = len(self.future_cerebellar_torque_buffer) - 1
            for i in range(future_samples):
                for j in range(len(self.joint_list)):
                    index = self.find_joint_index(self.joint_list, self.future_cerebellar_torque_buffer[last_future_sample - i]['names'][j])
                    if(index != ''):
                        mean_agonist[index] += self.future_cerebellar_torque_buffer[last_future_sample - i]['agonist'][index]
                        mean_antagonist[index] += self.future_cerebellar_torque_buffer[last_future_sample - i]['antagonist'][index]
                        samples[index] += 1

            for i in range(past_samples):
                for j in range(len(self.joint_list)):
                    index = self.find_joint_index(self.joint_list, self.past_cerebellar_torque_buffer[i]['names'][j])
                    if(index != ''):
                        mean_agonist[index] += self.past_cerebellar_torque_buffer[i]['agonist'][index]
                        mean_antagonist[index] += self.past_cerebellar_torque_buffer[i]['antagonist'][index]
                        samples[index] += 1

            for j in range(len(samples)):
                if(samples[j]>0):
                    mean_agonist[j]/=samples[j]
                    mean_antagonist[j]/=samples[j]
                else:
                    mean_agonist[j] = 0
                    mean_antagonist[j] = 0

                self.cereb_output_mean_agonist[j] = mean_agonist[j]
                self.cereb_output_mean_antagonist[j] = mean_antagonist[j]

        else:
            if(bool(self.future_cerebellar_torque_buffer)):
                top_value = self.future_cerebellar_torque_buffer[-1]
                for j in range(len(samples)):
                    self.cereb_output_mean_agonist[j] = top_value['agonist'][j]
                    self.cereb_output_mean_antagonist[j] = top_value['antagonist'][j]




    def find_joint_index(self,str_vector, name):

        found = False
        index = 0

        while(index < len(str_vector) and not found):
            if(str_vector[index] == name):
                found = True
            else:
                index = index + 1

        return index if found else ''

    def update_torque(self, current_time):
        end_time = current_time

        self.clean_cereb_buffer(end_time)
        new_message = dict()
        new_message['agonist'] = self.cereb_output_mean_agonist
        new_message['antagonist'] = self.cereb_output_mean_antagonist
        new_message['names'] = self.joint_list
        new_message['time'] = current_time

        #publish
        return new_message


    def insert_and_order_cereb_buffer_fut(self, new_msg):
        self.future_cerebellar_torque_buffer.insert(0,new_msg)
        if(len(self.future_cerebellar_torque_buffer)>1):
            index = 1
            while(index<len(self.future_cerebellar_torque_buffer) and self.future_cerebellar_torque_buffer[index-1]['stamp']<self.future_cerebellar_torque_buffer[index]['stamp']):
                aux = self.future_cerebellar_torque_buffer[index]
                self.future_cerebellar_torque_buffer[index] = self.future_cerebellar_torque_buffer[index-1]
                self.future_cerebellar_torque_buffer[index-1] = aux
                index+=1


    def insert_and_order_cereb_buffer_past(self, new_msg):
        self.past_cerebellar_torque_buffer.insert(0,new_msg)
        if(len(self.past_cerebellar_torque_buffer)>1):
            index = 1
            while(index<len(self.past_cerebellar_torque_buffer) and self.past_cerebellar_torque_buffer[index-1]['stamp']<self.past_cerebellar_torque_buffer[index]['stamp']):
                aux = self.past_cerebellar_torque_buffer[index]
                self.past_cerebellar_torque_buffer[index] = self.past_cerebellar_torque_buffer[index-1]
                self.past_cerebellar_torque_buffer[index-1] = aux
                index+=1

        
    def filter_callback(self, msg, sim_time):
        current_time = sim_time

        if(msg['stamp']>=current_time):
            self.insert_and_order_cereb_buffer_fut(deepcopy(msg))
        else:
            self.insert_and_order_cereb_buffer_past(deepcopy(msg))

            if(len(self.past_cerebellar_torque_buffer)>self.max_past_buffer_size):
                self.past_cerebellar_torque_buffer.pop()

# EOF
