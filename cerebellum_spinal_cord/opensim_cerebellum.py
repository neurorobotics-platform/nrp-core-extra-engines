import os
import numpy as np
from nrp_core.engines.py_sim import PySimEngineScript

# Spinal cord dependencies
from farms_network.neural_system import NeuralSystem
from farms_container import Container
import spinal_cord.src.spinal_cord.sc_network as ScNetwork



class Script(PySimEngineScript):

    def __init__(self):
        super().__init__()


    def init_params(self):
        # Joints and muscles names to be controlled
        self.joint_list = ['r_shoulder_elev', 'r_elbow_flexion']
        self.muscle_names = ['DELT1', 'DELT3', 'TRIlong','TRIlat','TRImed','BIClong','BICshort','BRA']

        self.agonist_muscles_per_joint = [['DELT1', 'BIClong'], ['BIClong','BICshort','BRA']]
        self.antagonist_muscles_per_joint = [['DELT3', 'TRIlong'], ['TRIlong','TRIlat','TRImed']]

        # Spinal Cord model file
        spinal_models_path = os.path.join(os.getcwd(), "spinal_cord/src/models")

        self.sc_model_file = os.path.join(spinal_models_path, "net_model_delt1and3")
        self.net_graph_file = os.path.join(spinal_models_path, "net_delt1and3.graphml")

        # Ros Topics to publish joint states
        self._registerDataPack("positions")
        self._setDataPack("positions", {"r_shoulder_elev": 0.0, "r_elbow_flexion": 0.0})

        self._registerDataPack("velocities")
        self._setDataPack("velocities", {"r_shoulder_elev": 0.0, "r_elbow_flexion": 0.0})

        self._registerDataPack("decoded_datapack")

        # Step size
        self.step_size = 0.002

        # Sampling frequency
        self.sampling_frequency = 1/self.step_size

        # Max iterations
        self.max_iterations = 2000.0

        # Arrays to store the cerebellar agonist and antagonist signals
        self.input_agonist = np.zeros(len(self.joint_list))
        self.input_antagonist = np.zeros(len(self.joint_list))
        self.input_agonist_names = self.input_antagonist_names = self.joint_list


        self.n_muscles = len(self.muscle_names)

        # muscle control signal
        self.controls = np.zeros(self.n_muscles)
        self.antagonist_input = np.zeros(self.n_muscles)
        self.agonist_input = np.zeros(self.n_muscles)

        self.biarticular_muscle_counter = np.zeros(self.n_muscles)


    def load_spinal_cord(self,step_size):
        self.sc_model = ScNetwork.build_sc(self.sc_model_file, step_size)
        self.sc_model.ext_muscles.update(self.sc_model.flex_muscles)

        #: Initialize network
        n_steps = self.max_iterations*self.sampling_frequency
        self.container = Container(max_iterations=n_steps)
        self.net_ = NeuralSystem(
            os.path.join(
                os.path.dirname(__file__),
                self.net_graph_file
            ),
            self.container
        )
        self.container.initialize()
        self.net_.setup_integrator()

    def initialize(self):
        # Initialize
        self.init_params()
        self.sim_manager.reset()

        #Load Spinal Cord model
        self.load_spinal_cord(self.step_size)



    def compute_controls(self):
        for m in range(len(self.muscle_names)):
            self.controls[m] = 0.0
            self.biarticular_muscle_counter[m] = 0.0
            for j in range(len(self.joint_list)):
                if self.muscle_names[m] in self.antagonist_muscles_per_joint[j]:
                    self.controls[m] += self.antagonist_input[j]
                    self.biarticular_muscle_counter[m]+=1.0
                elif self.muscle_names[m] in self.agonist_muscles_per_joint[j]:
                    self.controls[m] += self.agonist_input[j]
                    self.biarticular_muscle_counter[m]+=1.0

        # For biarticular muscles, get the final control signal as the mean of the joints
        # it actuates
        for n in range(len(self.muscle_names)):
            self.controls[n] = self.controls[n] / self.biarticular_muscle_counter[n]

        self.mn_rates = np.zeros(self.n_muscles)
        for i in range(len(self.muscle_names)):
            muscle = self.sim_manager.sim_interface.model.getMuscles().get(i)
            self.container.neural.inputs.get_parameter('aff_arm_' + self.muscle_names[i] + '_C').value = self.controls[i]
            self.sc_model.ext_muscles[self.muscle_names[i]].Prochazka_Ia_rates(self.sim_manager.sim_interface, muscle)
            self.container.neural.inputs.get_parameter('aff_arm_' + self.muscle_names[i] + '_Ia').value = \
                self.sc_model.ext_muscles[self.muscle_names[i]].past_Ia_rates[0]
        self.net_.step(dt=self.step_size)
        for i in range(self.n_muscles):
            self.mn_rates[i] = self.container.neural.outputs.get_parameter('nout_arm_Mn_' + self.muscle_names[i]).value



    def runLoop(self, timestep_ns):
        self.agonist_input= self._getDataPack("decoded_datapack").get("agonist")
        self.antagonist_input = self._getDataPack("decoded_datapack").get("antagonist")

        reset_flag = 0

        if reset_flag == 1:
            self.reset()
        else:
            # All Joints and Muscles can be found in the "*.osim"
            # Obtain the joint data from model "arm_26"
            # In arm_26, the joint set is [offset, r_shoulder, r_elbow]

            s_pos = self.sim_manager.get_model_all_properties(datapack_type="Joint")
            s_vel = self.sim_manager.get_model_all_properties( datapack_type="Velocity")

            # Send data to TF
            self._setDataPack("positions", {'r_shoulder_elev': s_pos['r_shoulder_elev'], 'r_elbow_flexion': s_pos['r_elbow_flexion']})
            self._setDataPack("velocities", {'r_shoulder_elev': s_vel['r_shoulder_elev'], 'r_elbow_flexion': s_vel['r_elbow_flexion']})


        self.compute_controls()

        # Set muscles' force so to change joints
        self.sim_manager.run_step(self.mn_rates, timestep_ns) #mn_rates are the output controls from spinal cord


    def shutdown(self):
        self.sim_manager.shutdown()
        print("Simulation engine is shutting down")








