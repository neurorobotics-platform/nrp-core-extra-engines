from nrp_core import *
from nrp_core.event_loop import *
from nrp_core.data.nrp_json import *

from spike_decoder import *
from cerebellum_filter import *

tau_time_constant = 0.0
delta_spike_pos = [0.55, 0.55]
delta_spike_neg = [0.55, 0.55]
joint_list = ['r_shoulder_elev', 'r_elbow_flexion']

torque_max_pos = [15.0, 15.0]
torque_max_neg = [-15.0, -15.0]
max_agonist = 1.0
min_agonist = 0.0
max_antagonist = 1.0
min_antagonist = 0.0

time_step = 0.002

min_neuron_index_pos = [20480, 20580]
max_neuron_index_pos = [20529, 20629]
min_neuron_index_neg = [20530, 20630]
max_neuron_index_neg = [20579, 20679]

spikes_decoder = SpikeDecoder(min_neuron_index_pos,max_neuron_index_pos, min_neuron_index_neg, max_neuron_index_neg, time_step, tau_time_constant, delta_spike_pos, delta_spike_neg, torque_max_pos, torque_max_neg, min_agonist, max_agonist, min_antagonist, max_antagonist, joint_list)
past_cerebellar_output_buffer_size = 10
min_future_samples = 2
motor_delay = 0.03


cereb_filter = CerebFilter(past_cerebellar_output_buffer_size, min_future_samples,joint_list)

@Clock("clock_in")
@ToEngine(keyword="control_datapack", address="/opensim")
@FromEngine(keyword='edlut_spikes', address='/edlut/spikes_datapack')
@FunctionalNode(name="opensim_control_signal", outputs=['control_datapack'])
def opensim_control_signal(edlut_spikes,clock_in):
    dec_datapack = JsonDataPack("decoded_datapack", "opensim")

    current_time = float(clock_in/1e3)
    global spikes_decoder
    global cereb_filter
    global motor_delay

    # Add spikes to decoder
    spikes_decoder.spike_callback({"time":current_time,"neuron_index":edlut_spikes.data.neuron_indexes[:],"spikes_time":edlut_spikes.data.spikes_time[:]}, motor_delay)
    agonist_antagonist_buffer = spikes_decoder.update_decoder(current_time)

    # Filter analog signals from agonist-antagonist buffer
    for i in range(len(agonist_antagonist_buffer)):
        cereb_filter.filter_callback(agonist_antagonist_buffer[i],current_time)
    filtered_signal = cereb_filter.update_torque(current_time)


    for k in filtered_signal.keys():
        dec_datapack.data[k] = filtered_signal[k]

    return [dec_datapack]



