from nrp_core import *
from nrp_core.data.nrp_json import *

from trajectory_gen import *

from nrp_core.event_loop import *

sampling_frequency = 500.0
joint_list = ['r_shoulder_elev', 'r_elbow_flexion']
samples = 750
positions_filename = 'fast_flexion_trajectory_position.txt'
velocities_filename = 'fast_flexion_trajectory_velocity.txt'

trajectory = TargetTrajectories(positions_filename, velocities_filename, len(joint_list),samples,sampling_frequency)

@Clock("clock_in")
@FunctionalNode(name="read_trajectory", outputs=['target_joint_state'], exec_policy=node_policies.functional_node.exec_policy.always)
def read_trajectory(clock_in):

    global trajectory
    current_time = float(clock_in/1e3)

    target_position, target_velocity = trajectory.get_state(current_time)

    target_state = {'positions' : {joint_list[0]:target_position[0],joint_list[1]:target_position[1]},
                    'velocities': {joint_list[0]:target_velocity[0],joint_list[1]:target_velocity[1]}}

    return [target_state]




