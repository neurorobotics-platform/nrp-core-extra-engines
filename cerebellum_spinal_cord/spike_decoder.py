import heapq
import math
from copy import deepcopy
import numpy as np

class SpikeDecoder:
    def __init__(self, min_neuron_index_pos, max_neuron_index_pos, min_neuron_index_neg, max_neuron_index_neg, time_step,tau_time_constant,spike_increment_pos, spike_increment_neg,torque_max_pos, torque_max_neg, min_agonist, max_agonist, min_antagonist, max_antagonist, joint_list):
        self.tau_time_constant = tau_time_constant
        self.spike_increment_pos = spike_increment_pos
        self.spike_increment_neg = spike_increment_neg

        self.last_time = 0.0
        self.torque_max_pos = torque_max_pos
        self.torque_max_neg = torque_max_neg
        self.min_agonist = min_agonist
        self.max_agonist = max_agonist
        self.min_antagonist = min_antagonist
        self.max_antagonist = max_antagonist
        self.time_step = time_step
        self.last_spike_time = 0.0
        self.joint_list = joint_list
        self.activity_queue = []


        self.min_neuron_index_pos = min_neuron_index_pos
        self.max_neuron_index_pos = max_neuron_index_pos
        self.min_neuron_index_neg = min_neuron_index_neg
        self.max_neuron_index_neg = max_neuron_index_neg

        self.output_var_agonist = np.zeros(len(min_neuron_index_pos))
        self.output_var_antagonist = np.zeros(len(min_neuron_index_neg))

        
    def update_decoder(self,end_time):

        agonist_antagonist_buffer = []
        checkpoint = end_time

        if(bool(self.activity_queue)):
            if(self.last_time + self.time_step > self.last_spike_time):
                checkpoint = self.last_time
            else:
                checkpoint = self.last_time + self.time_step

            top_spike = self.activity_queue[0]

            while((bool(self.activity_queue) and top_spike[0] <= checkpoint)):
                for i in range(len(self.min_neuron_index_pos)):
                    if(top_spike[1] <= self.max_neuron_index_pos[i] and top_spike[1]>= self.min_neuron_index_pos[i]):
                        self.output_var_agonist[i] += self.spike_increment_pos[i]
                    if(top_spike[1] <= self.max_neuron_index_neg[i] and top_spike[1]>= self.min_neuron_index_neg[i]):
                        self.output_var_antagonist[i] -= self.spike_increment_neg[i]

                heapq.heappop(self.activity_queue)


                if(bool(self.activity_queue)):
                    top_spike = self.activity_queue[0]

                if(top_spike[0] > checkpoint or not bool(self.activity_queue)):

                    for i in range(len(self.output_var_agonist)):

                        self.output_var_agonist[i] = self.output_var_agonist[i]/self.torque_max_pos[i]
                        if(self.output_var_agonist[i] < self.min_agonist):
                            self.output_var_agonist[i] = self.min_agonist
                        elif(self.output_var_agonist[i]> self.max_agonist):
                            self.output_var_agonist[i] = self.max_agonist

                        self.output_var_antagonist[i] = self.output_var_antagonist[i] / self.torque_max_neg[i]
                        if(self.output_var_antagonist[i] < self.min_antagonist):
                            self.output_var_antagonist[i] = self.min_antagonist
                        elif(self.output_var_antagonist[i]> self.max_antagonist):
                            self.output_var_antagonist[i] = self.max_antagonist


                    new_agonist_antagonist = dict()
                    new_agonist_antagonist['agonist'] = self.output_var_agonist.copy()
                    new_agonist_antagonist['antagonist'] = self.output_var_antagonist.copy()
                    new_agonist_antagonist['stamp'] = checkpoint
                    new_agonist_antagonist['names'] = self.joint_list


                    agonist_antagonist_buffer.append({'agonist':self.output_var_agonist.copy(),'antagonist':self.output_var_antagonist.copy(),'stamp':checkpoint,'names':self.joint_list})

                    elapsed_time = self.time_step
                    for i in range(len(self.output_var_agonist)):
                        if(self.tau_time_constant>0):
                            self.output_var_agonist[i] *= math.exp(-elapsed_time/self.tau_time_constant)
                            self.output_var_antagonist[i] *= math.exp(-elapsed_time/self.tau_time_constant)
                        else:
                            self.output_var_agonist[i] = 0
                            self.output_var_antagonist[i] = 0
                    if(checkpoint+self.time_step < self.last_spike_time):
                        checkpoint += self.time_step
        else:
            agonist_antagonist_buffer.append({'agonist':[0]*len(self.output_var_agonist),'antagonist': [0]*len(self.output_var_agonist),'stamp':checkpoint,'names':self.joint_list})

        self.last_time = checkpoint

        return agonist_antagonist_buffer

    def spike_callback(self, message, mt_delay):

        for j in range(len(message['spikes_time'])):
            found = False
            i = 0
            while(len(self.min_neuron_index_pos)>i and not found):
                found = ((message['neuron_index'][j]<=self.max_neuron_index_pos[i] and message['neuron_index'][j]>=self.min_neuron_index_pos[i]) or
                         (message['neuron_index'][j]<=self.max_neuron_index_neg[i] and message['neuron_index'][j]>=self.min_neuron_index_neg[i]))

                i+=1

            if(found):
                spike = (message['spikes_time'][j]+mt_delay,message['neuron_index'][j])
                heapq.heappush(self.activity_queue,deepcopy(spike))
                if(spike[0] > self.last_spike_time):
                    self.last_spike_time = spike[0]


# EOF
