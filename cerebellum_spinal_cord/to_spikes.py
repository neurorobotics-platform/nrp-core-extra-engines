import numpy as np

class spikesConverter:
    def __init__(self,joint_list, num_filters_row,min_neuron_index,max_neuron_index, min_values, max_values,sampling_frequency, overlapping_factor, max_spike_frequency_list):

        self.joint_list = joint_list
        self.neuron_indexes = []
        self.num_filters_row = num_filters_row
        self.max_spike_frequency = max_spike_frequency_list
        self.neuron_index_array = []
        self.time_array = []

        row = []

        for i in range(len(num_filters_row)): #[10,10] (0,9)
            for j in range(min_neuron_index[i],max_neuron_index[i]+1): #[0,40] [9,49] - (0,9) (40,49)
                row.append(j) #(0,1,2...,9) (40,41,42,...,49)
            self.neuron_indexes.append(row.copy())
            row.clear()


        self.centers = []
        self.width = []

        #Calculate centers and widths for each dimension
        for i in range(len(num_filters_row)):
            c = []
            w = []

            min_value = min_values[i]
            max_value = max_values[i]
            step = (max_value - min_value)/(num_filters_row[i]-1.0)
            for j in range(num_filters_row[i]):
                cen = min_value + step*j
                wid = step*overlapping_factor[i]
                c.append(cen)
                w.append(wid)

            self.centers.append(c)
            self.width.append(w)

        self.last_spike_times = []
        for i in range(len(num_filters_row)):
            self.last_spike_times.append([-1000]*num_filters_row[i])

        self.sampling_period = 1./sampling_frequency

    def gaussian_function(self,x, centers, widths):
        #SQUARE
        output_values = []
        aux_sum = 0
        for i,j in zip(centers,widths):
            value = 1.0 - abs((x-i)/(j))

            if (value <= 0.0):
                value = 0.00000000001
            else:
                value = 1.0
            aux_sum += value
            if(aux_sum>2):
                output_values.append(0.00000000001)
            else:
                output_values.append(value)

        return output_values

    def generate_activity(self,current_values, cur_simulation_time, delay):

        for i, value in zip(range(len(self.num_filters_row)),current_values):
            num_centers = self.num_filters_row[i]
            input_gaussian = current_values[self.joint_list[i]]
            if (current_values[self.joint_list[i]]<self.centers[i][0]):
                input_gaussian = self.centers[i][0]
            elif (current_values[self.joint_list[i]]>self.centers[i][num_centers-1]):
                input_gaussian = self.centers[i][num_centers-1]

            it_centers = self.centers[i]
            it_widths = self.width[i]
            it_neuron = self.neuron_indexes[i]
            it_spike_time = self.last_spike_times[i]
            output_current = self.gaussian_function(input_gaussian, it_centers, it_widths)


            spike_period = np.zeros(self.num_filters_row[i])
            spike_time=np.zeros(self.num_filters_row[i])
            for j in range(self.num_filters_row[i]):

                spike_period[j] = 1./(self.max_spike_frequency[i]*output_current[j])
                spike_time[j] = (it_spike_time[j])+spike_period[j]

                if(spike_time[j]<cur_simulation_time):
                    spike_time[j]=cur_simulation_time


                while(spike_time[j] < cur_simulation_time + self.sampling_period):

                    self.neuron_index_array.append(it_neuron[j])


                    self.time_array.append(spike_time[j] + delay)

                    self.last_spike_times[i][j]=spike_time[j]
                    spike_time[j]+=spike_period[j]



# EOF
