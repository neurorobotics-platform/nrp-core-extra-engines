import math
import numpy as np

class TargetTrajectories:
    def __init__(self, positions_file_name, velocities_file_name, n_joints,samples,update_frequency):

        self.n_joints = n_joints
        self.aux_index_computation = (update_frequency/samples)*(samples-1)
        self.inverse_trajectory_frequency=samples/update_frequency
        self.last_time = 0.0
        self.first_iteration = True
        self.file_positions = positions_file_name
        self.file_velocities = velocities_file_name

        self.row_positions = []
        self.row_velocities = []

        with open(positions_file_name) as pos:
            for line in pos:
                self.row_positions.append(line.strip().split())

        with open(velocities_file_name) as vel:
            for line in vel:
                self.row_velocities.append(line.strip().split())

    def get_state(self, current_time):
        elapsed_time = current_time
        current_position = np.zeros(self.n_joints)
        current_velocity = np.zeros(self.n_joints)

        if (elapsed_time > self.last_time or self.first_iteration):
            self.last_time = elapsed_time
            index = math.fmod(elapsed_time, self.inverse_trajectory_frequency) * self.aux_index_computation
            for i in range(self.n_joints):
                current_position[i] = float(self.row_positions[int(index)][i])
                current_velocity[i] = float(self.row_velocities[int(index)][i])

            self.first_iteration = False

        return current_position, current_velocity


# EOF
