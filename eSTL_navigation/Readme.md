# A brain-inspired Single Trial Learning (STL) navigation model

This folder contains an experiment leveraging a hippocampus-inspired model that,
altering its own synaptic connections can control a simulated Husky robot, which learns in a single trial the correct path to reach the exit while navigating maze.
![Husky Image](maze_image.jpg)

Details about the model in Coppolino, S., and Migliore, M., (2023) An explainable artificial intelligence approach to spatial navigation based on hippocampal circuitry.
In order to improve the stability of the synaptic plasticity, the model used in these experiments leverages Dopamine-Gated STDP (Spike-Timing-Dependent Plasticity), instead of the classic STDP used in the paper. See the learning experiment Readme for details.

Version 1.4.1 of `nrp-core` is required.



## The experiments
The simulation is split in two phases, the learning of the exit path and its recall.
The corresponding NRP experiments are contained, respectively, in the folders `learning` and `recall`; see the readme file within them for details.

Both experiments leverage the following engines (see the respective `simulation-config.json`):

- `gazebo_grpc`: simulation of the world
- `nest_server`: simulation of the neural network
- `spike_plotter`, a `python_grpc` based engine for the visualization of the spikes 

# Spike Plotter engine

To visualize the spikes of the neurons of the brain network, the experiments leverage a `matplotlib`-based raster plotter called `Spike plotter`.
The plotter is implemented as an Engine, thus reusable in other experiments, it spans two python modules: `spikes_plotter_engine.py` (the nrp-core ) and `resources/spike_plotter.py` (the module used by the former).
See the source files for documentation.
The TF that feeds spike data to the engine is in `recorders_monitor.py`  

![Spike Plotter](spike_plotter.png){width=50%}

## Dependencies
The experiments require `nrp-core`version >= 1.4.1 and the following python packages:
```shell
pip install opencv matplotlib
```


## Known Issues 

`Spike plotter` requires write rights on `/dev/shm` due to its use of python's `multiprocessing` module.
Run the following command to grant them:
```shell
sudo chmod 777 /dev/shm
```
