# A brain-inspired Single Trial Learning (STL) navigation model - LEARNING
 
This is the nrp-core experiment implementing the learning phase of the paper [Coppolino, S., and Migliore, M., (2023) An explainable artificial intelligence approach to spatial navigation based on hippocampal circuitry](https://doi.org/10.1016/j.neunet.2023.03.030).

To run the experiment use the following command:
```
# Compile ONCE the protobuf messages
$ nrp_compile_protobuf.py --proto_files_path ./proto_msgs

# run the experiment
$ NRPCoreSim -c simulation_config.json 2>log.err # redirect std-err to log.err, nest-server is very noisy

# In a separate terminal, start optional gazebo visualization 
$ gzclient
```

Once the robot has found the exit, the learned synaptic weights to be used in the recall phase, are saved in `learned_synaptic_weights/weights.json`.
Copy the file in the corresponding location of the `recall` experiment before starting it.

## Robot behavior 
The robot behavior is defined by the following state machine:

States are implemented in `resources/robot_states.py`, state transitions are labeled with the conditions that triggers them.
In square brackets are the prerequisites for the transition.

![state machines](diagrams/learning_states.svg){width=50%}


## TFs dataflow
The dataflow among the Transceiver functions is shown in the following graph:

Nodes are labeled with TF names (see `simulation_config.json`),
edges with the datapack name/global variable used by the receiving node; 
e.g. the `robot_control` TF owns the `robot_controller` variable that is used by the `image_processing` TF.

![TFs dataflow](diagrams/tf_learning_graph.svg){width=50%}


## Changes in the neural network

Using the network architecture described in the original paper, the plasticity between object cells and place cells have been found to be problematic,
In particular when trying to prevent an increase in connections strength for objects unrelated to the robot's current path. 
Thus, to improve the stability of the network, the model employed in the experiments deviates from the original in the follows aspects:

### Dopamine-Gated STDP for Object Cell and Place Cells Plasticity instead of STDP

   - **Dopamine Neuron**
     - **Purpose**: Regulates plasticity, particularly when the robot moves towards an object.
     - **Functionality**:
       - Activates plasticity only while the robot approaches an object, preventing Long-Term Potentiation (LTP) during object selection or redirection.
       - Triggers when an object sensor detects a certain intensity, ensuring the robot-object distance is not exceedingly large.
   - **Inhibitory Dopamine Neuron**
     - **Purpose**: Refines the timing for activating plasticity.
     - **Functionality**:
       - Suppresses the dopamine neuron.
       - Activates when in proximity to an object, preventing the LTP of object cells and place cells when the robot begins to turn.

### Inhibitory Connection between Dead End Cell and Place Cells

   - **Purpose**: Modulates the firing rate of place cells as the robot retraces to the maze's start.
   - **Functionality**:
     - Reduces the firing rate of place cells during the robot’s return journey.
     - Enhances the firing rate disparity between place cells during LTP and Long-Term Depression (LTD) phases, expanding the operational parameter space.

### Continuous Current Influx to Object Cells' Inhibitory Neuron

   - **Purpose**: Regulates the firing rate of the object cell during the system’s learning phase.
   - **Functionality**:
     - Diminishes the object cell's firing rate during learning, amplifying the firing rate difference between object cells across learning and forgetting phases.
     - During the forgetting phase, the dead-end cells inhibit the inhibitory neuron of the object cells, affecting the firing rate.
