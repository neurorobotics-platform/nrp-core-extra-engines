from functools import reduce
from typing import Dict

import cv2
import numpy as np

COLOR_THRESHOLDS = {
    "RED": {"lowerb": np.array([0, 30, 30]),
            "upperb": np.array([0, 255, 255])},
    "GREEN": {"lowerb": np.array([50, 30, 30]),
              "upperb": np.array([70, 255, 255])},
    "BLUE": {"lowerb": np.array([115, 100, 20]),
             "upperb": np.array([125, 255, 255])},
    "ORANGE": {"lowerb": np.array([15, 50, 50]),
               "upperb": np.array([24, 255, 255])},
    "YELLOW": {"lowerb": np.array([25, 50, 50]),
               "upperb": np.array([35, 255, 255])},
    "PURPLE": {"lowerb": np.array([150, 200, 20]),
               "upperb": np.array([165, 255, 255])},
    "CYAN": {"lowerb": np.array([85, 100, 30]),
             "upperb": np.array([90, 255, 255])},
    "MAGENTA": {"lowerb": np.array([135, 200, 20]),
                "upperb": np.array([145, 255, 255])}
}


def create_detector(params: cv2.SimpleBlobDetector_Params = None):
    if params is None:
        # Set up the SimpleBlobdetector with default parameters.
        params = cv2.SimpleBlobDetector_Params()

        # Change thresholds
        params.minThreshold = 0
        params.maxThreshold = 100

        # Filter by Area.
        params.filterByArea = True
        params.minArea = 80
        params.maxArea = 2500  # approx 6 meters from the cube

        # Filter by Circularity
        params.filterByCircularity = True
        params.minCircularity = 0.1

        # Filter by Convexity
        params.filterByConvexity = True
        params.minConvexity = 0.5

        # Filter by Inertia
        params.filterByInertia = True
        params.minInertiaRatio = 0.5

    return cv2.SimpleBlobDetector_create(params)


def detect_colors(detector, cv_image, colors, color_thresholds, black_mask=None,
                  process_mask=True):
    # Convert to HSV color space for easier color detection
    image_hsv = cv2.cvtColor(cv_image, cv2.COLOR_RGB2HSV)

    black_mask = black_mask if black_mask is not None else np.zeros(
        (image_hsv.shape[0], image_hsv.shape[1]), dtype=np.uint8)

    filtered_color_thresholds = [c_t for c, c_t in color_thresholds.items() if c in colors]

    mask = reduce(lambda acc, c_t: acc + cv2.inRange(image_hsv, c_t["lowerb"], c_t["upperb"]),
                  filtered_color_thresholds,
                  black_mask)

    if process_mask:
        mask = cv2.erode(cv2.dilate(mask, None, iterations=2), None, iterations=2)

    keypoints = detector.detect(255 - mask)  # detect with inverted mask

    # return color_to_keypoint dict
    return color_to_keypoint(image_hsv, keypoints, color_thresholds)


def decorate_image(cv_image, colors_to_keypoints, target_offset=None):
    rows, cols, _ = cv_image.shape

    center_col = cols // 2

    # decorate image with keypoints
    decorated_image = cv2.drawKeypoints(cv_image, list(colors_to_keypoints.values()),
                                        np.array([]), (0, 255, 0),
                                        cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    # draw black reference line
    decorated_image = cv2.line(decorated_image, (center_col, 0), (center_col, rows), (0, 0, 0), 1)

    if target_offset:
        # draw red target reference line
        decorated_image = cv2.line(decorated_image, (target_offset, 0), (target_offset, rows), (0, 0, 255), 1)

    return decorated_image


def keypoint_area(kp: cv2.KeyPoint):
    return int(((kp.size / 2) ** 2) * np.pi)


def pixel_color(pixel, color_theresholds):
    for c, c_t in color_theresholds.items():
        if all(c_t["lowerb"] <= pixel) and all(pixel <= c_t["upperb"]):
            return c
    else:
        return None


def keypoint_color(image_hsv, kp: cv2.KeyPoint, color_thresholds):
    x, y = int(kp.pt[0]), int(kp.pt[1])
    return pixel_color(image_hsv[y, x], color_thresholds)


def color_to_keypoint(image_hsv, keypoints, color_thresholds) -> Dict[str, cv2.KeyPoint]:
    return {color: kp for kp in keypoints if
            (color := keypoint_color(image_hsv, kp, color_thresholds)) is not None}
