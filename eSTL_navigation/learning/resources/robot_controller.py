from __future__ import annotations

from typing import List, Dict, Set, Optional, FrozenSet

import numpy as np

try:
    import utils
    import robot_states

except ImportError:
    import sys
    import os

    # add resources to PYTHONPATH
    resources_path = os.path.join(os.getcwd(), "resources")
    sys.path.append(resources_path)
    import utils
    import robot_states

SIMPLE_OBJECTS_DEFAULT = ["RED", "BLUE", "YELLOW", "CYAN", "ORANGE", "GREEN"]
EXIT_OBJECT_DEFAULT = "GREEN"
START_OBJECT_DEFAULT = "MAGENTA"
DEAD_END_OBJECT_DEFAULT = "PURPLE"

NUM_PLACE_CELLS_DEFAULT = 18


def make_learning_robot(simple_objects: List[str] = SIMPLE_OBJECTS_DEFAULT,
                        start_obj: str = START_OBJECT_DEFAULT,
                        dead_end_obj: str = DEAD_END_OBJECT_DEFAULT,
                        exit_obj: str = EXIT_OBJECT_DEFAULT,
                        num_place_cells: float = NUM_PLACE_CELLS_DEFAULT,
                        logger=None):
    return Robot(simple_objects=simple_objects,
                 start_obj=start_obj,
                 dead_end_obj=dead_end_obj,
                 exit_obj=exit_obj,
                 num_place_cells=num_place_cells,
                 logger=logger)


def make_recall_robot(simple_objects: List[str] = SIMPLE_OBJECTS_DEFAULT,
                      start_obj: str = START_OBJECT_DEFAULT,
                      dead_end_obj: str = DEAD_END_OBJECT_DEFAULT,
                      exit_obj: str = EXIT_OBJECT_DEFAULT,
                      num_place_cells: float = NUM_PLACE_CELLS_DEFAULT,
                      logger=None):
    robot = Robot(simple_objects=simple_objects,
                  start_obj=start_obj,
                  dead_end_obj=dead_end_obj,
                  exit_obj=exit_obj,
                  initial_state="MoveForwardState",
                  num_place_cells=num_place_cells,
                  logger=logger)
    return robot


class Robot:

    def __init__(self,
                 simple_objects: List[str] = SIMPLE_OBJECTS_DEFAULT,
                 start_obj: str = START_OBJECT_DEFAULT,
                 dead_end_obj: str = DEAD_END_OBJECT_DEFAULT,
                 exit_obj: str = EXIT_OBJECT_DEFAULT,
                 initial_state: str = "FindPivotState",
                 num_place_cells: float = NUM_PLACE_CELLS_DEFAULT,
                 logger=None):

        # pose of the robot (updated by TF update_robot_pose)
        # { "position" : ndarray([x, y, z])
        #   "orientation" : ndarray[x, y, z, w])
        # }
        self.pose: Optional[Dict[str, np.ndarray[float]]] = None

        self.logger = logger

        # number of place cells encoding a sector span of a turn.
        self.num_place_cells = num_place_cells
        self.sector_span_deg = 360 / num_place_cells

        # objects
        self.start_object = start_obj
        self.dead_end_object = dead_end_obj
        self.exit_object = exit_obj

        self.simple_objects_list = simple_objects

        self.special_objects_list = [
            self.exit_object,
            self.start_object,
            self.dead_end_object,
        ]

        self.simple_objects: FrozenSet[str] = frozenset(simple_objects)
        self.special_objects: FrozenSet[str] = frozenset(self.special_objects_list)

        # current navigation goal and info (set by goal_object_generation TF
        #                                   or by self.do_rotate_on_self)
        # {"color": str,
        #  "reached": bool,
        #  "info": color_info}
        #
        # "info" is None when the object is not visible, thus it should be looked for.
        # If None, no goal is selected, the robot should rotate on itself (do_rotate_on_self).
        self._goal_object: Dict[str, Dict] = None

        # the sector to align with in recall mode
        self._goal_direction_sector: int = None

        # the direction to align with or move towards in recall mode,
        # either computed from goal_direction_sector or manually set
        self.goal_direction_deg: float = None

        # object info of visible objects addressable by color
        # { color: {"color": str,
        #           "azimuth": int,
        #           "area": int}}
        self.visible_objects_info_dict: Dict[str, Dict] = {}

        # paths found from pivot
        self.paths_to_explore: Set[str] = set()
        self.paths_to_explore_direction: Dict[str, str] = {}  # Color -> cardinal direction

        # path being explored, current_path[0] is random.choice(paths_to_explore)
        self.path_being_explored: Optional[List[str]] = None  # LIFO
        self._path_being_explored_direction: str = None

        # the first object seen, paths are discovered at pivot
        self.pivot: str = None

        # state in which the robot is in
        self.state = getattr(robot_states, initial_state)(self, logger=self.logger)

        self.camera = utils.Camera()

    @property
    def goal_direction_sector(self):
        return self._goal_direction_sector

    @goal_direction_sector.setter
    def goal_direction_sector(self, cell_index):
        # set goal_direction_sector and update goal direction computing the goal angle from it
        self._goal_direction_sector = cell_index

        self.goal_direction_deg = utils.cell_index_to_yaw(cell_index,
                                                          self.num_place_cells)

    @property
    def path_being_explored_direction(self) -> Optional[str]:  # "NESW"
        # a valid path_being_explored is [start_object, pivot, start_of_path_object, objs..,]

        if self.path_being_explored is None or (len(self.path_being_explored) < 3):
            return None

        return self._path_being_explored_direction

    @path_being_explored_direction.setter
    def path_being_explored_direction(self, value):
        self._path_being_explored_direction = value

    @property
    def heading_direction(self) -> str:  # "NESW"
        yaw_rad = utils.yaw_euler_from_quaternion(*self.pose["orientation"])
        return utils.angle_to_cardinal_direction(yaw_rad)

    @property
    def goal_object(self):
        if self._goal_object is not None:
            self._goal_object["info"] = self.visible_objects_info_dict.get(self._goal_object["color"], None)

        return self._goal_object

    @goal_object.setter
    def goal_object(self, color: str):

        if self._goal_object is not None and self._goal_object["color"] == color:
            return

        self._goal_object = {"color": color,
                             "reached": False,
                             "info": None}

    @property
    def closest_visible_object_info(self) -> Optional[Dict]:
        visible_objects_info = self.visible_objects_info_dict.values()
        return self.sort_objects_by_distance(visible_objects_info)[0] if visible_objects_info else None

    @property
    def closest_visible_regular_object_info(self) -> Optional[Dict]:
        visible_regular_objects_info = [obj_info for obj_id, obj_info in
                                        self.visible_objects_info_dict.items() if
                                        obj_id in self.simple_objects]

        return self.sort_objects_by_distance(visible_regular_objects_info)[0] if visible_regular_objects_info else None

    @staticmethod
    def sort_objects_by_distance(objects_info_list):
        """Sort objects_info_list by "area" key in descending order"""
        return sorted(objects_info_list, key=lambda obj_info: obj_info["area"], reverse=True)

    def is_dead_end_object(self, object_id: str):
        return self.dead_end_object == object_id

    def is_start_object(self, object_id: str):
        return self.start_object == object_id

    def is_exit_object(self, object_id: str):
        return self.exit_object == object_id

    def do_rotate_on_self(self):
        # goal_object is None means ROTATE on self, defined in motor_control TF
        self._goal_object = None

    @property
    def is_rotating_on_self(self):
        return self._goal_object is not None

    def transition_to_state(self, new_state):
        self.logger(f"{self.state.__class__.__name__} --> {new_state.__class__.__name__}")
        self.state = new_state

    @property
    def has_exited(self):
        return self.state is not None and isinstance(self.state, robot_states.FinalState)

    def step(self, t_ns):
        self.logger(f"[{self.state.__class__.__name__}] robot_controller.step")
        self.state.step(t_ns)
