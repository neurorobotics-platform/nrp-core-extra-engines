from __future__ import annotations

import abc
import math
import random
from typing import Dict, Union

import numpy as np

try:
    import utils
    import robot_controller
except ImportError:
    import sys
    import os

    # add resources to PYTHONPATH
    resources_path = os.path.join(os.getcwd(), "resources")
    sys.path.append(resources_path)
    import utils
    import robot_controller

ROTATE_ON_SELF_ANGULAR_SPEED = 90.0 * 2  # deg/s from motor_control TF


class RobotState(abc.ABC):

    def __init__(self, robot: robot_controller.robot, logger=None):
        self.robot = robot
        self.logger = logger or print

    @abc.abstractmethod
    def step(self, t_ns):
        pass

    def logger_info(self, message):
        log = f'{self.__class__.__name__}: {message}'

        self.logger(log)


class RecallState(RobotState):

    def step(self, t_ns):
        pass


class FinalState(RobotState):
    DISPLAY_PERIOD_S = 2  # secs

    def __init__(self, robot: robot_controller.Robot, logger=None, message=""):
        super().__init__(robot, logger)
        self.message = message
        self.wake_up_time = None

    def step(self, t_ns):
        t_s = t_ns / 1e9
        if self.wake_up_time is None:
            self.wake_up_time = t_s + self.DISPLAY_PERIOD_S  # start DISPLAY_PERIOD_S timer
            self.logger_info(self.message)

        elif t_s > self.wake_up_time:
            self.logger_info(self.message)
            self.wake_up_time = None  # reset timer


class FailState(FinalState):
    pass


# FindPaths
class FindPivotState(RobotState):

    def step(self, t_ns):

        closest_regular_object_info = self.robot.closest_visible_regular_object_info

        if closest_regular_object_info is None:
            self.logger_info("Can't see any object")
            return

        if self.robot.pivot is None:
            # find new pivot
            self.robot.pivot = closest_regular_object_info["color"]
            next_state = "SearchAvailablePathsState"
        else:
            next_state = "ChoosePathState"

        # pivot is the navigation goal
        self.robot.goal_object = self.robot.pivot
        self.logger_info(f'Goal is Pivot ({self.robot.pivot})')

        self.robot.transition_to_state(MoveToObjectState(self.robot, logger=self.logger,
                                                         next_state=next_state))


class MoveToObjectState(RobotState):

    def __init__(self, robot: robot_controller.Robot, logger=None, next_state=None,
                 **next_state_kwargs):
        super().__init__(robot, logger)

        if next_state is None:
            next_state = "FailState"
            next_state_kwargs = {"message": "No next state specified"}

        self.next_state = next_state
        self.next_state_instance = globals()[next_state](self.robot, logger=self.logger,
                                                         **next_state_kwargs)

    def step(self, t_ns):

        if (goal := self.robot.goal_object) is None:
            self.logger_info(f"No object goal available")
            # TODO Fail?
            return

        if goal["reached"]:  # color.spiked -> reached
            # goal["reached"] = False
            self.logger_info(f"Object ({goal['color']}) Reached!")

            self.robot.transition_to_state(self.next_state_instance)
        else:
            self.logger_info(f'Goal is {goal["color"]}). Moving...')


class SearchAvailablePathsState(RobotState):
    MIN_OBJECT_AREA = 120

    def __init__(self, robot: robot_controller.Robot, logger=None):
        super().__init__(robot, logger=logger)

        self.rotation_start_time_ns = 0
        self.rotation_start_yaw_deg = None
        self.rotation_min_duration_ns = int(2 * 1e9)  # 2 secs

        # {"offset": 0, # "directions": np.ndarray}
        self.objects_directions: Dict[str, Dict[str, Union[int, np.ndarray]]] = {}
        self.objects_directions_len = 50

    def step(self, t_ns):
        t_s = t_ns / 1e9
        # Rotate on self and look for paths
        yaw_deg = math.degrees(
            math.fmod(utils.yaw_euler_from_quaternion(*self.robot.pose["orientation"]),
                      2 * math.pi))

        if self.rotation_start_yaw_deg is not None:
            # exploration completed (full rotation within 1 degree), after at least self.rotation_min_duration_ns time
            if (t_ns - self.rotation_start_time_ns) > self.rotation_min_duration_ns and abs(
                    self.rotation_start_yaw_deg - yaw_deg) < 1.:
                # paths search complete, compute direction of every object found
                for obj_id, obj_direction in self.objects_directions.items():
                    median_direction_angle = np.median(obj_direction["directions"])
                    self.robot.paths_to_explore_direction[
                        obj_id] = utils.angle_to_cardinal_direction(median_direction_angle)

                self.logger_info(
                    f'[{t_s:.2f}] STOP. YAW: {yaw_deg} Found paths {self.robot.paths_to_explore_direction}')

                self.robot.transition_to_state(ChoosePathState(self.robot, logger=self.logger))
                return
        else:
            # start path exploration: rotate on self counter-clockwise
            self.logger_info(f'[{t_s:.2f}] START path exploration. YAW: {yaw_deg}')
            self.rotation_start_yaw_deg = yaw_deg
            self.rotation_start_time_ns = t_ns

        # - rotate
        self.logger_info(f"ROTATE")
        self.robot.do_rotate_on_self()

        pivot_less_visible_regular_objects_info = (o_info
                                                   for o_info in
                                                   self.robot.visible_objects_info_dict.values()
                                                   if
                                                   (obj_id := o_info["color"]) != self.robot.pivot
                                                   and obj_id not in self.robot.special_objects)

        # visible object in the foreground (i.e. >= MIN_OBJECT_AREA) except pivot
        foreground_pivot_less_visible_regular_objects_ids = [o_info["color"]
                                                             for o_info in
                                                             pivot_less_visible_regular_objects_info
                                                             if
                                                             o_info["area"] >= self.MIN_OBJECT_AREA]

        # - look for paths. No duplicates, it's a set
        yaw_rad = utils.yaw_euler_from_quaternion(*self.robot.pose["orientation"])
        for object_id in foreground_pivot_less_visible_regular_objects_ids:
            self.robot.paths_to_explore.add(object_id)

            # keep track of object heading
            try:
                object_direction = self.objects_directions[object_id]
            except KeyError:
                object_direction = self.objects_directions[object_id] = {"offset": 0,
                                                                         "directions": np.zeros(
                                                                             self.objects_directions_len)}  # 1 sec buffer
            object_direction["directions"][
                object_direction["offset"] % self.objects_directions_len] = yaw_rad
            object_direction["offset"] += 1


class ChoosePathState(RobotState):

    fixed_paths_to_explore = iter(["RED", "YELLOW"])  #  "ORANGE" list here object to force a path

    def step(self, t_ns):
        if len(self.robot.paths_to_explore) == 0:
            # no more paths left. move to error state
            self.robot.transition_to_state(FailState(self.robot, logger=self.logger,
                                                     message="No paths are left to Explore!! EXIT NOT FOUND!"))
            return

        if self.robot.path_being_explored is not None and len(self.robot.path_being_explored) > 0:
            self.logger_info(f"A PATH IS STILL ACTIVE: {self.robot.path_being_explored}")
            return

        # choose randomly among paths
        # if we don't care about using random, which requires a list, set.pop() can be used instead
        #chosen_path_object_id = random.choice(list(self.robot.paths_to_explore))
        chosen_path_object_id = next(self.fixed_paths_to_explore)

        # start keeping track of path, and its heading, being explored
        self.robot.path_being_explored = [self.robot.start_object, self.robot.pivot,
                                          chosen_path_object_id]
        self.robot.path_being_explored_direction = self.robot.paths_to_explore_direction[
            chosen_path_object_id]

        self.logger_info(
            f"Chosen path: {chosen_path_object_id}, HEADING: {self.robot.path_being_explored_direction}")
        self.logger_info(f"Paths left to explore: {self.robot.paths_to_explore}")

        # set_goal
        self.robot.goal_object = chosen_path_object_id

        self.robot.transition_to_state(MoveToObjectState(self.robot, logger=self.logger,
                                                         next_state="ObjectReached"))

        # remove from paths to explore
        self.robot.paths_to_explore.remove(chosen_path_object_id)
        del self.robot.paths_to_explore_direction[chosen_path_object_id]


# ExplorePaths
class ObjectReached(RobotState):

    def step(self, t_ns):
        # check which object reached
        if (goal_object := self.robot.goal_object) is None:
            self.logger_info("No object goal available")
            # TODO Fail?
            return

        # if goal_info := self.robot.goal_object["info"] is None:
        #     self.logger_info("No object goal info available")
        #     # TODO Fail?
        #     return
        goal_object_id = goal_object["color"]

        if self.robot.is_dead_end_object(goal_object_id):
            # reached last object of a path, remove it
            del self.robot.path_being_explored[-1]  # LIFO pop
            # dead_end object: backtrack to start object
            self.robot.transition_to_state(
                ChoosePreviousObjectState(self.robot, logger=self.logger))

        elif self.robot.is_exit_object(goal_object_id):
            # exit object: saving of the weights is performed in TF
            self.robot.transition_to_state(
                FinalState(self.robot, logger=self.logger, message="Exit FOUND!"))


        else:
            # normal object. find closest unseen obj
            self.robot.transition_to_state(
                FindClosestUnseenObjectState(self.robot, logger=self.logger))


class FindClosestUnseenObjectState(RobotState):

    def step(self, t_ns):

        if (closest_visible_object_info := self.robot.closest_visible_object_info) is None:
            self.logger_info("Can't see any object")
            return

        if not self.robot.path_being_explored:  # None or empty
            self.logger_info("No object to move to is available")
            self.robot.transition_to_state(
                FailState(self.robot, logger=self.logger,
                          message="The path being explored is empty. ERROR"))
            return

        # look for closest object not in path and not one of the paths to explore

        # robot should find objects on the same path,
        # i.e. along the same direction of the first object of the path

        closest_visible_object_id = closest_visible_object_info["color"]

        # objects should be new and on the same path
        is_robot_aligned_to_path = (
                    self.robot.heading_direction == self.robot.path_being_explored_direction)
        is_new_object = closest_visible_object_id not in self.robot.path_being_explored

        closest_unseen_object_id = closest_visible_object_id if (
                    is_new_object and is_robot_aligned_to_path) else None

        self.logger_info(f"{closest_visible_object_id=}")

        if closest_unseen_object_id is None:
            # can't see goal, look for it
            self.robot.do_rotate_on_self()
            self.logger_info("No goal found. Keep looking.")
        else:
            self.robot.goal_object = closest_unseen_object_id

            # append to current path
            self.robot.path_being_explored.append(closest_unseen_object_id)

            self.logger_info(f"Found goal: {closest_unseen_object_id}")

            self.robot.transition_to_state(
                MoveToObjectState(self.robot, logger=self.logger, next_state="ObjectReached"))


# ReturnToStart
class ChoosePreviousObjectState(RobotState):

    def step(self, t_ns):
        if not self.robot.path_being_explored:  # None or empty
            self.logger_info("No object to move to is available")
            self.robot.transition_to_state(FailState(self.robot, logger=self.logger,
                                                     message=f"Path fully backtracked but "
                                                             f"{self.robot.start_object} NOT found."
                                                             f" ERROR"))
            return

        previous_object_id = self.robot.path_being_explored[-1]  # LIFO peek

        self.robot.goal_object = previous_object_id

        self.logger_info(f"Chose goal: {previous_object_id}")

        self.robot.transition_to_state(
            MoveToObjectState(self.robot, logger=self.logger, next_state="InnerObjectReached"))


class FindPreviousObjectState(RobotState):

    def step(self, t_ns):

        if len(self.robot.visible_objects_info_dict) == 0:
            self.logger_info("Can't see any object")
            return

        if not self.robot.path_being_explored:  # None or empty
            self.logger_info("No object to move to is available")
            self.robot.transition_to_state(FailState(self.robot, logger=self.logger,
                                                     message=f"Path fully backtracked but "
                                                             f"{self.robot.start_object} NOT found."
                                                             f" ERROR"))
            return

        # look for previous object in path,
        # remove from list once reached and before choosing the next
        previous_object_in_path_id = self.robot.path_being_explored[-1]  # LIFO peek

        if previous_object_in_path_id not in self.robot.visible_objects_info_dict:
            # can't see goal, look for it
            self.robot.do_rotate_on_self()
            self.logger_info("No goal found. Keep looking.")
        else:
            # previous_object_in_path seen, move toward it
            self.robot.goal_object = previous_object_in_path_id

            self.logger_info(f"Found goal: {previous_object_in_path_id}")

            self.robot.transition_to_state(
                MoveToObjectState(self.robot, logger=self.logger, next_state="InnerObjectReached"))


class InnerObjectReached(RobotState):

    def step(self, t_ns):
        # check which object reached
        if (goal_object := self.robot.goal_object) is None:
            self.logger_info("InnerObjectReached: No object goal available")
            # TODO Fail?
            return

        # object reached remove from current path
        del self.robot.path_being_explored[-1]  # LIFO pop

        if self.robot.is_start_object(goal_object["color"]):
            # start object: go to pivot
            self.robot.transition_to_state(FindPivotState(self.robot, logger=self.logger))
        else:
            # normal object. find previous object in path
            self.robot.transition_to_state(
                ChoosePreviousObjectState(self.robot, logger=self.logger))
