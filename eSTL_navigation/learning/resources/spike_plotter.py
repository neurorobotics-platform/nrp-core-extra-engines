import sys

import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
import numpy as np
from matplotlib import ticker
import matplotlib as mpl

mplstyle.use('fast')
# deactivate toolbar
mpl.rcParams['toolbar'] = 'None'

np.set_printoptions(threshold=sys.maxsize)


class SpikePlotter:
    def __init__(
            self,
            timestep_s: float = 0.02,  # secs
            window_len_s: float = 10.,  # secs
            num_neurons: int = 50
    ) -> None:
        self.timestep_s: float = timestep_s
        self.timestep_ns: int = int(timestep_s * 1e9)

        self.window_len_s: float = window_len_s
        self.window_len_ns: int = int(window_len_s * 1e9)
        self.window_len_idx = int(self.window_len_ns / self.timestep_ns)

        self.num_neurons = num_neurons
        self.t_ns: int = 0

        self.animated_artists = {}

        self.plotting_data = np.zeros([num_neurons, self.window_len_idx],
                                      dtype=np.bool_)

        self.x_data = []
        self.y_data = []

        self.plt = plt

        ### Setup the figure
        self.fig, self.ax = plt.subplots(figsize=plt.figaspect(9 / 16))

        # Windows resizing is not supported. Prevent it.
        bck = plt.get_backend()
        if bck == "TkAgg":
            self.fig.canvas.manager.window.resizable(False, False)
        elif bck == "QT4Agg":
            win = self.fig.canvas.window()
            win.setFixedSize(win.size())
        # TODO support more backends

        self.fig.canvas.manager.set_window_title('NRP Spike Plotter')

        # Configure axis.
        # Draw the un-animated part of the plot
        self._configure_axis()

        # keep track of X axis artist for redraw
        self.animated_artists["xaxis"] = self.ax.xaxis

        self.animated_artists["time_vlines"] = self.ax.vlines(x=[0.],
                                                              ymin=0, ymax=self.num_neurons + 1,
                                                              colors='red',
                                                              ls='--', lw=1, animated=True)
        # Plot the initial configuration
        self.animated_artists["plot"] = self.ax.scatter([0], [0],
                                                        marker='.',
                                                        c=[0],
                                                        animated=True)

        # Draw the animated artists
        self.draw_artists()
        # show the result to the screen, this pushes the updated RGBA buffer from the
        # renderer to the GUI framework, so you can see it
        self.fig.canvas.blit(self.fig.bbox)

    def _configure_axis(self):
        # Configure axis.
        # Draw the non-animated part of the plot
        self.ax.set_title("Spike Plotter")

        # Y axis
        self.ax.set(ylabel="Neuron ID")
        self.ax.set_ylim(-1, self.num_neurons)

        # X axis
        self.ax.set(xlabel="Time [s]")
        self.ax.set_xlim(-self.window_len_s, 0.)
        self.ax.xaxis.set_animated(True)

        # Customize the tick locator to hide negative numbers major ticks
        def hide_negative_ticks(x, _pos):
            x_int = int(x)
            # restore formatter when out of negative time range to save calling this function
            if x_int > int(self.window_len_s):
                self.ax.xaxis.set_major_formatter(self.original_xaxis_major_formatter)
            return x_int if x_int >= 0 else ""

        self.original_xaxis_major_formatter = self.ax.xaxis.get_major_formatter()
        self.ax.xaxis.set_major_formatter(ticker.FuncFormatter(hide_negative_ticks))

        # Show figure and store the cleaned background for later update
        plt.show(block=False)
        plt.pause(0.1)

        self.clean_background = self.fig.canvas.copy_from_bbox(self.fig.bbox)

    def draw_artists(self):
        for a in self.animated_artists.values():
            self.ax.draw_artist(a)

    def update_data(self, datum) -> None:
        """Update the data stored for plotting with provided datum"""
        # datum: {
        #     "simulationTime": int, # ns
        #     "neurons": np.ndarray[int],
        #     "spikeCounts": np.ndarray[int]
        # }
        # add datum to self.plotting_data
        if datum["simulationTime"] >= self.window_len_ns:
            # shift columns to the left, np.roll is slower
            self.plotting_data[:, :-1] = self.plotting_data[:, 1:]
            self.plotting_data[:, -1] = False  # clear last column
            curr_col = -1
        else:
            curr_col = int(datum["simulationTime"] / self.timestep_ns)

        if len(datum["neurons"]):
            self.plotting_data[datum["neurons"], curr_col] = True  # set curr_col column

            # TODO optimization,
            #  we could save the previous x_data and y_data and compute nonzero only on datum
            #  currently we compute it on a matrix of constant size (num_neurons, windows_len_idx)

        # with catchtime("update_data (nonzero)"):
        self.y_data, self.x_data = np.nonzero(self.plotting_data)

        self.t_ns = datum["simulationTime"]

    def update_actors(self) -> None:
        """Update the figure by updating the data relative to each actor."""

        # shift x-axis limits
        plot_xmin, plot_xmax = self.ax.get_xlim()
        new_xmin = round(plot_xmin + self.timestep_s, 3)
        new_xmax = round(plot_xmax + self.timestep_s, 3)

        self.ax.set_xlim(xmin=new_xmin, xmax=new_xmax)

        # compute time from the y coords of non-zero matrix elems
        x_data_to_secs = (self.x_data * self.timestep_s) + max(new_xmin, 0.)

        # self.plot_ln.set_data(x_data_to_secs, self.y_data)
        self.animated_artists["plot"].set_offsets(np.column_stack((x_data_to_secs, self.y_data)))
        self.animated_artists["plot"].set_array(self.y_data)

        self.animated_artists["time_vlines"] = self.ax.vlines(
            x=np.arange(start=np.ceil(min(0., new_xmin)), stop=np.floor(new_xmax) + self.timestep_s,
                        step=1.),
            ymin=0, ymax=self.num_neurons + 1,
            colors='red', ls='--', lw=1,
            animated=True)

    def update_fig(self):

        self.fig.canvas.restore_region(self.clean_background)

        # update artists
        self.update_actors()

        # redraw artists after update
        self.draw_artists()

        self.fig.canvas.blit(self.fig.bbox)
        self.fig.canvas.flush_events()

    def close(self):
        plt.close(self.fig)
