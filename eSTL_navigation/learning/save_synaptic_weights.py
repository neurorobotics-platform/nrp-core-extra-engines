# This TF saves the learned synaptic weights in a file specified by
# save_synaptic_weights_vars["learned_weights_filename"] once the robot has found the exit.

import json

from nrp_core import TransceiverFunction
from nrp_core.data.nrp_protobuf import *

save_synaptic_weights_vars = {
    "learned_weights_filename": "learned_synaptic_weights/weights.json",
    "robot_exit_time_ns": None,  # index of the first spike event happened in this timestep
    "exit_wait_time_ns": 4 * 10 ** 9,  # wait before saving weights
    # names of the neuronal connections to monitor (defined in brain_script.py)
    "connections_dps": ["persistent_cells_to_object_cells", "object_cells_to_place_cells"]
}


@EngineDataPacks(keyword='connections_dp',
                 datapackNames=save_synaptic_weights_vars["connections_dps"],
                 engineName='nest')
@SimulationTime(keyword="t_ns")
@TransceiverFunction("nest")
def save_synaptic_weights(t_ns, connections_dp):
    tf_vars = save_synaptic_weights_vars

    # global robot_control_vars
    robot = robot_control_vars["robot_controller"]

    if robot is None or not robot.has_exited:
        return []

    try:
        if (t_ns - tf_vars["robot_exit_time_ns"]) < tf_vars["exit_wait_time_ns"]:
            return []
    except TypeError:
        # save robot exit time since it's None
        tf_vars["robot_exit_time_ns"] = t_ns

    # robot has exited: save synaptic weights
    learned_weights_filename = tf_vars["learned_weights_filename"]

    keys_to_save = ['source', 'target', 'weight']

    results = {
        f"{dp_name}_connections_info": {k: connections_dp[dp_name].data[k] for k in keys_to_save}
        for dp_name in tf_vars["connections_dps"]
    }
    # results = {
    #     "object_cells_to_place_cells_connections_info":
    #         {k: connections_dp["object_cells_to_place_cells"].data[k] for k in keys_to_save},
    #     "persistent_cells_to_object_cells_connections_info":
    #         {k: connections_dp["persistent_cells_to_object_cells"].data[k] for k in keys_to_save}
    # }

    t_ms = t_ns * 1e-6
    try:
        # WARNING: skip if file exists (write once)
        with open(learned_weights_filename, "x") as fp:
            json.dump(results, fp)
    except FileExistsError:
        print(f"([{t_ms}] {learned_weights_filename} already exists.")
    else:
        print(f"([{t_ms}] SAVED synaptic weights in {learned_weights_filename}.")

    return []
