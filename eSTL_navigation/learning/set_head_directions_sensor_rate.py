# This TF sets the rate of the poisson generator connected to the head direction cell
# encoding the sector (i.e. one of the 360/robot.num_place_cells) towards which the robot is heading

import math

from nrp_core import TransceiverFunction, EngineDataPack

try:
    import utils
except ImportError:
    import sys
    import os

    # add resources to PYTHONPATH
    resources_path = os.path.join(os.getcwd(), "resources")
    sys.path.append(resources_path)
    import utils


@TransceiverFunction("nest")
def set_head_directions_sensor_rate():
    # global robot_control_vars
    robot = robot_control_vars["robot_controller"]

    try:
        robot_orientation_quaternion = robot.pose["orientation"]  # : np.ndarray
    except TypeError:
        # robot.pose is None
        return []

    current_yaw_rad = utils.yaw_euler_from_quaternion(*robot_orientation_quaternion)

    heading_cell_index = utils.yaw_to_cell_index(current_yaw_rad, robot.num_place_cells)

    poisson_generator_sensor_head_directions_dps = JsonDataPack(
        "poisson_generator_sensor_head_directions", "nest")

    # update rates: 0. except heading_cell_index
    rates = [0.] * robot.num_place_cells
    rates[heading_cell_index] = 30_000.0

    print(
        f"Current HEADING: {math.degrees(current_yaw_rad):.2f}: "
        f"CELL-SECTOR_INDEX: {heading_cell_index}")

    for rate in rates:
        poisson_generator_sensor_head_directions_dps.data.append({"rate": rate})

    return [poisson_generator_sensor_head_directions_dps]
