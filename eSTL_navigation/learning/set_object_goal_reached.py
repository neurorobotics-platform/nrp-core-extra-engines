# This TF set the "reached" key of the current goal_object
# when its pixel count is less than a threshold

from nrp_core import TransceiverFunction

set_object_goal_reached_vars = {"MAX_AREA": 1200}


@SimulationTime(keyword="t_ns")
@TransceiverFunction("gazebo")
def set_object_goal_reached(t_ns):
    # global robot_control_vars
    robot = robot_control_vars["robot_controller"]

    if (goal := robot.goal_object) is None or (goal_info := goal["info"]) is None:
        return []

    if goal_info["area"] > set_object_goal_reached_vars["MAX_AREA"]:
        goal["reached"] = True
        print(f"{t_ns/1e9=:.2f}: {goal['color']}: REACHED")

    return []
