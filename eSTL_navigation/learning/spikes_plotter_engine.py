"""
This Engine implements a spike plotter.

An X server and write rights on /dev/shm (multiprocess) are required.


EngineExtraConfigs:
    - WindowLengthSec: plotter sliding window length in seconds (X axis)
    - NumNeurons: Number of spiking neurons (Y axis)
Input DP:
    - "recorded_spike_events":
"""
import multiprocessing as mp
from typing import Optional, Dict

import numpy as np
from nrp_core.engines.python_grpc import GrpcEngineScript

try:
    import spike_plotter
except ImportError:
    import sys
    import os

    # add resources to PYTHONPATH
    resources_path = os.path.join(os.getcwd(), "resources")
    sys.path.append(resources_path)

    import spike_plotter

from nrpspikesmonitormsgs_pb2 import SpikeEvents

# empty dict as a sentinel object to request queue consumer termination
QUEUE_SENTINEL: Dict = dict()
DEFAULT_TIMESTEP_S: float = 0.02
# default plotter window length in seconds
DEFAULT_WINDOW_LEN_S: float = 10.


class Script(GrpcEngineScript):

    def __init__(self):
        super().__init__()

        self.dt_s: float = DEFAULT_TIMESTEP_S
        self.num_neurons: int = 0

        self.window_len_s: float = DEFAULT_WINDOW_LEN_S

        self.spike_events_msg: Optional[SpikeEvents] = None

        self.plotter_process: Optional[mp.Process] = None
        self.plotter_data_queue: Optional[mp.Queue] = None

    def _load_configuration(self):
        # LOAD configuration
        self.dt_s = float(self._config.get("EngineTimestep", self.dt_s))  # secs
        # engine specific configuration
        self.engine_conf = self._config["EngineExtraConfigs"]
        self.window_len_s = self.engine_conf.get("WindowLengthSec", self.window_len_s)
        self.num_neurons = self.engine_conf.get("NumNeurons", self.num_neurons)

    @staticmethod
    def plotter_process_fun(in_data_queue: mp.JoinableQueue,
                            dt_s: float,
                            window_len: float,
                            num_neurons: int):

        # configure plot
        plotter = spike_plotter.SpikePlotter(
            dt_s,  # t
            window_len,
            num_neurons
        )
        # show plot
        plotter.plt.show(block=False)

        # consume items from queue until sentinel value is received
        # we wait for the plotter to complete its updating (task_done()). Fully synchronous.
        while (datum := in_data_queue.get()) != QUEUE_SENTINEL:
            plotter.update_data(datum)
            plotter.update_fig()
            in_data_queue.task_done()
        else:
            # ack QUEUE_SENTINEL
            in_data_queue.task_done()

        print(f"Plotter: Closing")
        plotter.close()

    def initialize(self):
        print(f"{self._name} is initializing")
        print("Loading configuration...")
        self._load_configuration()
        self._initialize_datapacks()

        # start plotter in a separate process.
        # Matplotlib requires working on the main Thread
        self.plotter_data_queue = mp.JoinableQueue()
        self.plotter_process = mp.Process(target=Script.plotter_process_fun,
                                          args=(self.plotter_data_queue,
                                                self.dt_s,
                                                self.window_len_s,
                                                self.num_neurons
                                                # {populations, colors}
                                                )
                                          )
        self.plotter_process.start()

    @property
    def initialized(self) -> bool:
        return self.plotter_process and self.plotter_process.is_alive()

    def _initialize_datapacks(self):
        # Register input datapack
        # - recorded_spike_events
        self._registerDataPack("recorded_spike_events", SpikeEvents)

    def runLoop(self, _timestep_ns):

        if not self.initialized:
            return

        # RETRIEVE new data packs values
        # - spike_events pose if changed
        self.spike_events_msg = self._getDataPack("recorded_spike_events")
        try:
            # datum: {
            #     "simulationTime": int, # ns
            #     "neurons": np.ndarray[int],
            #     "spikeCounts": np.ndarray[int]
            # }
            # send to new data to plot:
            self.plotter_data_queue.put({
                "simulationTime": self.spike_events_msg.simulationTime,
                "neurons": np.array(self.spike_events_msg.neurons[:]),
                "spikeCounts": np.array(self.spike_events_msg.spikeCounts[:])
            })
        except AttributeError:
            print(f"{self._name}: recorded_spike_events is None")
            return

        # Wait for task being completed. Fully synchronous.
        self.plotter_data_queue.join()

    def shutdown(self):
        if self.initialized:
            # send sentinel to request termination
            self.plotter_data_queue.put(QUEUE_SENTINEL)
            self.plotter_process.join(timeout=3)  # 3 secs
            print(f"Engine '{self._name}' shutdown complete")
        else:
            print(f"Engine '{self._name}' already shutdown")

    def reset(self):
        # reset figure
        print(f"Engine {self._name} is resetting")

        # close subprocess
        self.shutdown()
        # re-initialize
        self.initialize()
