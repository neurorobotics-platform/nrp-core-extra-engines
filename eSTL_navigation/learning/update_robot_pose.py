import numpy as np

from nrp_core import *
from nrp_core.data.nrp_protobuf import GazeboModelDataPack

@EngineDataPack(keyword='husky_model_state', id=DataPackIdentifier('husky', 'gazebo'))
@PreprocessingFunction("gazebo")
def update_robot_pose(husky_model_state):

    # not worth it to check at every loop
    # if not husky_model_state.isUpdated():
    #     print("update_robot_pose(): NO NEW model state ")
    #     return []

    try:
        husky_model_state = husky_model_state.data
    except AttributeError:
        print("update_robot_pose(): NO model state ")
        return []

    # global robot_control_vars
    robot = robot_control_vars["robot_controller"]

    # update pose
    robot.pose = {
        "position": np.array(husky_model_state.position[:]),
        "orientation": np.array(husky_model_state.rotation[:])
    }

    return []
