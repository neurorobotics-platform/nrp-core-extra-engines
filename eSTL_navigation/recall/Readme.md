# A brain-inspired Single Trial Learning (STL) navigation model - RECALL
 
This is the nrp-core experiment implementing the recall phase of the paper [Coppolino, S., and Migliore, M., (2023) An explainable artificial intelligence approach to spatial navigation based on hippocampal circuitry](https://doi.org/10.1016/j.neunet.2023.03.030).

## Description
In this phase the robot navigates towards the exit selecting the correct path.
When reaching a landmark/object in the maze, the brain model indicates the direction that leads to the next object until the exit is found.
To reach an object, the robot is guided by a PID controller on the camera image (azimuthal angle of the target object).

To run the experiment use the following command:
```
# ONLY the first time. Compile the protobuf messages 
$ nrp_compile_protobuf.py --proto_files_path ./proto_msgs

# copy the learned weights from the learning experiment 
$ cp ../learning/learned_synaptic_weights/weights.json ./learned_synaptic_weights/weights.json 

# run the experiment
$ NRPCoreSim -c simulation_config.json 2>log.err # redirect std err to log.err, nest-server is very noisy
# In a separate terminal, start optional gazebo visualization 
$ gzclient
```

## Robot behavior 

The robot behavior is defined by the following state machine:

States are implemented in `resources/robot_states.py`, state transitions are labeled with the conditions that triggers them.
In square brackets are the prerequisites for the transition.

![state machines](diagrams/recall_states.svg){width=50%}


## TFs dataflow
The dataflow among the Transceiver functions is shown in the following graph:

Nodes are labeled with TF names (see `simulation_config.json`),
edges with the datapack name/global variable used by the receiving node; e.g. the `robot_control` TF owns the `robot_controller` variable that is used by the `image_processing` TF.

![TFs dataflow](diagrams/tf_recall_graph.svg){width=50%}

