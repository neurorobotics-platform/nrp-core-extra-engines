import functools
import operator
import os

import nest
import numpy

"""
Initializes PyNN with the minimal neuronal network
"""
nest.set_verbosity("M_DEPRECATED")  # "M_INFO")

NEST_LOCAL_NUM_TRHEADS = 14


# {k: v.tolist() for k, v in populations.items()}
# {'sensor_simple_objects': [1, 2, 3, 4, 5, 6],
#  'sensor_exit_object': [7],
#  'sensor_start_object': [8],
#  'sensor_dead_end_object': [9],
#  'sensor_head_directions': [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27],
#  'object_cells': [28, 29, 30, 31, 32, 33],
#  'persistent_cells': [34, 35, 36, 37, 38, 39],
#  'exit_cells': [40],
#  'dead_end_cells': [41],
#  'place_cells': [42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59],
#  'lateral_inhibition_persistent_neuron_cells': [60, 61, 62, 63, 64, 65],
#  'lateral_and_feedback_inhibition_object_cells': [66],
#  'inhibition_backtracking_cells': [67],
#  'inhibitory_exit_cells': [68],
#  'inhibitory_start_trial_cells': [69],
#  'dopamine_neuron_cell': [70],
#  'inhibitory_dopamine_neuron_cell': [71],
#  'spike_recorder_sensor_simple_objects': [73],
#  'spike_recorder_sensor_exit_object': [74],
#  'spike_recorder_sensor_start_object': [75],
#  'spike_recorder_sensor_dead_end_object': [76],
#  'spike_recorder_sensor_head_directions': [77],
#  'poisson_generator_sensor_simple_objects': [78, 79, 80, 81, 82, 83],
#  'poisson_generator_sensor_exit_object': [84],
#  'poisson_generator_sensor_start_object': [85],
#  'poisson_generator_sensor_dead_end_object': [86],
#  'poisson_generator_sensor_head_directions': [87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99,
#                                               100, 101, 102, 103, 104],
#  'poisson_generator_sensors_objects': [78, 79, 80, 81, 82, 83, 84, 85, 86],
#  'spike_recorder_object_cells': [105],
#  'spike_recorder_persistent_cells': [106],
#  'spike_recorder_exit_cells': [107],
#  'spike_recorder_dead_end_cells': [108],
#  'spike_recorder_place_cells': [109],
#  'spike_recorder_lateral_inhibition_persistent_neuron_cells': [110],
#  'spike_recorder_lateral_and_feedback_inhibition_object_cells': [111],
#  'spike_recorder_inhibition_backtracking_cells': [112],
#  'spike_recorder_inhibitory_exit_cells': [113],
#  'spike_recorder_inhibitory_start_trial_cells': [114],
#  'spike_recorder_dopamine_neuron_cell': [115],
#  'spike_recorder_inhibitory_dopamine_neuron_cell': [116],
#  'spike_recorders': [73, 74, 75, 76, 77, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115,
#                      116]}

class Brain:

    @staticmethod
    def load_network(status_synaptic_connections, learned_weights_filename, synapsesCollection):

        def reorder_arrays(list1, list2):
            if len(list1) != len(list2):
                raise ValueError("The lists must have the same shape and contain the same elements")

            reordered_list2 = [None] * len(list2)
            found_indices = set()

            for i in range(len(list1)):
                pair1 = list1[i]
                for j in range(len(list2)):
                    if list2[j][:2] == pair1 and j not in found_indices:
                        reordered_list2[i] = list2[j]
                        found_indices.add(j)
                        break

            return reordered_list2

        def get_ordered_weight_dict(status_synaptic_connections, weights_dict):

            weight_file_T = []

            for i in range(len(weights_dict["source"])):
                list_to_append = [weights_dict["source"][i],
                                  weights_dict["target"][i],
                                  weights_dict["weight"][i]]
                weight_file_T.append(list_to_append)

            source_target = []
            for single_synaptic_status in status_synaptic_connections:
                source_target.append(
                    [single_synaptic_status['source'], single_synaptic_status['target']])

            reordered_list2 = reorder_arrays(source_target, weight_file_T)
            if status_synaptic_connections[0]['synapse_model'] == 'standard_persistent':
                list_weight_dict = [{'weight': conn[2], 'lambda': 0.0} for conn in reordered_list2]
            elif status_synaptic_connections[0]['synapse_model'] == 'standard':
                list_weight_dict = [{'weight': conn[2]} for conn in reordered_list2]
            else:
                raise ValueError(
                    f"The synaptic model {status_synaptic_connections[0]['synapse_model']} is not yet supported for loading, you can change the dict in this line to load your synaptic model")

            return list_weight_dict

        import json
        # Opening JSON file
        with open(learned_weights_filename) as json_file:
            all_data = json.load(json_file)

        # obj2place_weights

        list_dict_weight_pc2obj = get_ordered_weight_dict(status_synaptic_connections[0], all_data[
            'persistent_cells_to_object_cells_connections_info'])
        list_dict_weight_obj2place = get_ordered_weight_dict(status_synaptic_connections[1],
                                                             all_data[
                                                                 'object_cells_to_place_cells_connections_info'])

        nest.SetStatus(synapsesCollection[0], list_dict_weight_pc2obj)
        nest.SetStatus(synapsesCollection[1], list_dict_weight_obj2place)

    @staticmethod
    def _connect_pre_post_for_lateral_inhibition(nodeCollection_pre, nodeCollection_post,
                                                 syn_spec):
        # this function takes as input 2 NodeCollection and outputs the NodeCollection that can be used to set up a lateral inhibition system
        # if the neurons id of nodeCollection_pre are [0, 1, 2] and the ones of nodeCollection_post are [3, 4, 5] then the output
        # would be [0, 0, 1, 1, 2, 2] [4, 5, 3, 5, 3, 4] so the index 0 of the nodeCollection_pre is not connected to the index 0 of the second one
        # and so on
        nodeCollection_pre_numpy = numpy.array(nodeCollection_pre, dtype=int)
        nodeCollection_post_numpy = numpy.array(nodeCollection_post, dtype=int)

        n_neurons_in_the_node_collections = len(nodeCollection_pre_numpy)
        complete_mask = numpy.ones(n_neurons_in_the_node_collections, dtype=bool)
        new_nodeCollection_pre = numpy.array([], dtype=int)
        new_nodeCollection_post = numpy.array([], dtype=int)

        for i in range(n_neurons_in_the_node_collections):
            curr_mask = numpy.copy(complete_mask)
            curr_mask[i] = False
            inverted_curr_mask = numpy.logical_not(curr_mask)

            new_nodeCollection_pre = numpy.concatenate(
                (new_nodeCollection_pre, nodeCollection_pre_numpy[
                    inverted_curr_mask] * numpy.ones(curr_mask.sum(), dtype=int)))
            # new_nodeCollection_pre += nodeCollection_pre[inverted_curr_mask] * curr_mask.sum()
            new_nodeCollection_post = numpy.concatenate(
                (new_nodeCollection_post, nodeCollection_post_numpy[curr_mask]))

        if 'weight' in syn_spec:
            syn_spec['weight'] = numpy.ones(len(new_nodeCollection_pre)) * syn_spec['weight']
            syn_spec['delay'] = numpy.ones(len(new_nodeCollection_pre)) * syn_spec['delay']

        nest.Connect(new_nodeCollection_pre, new_nodeCollection_post, 'one_to_one',
                     syn_spec=syn_spec)

    def __init__(self, learned_weights_filename=None, nest_local_num_threads=14):

        eliminate_plasticity = bool(learned_weights_filename)

        nest.ResetKernel()

        synapse_model_key = "synapse_model"
        nest.local_num_threads = nest_local_num_threads

        # number of neurons parameters
        n_colors = 6
        n_encoded_head_direction = 18
        delay = 0.1

        ### NEURONS PARAMETERS

        # Define neuron parameters
        SENSORPARAMS = {'C_m': 1000.,
                        'E_L': -65.0,
                        'V_reset': -65.0,
                        'V_th': -50.0,
                        't_ref': 1.0,
                        'tau_syn_ex': 5.0,
                        'tau_syn_in': 5.0,
                        'tau_minus': 30, }

        # set neuron parameters
        neuron_params = {'C_m': SENSORPARAMS['C_m'],
                         'E_L': SENSORPARAMS['E_L'],
                         'V_reset': SENSORPARAMS['V_reset'],
                         'V_th': SENSORPARAMS['V_th'],
                         'tau_syn_ex': SENSORPARAMS['tau_syn_ex'],
                         'tau_syn_in': SENSORPARAMS['tau_syn_in'],
                         't_ref': SENSORPARAMS['t_ref']}

        nest.SetDefaults("iaf_cond_alpha", neuron_params)

        ### SYNAPTIC PARAMETERS

        # Define the synaptic parameters dictionary
        synaptic_params = {
            'A_plus': 1.5,
            'A_minus': 1.5,  # 1.7,
            'Wmax': 30.,  # 40
            'weight': 0.01,
        }

        # Create the STDP synapse model
        nest.CopyModel("stdp_dopamine_synapse", "standard", params=synaptic_params)

        # Define the persistent synaptic parameters dictionary
        synaptic_params_persistent = {
            'weight': 0.1,
            'delay': 5.5,
            'tau_plus': 15.0,
            'Wmax': 300.00,
            'lambda': 2.00,  # 2.0,
            'alpha': 0.3,  # 0.4
        }

        # Create the STDP synapse model
        nest.CopyModel("stdp_synapse", "standard_persistent", params=synaptic_params_persistent)

        if eliminate_plasticity:  # set plasticity to 0 --> RECALL MODE
            nest.SetDefaults("standard", {'A_plus': 0., 'A_minus': 0.})
            nest.SetDefaults("standard_persistent", {'lambda': 0.})

        SYNAPSE_PARAMS = {
            'weight_poisson_generator': 0.15,
            'rate_gain_poisson_generator': 30 * 1000.,
            'sensor_simple_object_to_object_cells_weight': 20.0,  # 7.0
            'sensor_simple_object_to_persistent_cells_weight': 3.5,  # 1.2
            'sensor_simple_object_to_lateral_inhibition_persistent_neuron_cells_weight': 6.5,
            # 0.03,
            'sensor_simple_object_to_dopamine_neuron_cell_weight': 13.,  # 1.55,
            'sensor_simple_object_to_inhibitory_dopamine_neuron_cell_weight': 5.,  # 1.22
            'sensor_simple_object_to_lateral_and_feedback_inhibition_object_cells_weight': 0.,
            # 0.03,

            'sensor_dead_end_object_to_dead_end_cells_weight': 5.5,  # 0.03
            'sensor_dead_end_object_to_persistent_cells_weight': 13.,  # 0.036,
            'sensor_start_object_to_inhibitory_start_trial_cells_weight': 20.0,  # 0.03
            'sensor_start_object_to_auxiliary_start_trial_cells_weight': 1.8,  # 0.03
            'sensor_exit_object_to_exit_cells_weight': 3.8,  # 0.1
            'sensor_exit_object_to_inhibitory_exit_cells_weight': 3.8,  # 0.3

            'sensor_head_directions_to_place_cells_weight': 2.,  # 0.05
            'dead_end_cells_to_sensor_head_directions_weight': -1000.0,  # -4.50,

            'object_cells_to_lateral_and_feedback_inhibition_object_cells_weight': 200.0,  # 13.0

            'persistent_cells_to_persistent_cells_weight': 200.,  # 0.2
            'persistent_cells_to_inhibition_backtracking_cells_weight': 2.5,  # 0.05

            'dead_end_cells_to_dead_end_cells_weight': 200.,  # 0.3
            'dead_end_cells_to_persistent_cells_weight': -200.,  # -20.00,
            'dead_end_cells_to_lateral_and_feedback_inhibition_object_cells': -200.0,  # -10.
            'dead_end_cells_to_place_cells_weight': -1.5,  # 0.5
            'dead_end_cells_to_inhibitory_dopamine_neuron_cell_weight': -10.0,  # 10.0,

            'inhibitory_dopamine_neuron_cell_to_inhibitory_dopamine_neuron_cell_weight': 10.,
            #

            'lateral_inhibition_persistent_neuron_cells_to_persistent_cells_weight': -12.,  # -12.0,
            'lateral_and_feedback_inhibition_object_cells_to_object_cells_weight': -25.0,  # -20,

            'inhibitory_start_trial_cells_to_persistent_cells_weight': -2.0,  # -9.0,
            'inhibitory_start_trial_cells_to_dead_end_cells_weight': -100.0,  # -9.0,
            'inhibitory_exit_cells_to_persistent_cells_weight': -300.0,  # -10.
            'inhibitory_dopamine_neuron_cell_to_dopamine_neuron_cell_weight': -1000.0,  #

            'delay': delay,
            'I_e_lateral_and_feedback_inhibition_object_cells': 250.0,
        }

        self.weight_poisson_generator = SYNAPSE_PARAMS['weight_poisson_generator']
        self.rates_gain = SYNAPSE_PARAMS['rate_gain_poisson_generator']

        Cm_inhibitory_neuron = 50.

        ### SENSOR NEURONS connected directly to the sensors

        # sensor_simple_object [RED, BLUE, YELLOW, CYAN, ORANGE]
        sensor_simple_objects = nest.Create("iaf_cond_alpha", n_colors)

        # sensor_neuron exit [GREEN]
        sensor_exit_object = nest.Create("iaf_cond_alpha", 1)

        # sensor_neuron dead end [MAGENTA]
        sensor_start_object = nest.Create("iaf_cond_alpha", 1)

        # sensor_neuron restart trial [PURPLE]
        sensor_dead_end_object = nest.Create("iaf_cond_alpha", 1)

        # sensor_neuron for the direction of the head [18 possible direction encoded]
        sensor_head_directions = nest.Create("iaf_cond_alpha", n_encoded_head_direction)

        ### BRAIN NEURONS

        ## EXITATORY NEURONS

        # object_cells_colors [RED, BLUE, YELLOW, CYAN, ORANGE]
        object_cells = nest.Create("iaf_cond_alpha", n_colors)
        # nest.SetStatus(object_cells, {'I_e':250.0})

        # persistent_neurons [RED, BLUE, YELLOW, CYAN, BLACK]
        persistent_cells = nest.Create("iaf_cond_alpha", n_colors)

        # neuron responding to exit [GREEN]
        exit_cells = nest.Create("iaf_cond_alpha", 1)

        # neuron responding to restarting trial [PURPLE]
        dead_end_cells = nest.Create("iaf_cond_alpha", 1, {'C_m': Cm_inhibitory_neuron})

        # neuron responding to the direction of the head [18 possible direction encoded]
        place_cells = nest.Create("iaf_cond_alpha", n_encoded_head_direction)

        ## INHIBITORY NEURONS

        # neurons for lateral inhibition for persistend neurons [RED, BLUE, YELLOW, CYAN, BLACK]
        lateral_inhibition_persistent_neuron_cells = nest.Create("iaf_cond_alpha", n_colors,
                                                                 {'C_m': Cm_inhibitory_neuron})

        # neuron for lateral and feedback inhibition on the object cells layer [INa] (INa in the paper), LF: not sure why we need it
        lateral_and_feedback_inhibition_object_cells = nest.Create("iaf_cond_alpha", 1,
                                                                   {'C_m': Cm_inhibitory_neuron,
                                                                    'I_e': SYNAPSE_PARAMS[
                                                                        'I_e_lateral_and_feedback_inhibition_object_cells']})

        # neuron responding to the activity of all persistend cells at the same time (AND) [INb] (INb in the paper),
        # active after an error signal is obtained
        inhibition_backtracking_cells = nest.Create("iaf_cond_alpha", 1)

        # neuron connected to the exit sensor that inhibits persistent cells (to avoid activation of the AND gate?)
        inhibitory_exit_cells = nest.Create("iaf_cond_alpha", 1)

        # neuron connected to the restart trial sensor that inhibits persistent cells
        inhibitory_start_trial_cells = nest.Create("iaf_cond_alpha", 1)

        ### Auxiliary NEURONS

        # fires when the object is close enough (close = more pixels) then it stops the robot
        dopamine_neuron_cell = nest.Create("iaf_cond_alpha", 1, {'C_m': 500.0, 'g_L': 20.0})

        # fires to stop the feedback
        inhibitory_dopamine_neuron_cell = nest.Create("iaf_cond_alpha", 1,
                                                      {'C_m': 500.0, 'g_L': 20.0})

        ### Volume transmitter for standard syn
        # not a real neuron, won't belong to any population
        volume_transmitter = nest.Create("volume_transmitter", {"deliver_interval": 1})
        nest.SetDefaults("standard", {'vt': volume_transmitter.tolist()[0]})

        ### CONNECTIONS PARAMETERS

        # sensor_simple_object

        params_syn_sensor_simple_object_to_object_cells = {synapse_model_key: "static_synapse",
                                                           "weight": SYNAPSE_PARAMS[
                                                               'sensor_simple_object_to_object_cells_weight'],
                                                           "delay": SYNAPSE_PARAMS['delay']}
        params_syn_sensor_simple_object_to_persistent_cells = {synapse_model_key: "static_synapse",
                                                               "weight": SYNAPSE_PARAMS[
                                                                   'sensor_simple_object_to_persistent_cells_weight'],
                                                               "delay": SYNAPSE_PARAMS['delay']}
        params_syn_sensor_simple_object_to_lateral_inhibition_persistent_neuron_cells = {
            synapse_model_key: "static_synapse", "weight": SYNAPSE_PARAMS[
                'sensor_simple_object_to_lateral_inhibition_persistent_neuron_cells_weight'],
            "delay": SYNAPSE_PARAMS['delay']}

        params_syn_sensor_simple_object_to_dopamine_neuron_cell = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'sensor_simple_object_to_dopamine_neuron_cell_weight'],
            "delay": SYNAPSE_PARAMS['delay']}
        params_syn_sensor_simple_object_to_lateral_and_feedback_inhibition_object_cells = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'sensor_simple_object_to_lateral_and_feedback_inhibition_object_cells_weight'],
            "delay": SYNAPSE_PARAMS['delay']}

        params_syn_sensor_simple_object_to_inhibitory_dopamine_neuron_cell = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'sensor_simple_object_to_inhibitory_dopamine_neuron_cell_weight'],
            "delay": SYNAPSE_PARAMS['delay']}
        params_syn_inhibitory_dopamine_neuron_cell_to_inhibitory_dopamine_neuron_cell = {
            synapse_model_key: "tsodyks_synapse",
            "weight": SYNAPSE_PARAMS[
                'inhibitory_dopamine_neuron_cell_to_inhibitory_dopamine_neuron_cell_weight'],
            "delay": 50.0, "U": 0.5,  # "delay": 55.0, "U": 0.5,
            "tau_rec": 100.0,
            "tau_fac": 10.0}  # "tau_fac": 10.0}

        # SENSOR HEAD DIRECTIONS
        params_syn_sensor_head_directions_to_place_cells = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'sensor_head_directions_to_place_cells_weight'],
            "delay": SYNAPSE_PARAMS['delay']}

        # OBJECT_CELLS
        params_syn_object_cells_to_place_cells = {synapse_model_key: "standard"}

        params_syn_object_cells_to_lateral_and_feedback_inhibition_object_cells = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'object_cells_to_lateral_and_feedback_inhibition_object_cells_weight'],
            "delay": SYNAPSE_PARAMS['delay']
        }

        # PERSISTENT_CELLS
        params_syn_persistent_cells_to_object_cells = {synapse_model_key: "standard_persistent"}
        params_syn_persistent_cells_to_persistent_cells = {synapse_model_key: "tsodyks_synapse",
                                                           "weight": SYNAPSE_PARAMS[
                                                               'persistent_cells_to_persistent_cells_weight'],
                                                           "delay": 55.0, "U": 0.5,
                                                           "tau_rec": 100.0,
                                                           "tau_fac": 10.0}
        params_syn_persistent_cells_to_inhibition_backtracking_cells = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'persistent_cells_to_inhibition_backtracking_cells_weight'],
            "delay": SYNAPSE_PARAMS['delay']}

        params_syn_lateral_inhibition_persistent_neuron_cells_to_persistent_cells = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'lateral_inhibition_persistent_neuron_cells_to_persistent_cells_weight'],
            "delay": SYNAPSE_PARAMS['delay']}
        # INa
        params_syn_lateral_and_feedback_inhibition_object_cells_to_object_cells = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'lateral_and_feedback_inhibition_object_cells_to_object_cells_weight'],
            "delay": SYNAPSE_PARAMS['delay']}
        params_syn_dead_end_cells_to_lateral_and_feedback_inhibition_object_cells = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'dead_end_cells_to_lateral_and_feedback_inhibition_object_cells'],
            "delay": SYNAPSE_PARAMS['delay']}

        # RESTART
        params_syn_sensor_dead_end_object_to_dead_end_cells = {synapse_model_key: "static_synapse",
                                                               "weight": SYNAPSE_PARAMS[
                                                                   'sensor_dead_end_object_to_dead_end_cells_weight'],
                                                               "delay": SYNAPSE_PARAMS['delay']}
        params_syn_sensor_dead_end_object_to_persistent_cells = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'sensor_dead_end_object_to_persistent_cells_weight'],
            "delay": SYNAPSE_PARAMS['delay']}

        params_dead_end_cells_to_dead_end_cells = {synapse_model_key: "tsodyks_synapse",
                                                   "weight": SYNAPSE_PARAMS[
                                                       'dead_end_cells_to_dead_end_cells_weight'],
                                                   "delay": 55.0, "U": 0.5, "tau_rec": 100.0,
                                                   "tau_fac": 10.0}
        params_syn_dead_end_cells_to_persistent_cells = {synapse_model_key: "static_synapse",
                                                         "weight": SYNAPSE_PARAMS[
                                                             'dead_end_cells_to_persistent_cells_weight'],
                                                         "delay": SYNAPSE_PARAMS['delay']}

        params_syn_dead_end_cells_to_sensor_head_directions = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'dead_end_cells_to_sensor_head_directions_weight'],
            "delay": SYNAPSE_PARAMS['delay']}

        # DEAD END
        params_syn_sensor_start_object_to_inhibitory_start_trial_cells = {
            synapse_model_key: "static_synapse", "weight": SYNAPSE_PARAMS[
                'sensor_start_object_to_inhibitory_start_trial_cells_weight'],
            "delay": SYNAPSE_PARAMS['delay']}
        params_syn_inhibitory_start_trial_cells_to_persistent_cells = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'inhibitory_start_trial_cells_to_persistent_cells_weight'],
            "delay": SYNAPSE_PARAMS['delay']}
        params_syn_inhibitory_start_trial_cells_to_dead_end_cells = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'inhibitory_start_trial_cells_to_dead_end_cells_weight'],
            "delay": SYNAPSE_PARAMS['delay']}

        params_syn_dead_end_cells_to_place_cells = {synapse_model_key: "static_synapse",
                                                    "weight": SYNAPSE_PARAMS[
                                                        'dead_end_cells_to_place_cells_weight'],
                                                    "delay": SYNAPSE_PARAMS['delay']}
        params_syn_dead_end_cells_to_inhibitory_dopamine_neuron_cell = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'dead_end_cells_to_inhibitory_dopamine_neuron_cell_weight'],
            "delay": SYNAPSE_PARAMS['delay']}

        # EXIT
        params_syn_sensor_exit_object_to_exit_cells = {synapse_model_key: "static_synapse",
                                                       "weight": SYNAPSE_PARAMS[
                                                           'sensor_exit_object_to_exit_cells_weight'],
                                                       "delay": SYNAPSE_PARAMS['delay']}
        params_syn_sensor_exit_object_to_inhibitory_exit_cells = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'sensor_exit_object_to_inhibitory_exit_cells_weight'],
            "delay": SYNAPSE_PARAMS['delay']}
        params_syn_exit_cells_to_place_cells = {synapse_model_key: "standard"}
        params_syn_inhibitory_exit_cells_to_persistent_cells = {synapse_model_key: "static_synapse",
                                                                "weight": SYNAPSE_PARAMS[
                                                                    'inhibitory_exit_cells_to_persistent_cells_weight'],
                                                                "delay": SYNAPSE_PARAMS['delay']}

        # volume_transmitter
        params_syn_inhibitory_dopamine_neuron_cell_to_dopamine_neuron_cell = {
            synapse_model_key: "static_synapse",
            "weight": SYNAPSE_PARAMS[
                'inhibitory_dopamine_neuron_cell_to_dopamine_neuron_cell_weight'],
            "delay": SYNAPSE_PARAMS['delay']}

        ### CONNECTING NEURONS

        # sensor_simple_object
        nest.Connect(sensor_simple_objects, object_cells, "one_to_one",
                     syn_spec=params_syn_sensor_simple_object_to_object_cells)
        nest.Connect(sensor_simple_objects, persistent_cells, "one_to_one",
                     syn_spec=params_syn_sensor_simple_object_to_persistent_cells)
        nest.Connect(sensor_simple_objects, lateral_inhibition_persistent_neuron_cells,
                     "one_to_one",
                     syn_spec=params_syn_sensor_simple_object_to_lateral_inhibition_persistent_neuron_cells)

        nest.Connect(sensor_simple_objects, dopamine_neuron_cell, "all_to_all",
                     syn_spec=params_syn_sensor_simple_object_to_dopamine_neuron_cell)
        nest.Connect(sensor_simple_objects, inhibitory_dopamine_neuron_cell, "all_to_all",
                     syn_spec=params_syn_sensor_simple_object_to_inhibitory_dopamine_neuron_cell)
        nest.Connect(sensor_simple_objects, lateral_and_feedback_inhibition_object_cells,
                     "all_to_all",
                     syn_spec=params_syn_sensor_simple_object_to_lateral_and_feedback_inhibition_object_cells)

        # sensor_head_directions
        nest.Connect(sensor_head_directions, place_cells, "one_to_one",
                     syn_spec=params_syn_sensor_head_directions_to_place_cells)

        # OBJECT_CELLS

        nest.Connect(object_cells, place_cells, "all_to_all",
                     syn_spec=params_syn_object_cells_to_place_cells)

        nest.Connect(object_cells, lateral_and_feedback_inhibition_object_cells, "all_to_all",
                     syn_spec=params_syn_object_cells_to_lateral_and_feedback_inhibition_object_cells)

        # PERSISTENT_CELLS
        self._connect_pre_post_for_lateral_inhibition(persistent_cells,
                                                      object_cells,
                                                      params_syn_persistent_cells_to_object_cells)

        nest.Connect(persistent_cells, persistent_cells, "one_to_one",
                     syn_spec=params_syn_persistent_cells_to_persistent_cells)
        nest.Connect(persistent_cells, inhibition_backtracking_cells, "all_to_all",
                     syn_spec=params_syn_persistent_cells_to_inhibition_backtracking_cells)
        self._connect_pre_post_for_lateral_inhibition(lateral_inhibition_persistent_neuron_cells,
                                                      persistent_cells,
                                                      params_syn_lateral_inhibition_persistent_neuron_cells_to_persistent_cells)

        # INa
        nest.Connect(lateral_and_feedback_inhibition_object_cells, object_cells, "all_to_all",
                     syn_spec=params_syn_lateral_and_feedback_inhibition_object_cells_to_object_cells)

        # DEAD END
        nest.Connect(sensor_dead_end_object, dead_end_cells, "one_to_one",
                     syn_spec=params_syn_sensor_dead_end_object_to_dead_end_cells)
        nest.Connect(sensor_dead_end_object, persistent_cells, "all_to_all",
                     syn_spec=params_syn_sensor_dead_end_object_to_persistent_cells)
        nest.Connect(dead_end_cells, dead_end_cells, "one_to_one",
                     syn_spec=params_dead_end_cells_to_dead_end_cells)
        nest.Connect(dead_end_cells, persistent_cells, "all_to_all",
                     syn_spec=params_syn_dead_end_cells_to_persistent_cells)
        nest.Connect(dead_end_cells, lateral_and_feedback_inhibition_object_cells, "one_to_one",
                     syn_spec=params_syn_dead_end_cells_to_lateral_and_feedback_inhibition_object_cells)
        nest.Connect(dead_end_cells, sensor_head_directions, "all_to_all",
                     syn_spec=params_syn_dead_end_cells_to_sensor_head_directions)
        nest.Connect(dead_end_cells, place_cells, "all_to_all",
                     syn_spec=params_syn_dead_end_cells_to_place_cells)
        nest.Connect(dead_end_cells, inhibitory_dopamine_neuron_cell, "all_to_all",
                     syn_spec=params_syn_dead_end_cells_to_inhibitory_dopamine_neuron_cell)

        # RESTART
        nest.Connect(sensor_start_object, inhibitory_start_trial_cells, "one_to_one",
                     syn_spec=params_syn_sensor_start_object_to_inhibitory_start_trial_cells)
        nest.Connect(inhibitory_start_trial_cells, persistent_cells, "all_to_all",
                     syn_spec=params_syn_inhibitory_start_trial_cells_to_persistent_cells)
        nest.Connect(inhibitory_start_trial_cells, dead_end_cells, "one_to_one",
                     syn_spec=params_syn_inhibitory_start_trial_cells_to_dead_end_cells)

        # EXIT
        nest.Connect(sensor_exit_object, exit_cells, "one_to_one",
                     syn_spec=params_syn_sensor_exit_object_to_exit_cells)
        nest.Connect(sensor_exit_object, inhibitory_exit_cells, "one_to_one",
                     syn_spec=params_syn_sensor_exit_object_to_inhibitory_exit_cells)
        nest.Connect(inhibitory_exit_cells, persistent_cells, "all_to_all",
                     syn_spec=params_syn_inhibitory_exit_cells_to_persistent_cells)

        # volume_transmitter
        nest.Connect(inhibitory_dopamine_neuron_cell, dopamine_neuron_cell, "all_to_all",
                     syn_spec=params_syn_inhibitory_dopamine_neuron_cell_to_dopamine_neuron_cell)
        nest.Connect(inhibitory_dopamine_neuron_cell, inhibitory_dopamine_neuron_cell, "all_to_all",
                     syn_spec=params_syn_inhibitory_dopamine_neuron_cell_to_inhibitory_dopamine_neuron_cell)
        nest.Connect(dopamine_neuron_cell, volume_transmitter, "one_to_one")

        ## SETTING INPUTS
        self.input_sensors = {
            'sensor_simple_objects': sensor_simple_objects,
            'sensor_exit_object': sensor_exit_object,
            'sensor_start_object': sensor_start_object,
            'sensor_dead_end_object': sensor_dead_end_object,
            'sensor_head_directions': sensor_head_directions
        }

        self.spike_recorders_input_sensors, self.poisson_generator_input_sensor = self.initialize_neuron_dict(
            self.input_sensors, is_input=True)

        # Group objects poisson generators in a population so to set their rate
        # in one SetStatus REST call to nest-server instead of one call per generator.
        # order is same of the keys in self.input_sensors (dicts preserve key insertion order)
        poisson_generator_sensors_objects_list = [rec for name, rec
                                                  in self.poisson_generator_input_sensor.items()
                                                  if "object" in name]
        self.poisson_generator_sensors_objects = {
            "poisson_generator_sensors_objects": functools.reduce(operator.add,
                                                                  poisson_generator_sensors_objects_list)}

        ## SETTING BRAIN NEURONS
        self.brain_neurons = {
            'object_cells': object_cells,
            'persistent_cells': persistent_cells,
            'exit_cells': exit_cells,
            'dead_end_cells': dead_end_cells,
            'place_cells': place_cells,
            'lateral_inhibition_persistent_neuron_cells': lateral_inhibition_persistent_neuron_cells,
            'lateral_and_feedback_inhibition_object_cells': lateral_and_feedback_inhibition_object_cells,
            'inhibition_backtracking_cells': inhibition_backtracking_cells,
            'inhibitory_exit_cells': inhibitory_exit_cells,
            'inhibitory_start_trial_cells': inhibitory_start_trial_cells,
            'dopamine_neuron_cell': dopamine_neuron_cell,
            'inhibitory_dopamine_neuron_cell': inhibitory_dopamine_neuron_cell,
        }
        self.spike_recorders_brain_neurons, _ = self.initialize_neuron_dict(self.brain_neurons)

        self.spike_recorders = {
            "spike_recorders": functools.reduce(operator.add,
                                                [*self.spike_recorders_input_sensors.values(),
                                                 *self.spike_recorders_brain_neurons.values()]
                                                )}

        # populations whose connections should be monitored (e.g. record their synaptic weights)
        # with GetConnection(source, target)
        # "source" and "target" MUST be in self.all_neurons
        self.all_connections = {
            "persistent_cells_to_object_cells": {"source": "persistent_cells",
                                                 "target": "object_cells"},
            "object_cells_to_place_cells": {"source": "object_cells",
                                            "target": "place_cells"}
        }

        self.all_neurons = {**self.input_sensors,
                            **self.brain_neurons,
                            **self.spike_recorders_input_sensors,
                            **self.poisson_generator_input_sensor,
                            **self.poisson_generator_sensors_objects,
                            **self.spike_recorders_brain_neurons,
                            **self.spike_recorders}

        self.starting_time = 0.0

        ## Load network
        if learned_weights_filename:
            conn1 = nest.GetConnections(persistent_cells, object_cells)
            conn2 = nest.GetConnections(object_cells, place_cells)

            status_conn1 = nest.GetStatus(conn1)
            status_conn2 = nest.GetStatus(conn2)

            status_synaptic_connections = [status_conn1] + [status_conn2]
            all_synapses_connections = [conn1] + [conn2]

            self.load_network(status_synaptic_connections, learned_weights_filename,
                              all_synapses_connections)

    @staticmethod
    def generate_spike_recorder(neuron_dict):
        spike_recorders_dict = {}
        for name, neurons in neuron_dict.items():
            recorder_name = f"spike_recorder_{name}"
            spike_recorders_dict[recorder_name] = nest.Create("spike_recorder")
            nest.Connect(neurons, spike_recorders_dict[recorder_name])

        return spike_recorders_dict

    def generate_poisson_generator(self, neuron_dict, generator_type="poisson_generator"):
        poisson_generators_dict = {}
        for name, neurons in neuron_dict.items():
            generator_name = f"poisson_generator_{name}"
            poisson_generators_dict[generator_name] = nest.Create(generator_type,
                                                                  n=len(neurons))
            nest.Connect(poisson_generators_dict[generator_name], neurons, "one_to_one",
                         {"weight": self.weight_poisson_generator})
        return poisson_generators_dict

    def initialize_neuron_dict(self, neuron_dict, is_input=False):
        spike_recorders_dict = self.generate_spike_recorder(neuron_dict)
        if is_input:
            poisson_generator_input_dict = self.generate_poisson_generator(neuron_dict)
            return spike_recorders_dict, poisson_generator_input_dict
        else:
            return spike_recorders_dict, {}


brain = Brain(learned_weights_filename=os.path.join("learned_synaptic_weights", "weights.json"),
              nest_local_num_threads=NEST_LOCAL_NUM_TRHEADS)

populations = brain.all_neurons
connections = brain.all_connections

# uncomment to print the neurons IDs (top of the file) when changing the network.
# then update place_cells_monitor_vars["first_place_cell_id"] in place_cells_monitor.py
# breakpoint()
# print({k: v.tolist() for k, v in populations.items()})
