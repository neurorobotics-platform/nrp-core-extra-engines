# This TF converts the size of the objects as recorded by the robot camera
# to rates of the sensor_objects poisson generators and
# sets them with the "poisson_generator_sensors_objects" datapack


import itertools
from nrp_core import TransceiverFunction, EngineDataPack
from nrp_core.data.nrp_json import JsonDataPack
import math


def __gaussian(x, mi=0.055, sig=0.03):
    g = (math.exp(-math.pow(x - mi, 2.) / (2 * math.pow(sig, 2.))) * 35_000.0) - 6519.466 + 900.
    return max(g, 0.)


global CROPPED_IMAGE_SHAPE
camera_sensor_transmit_vars = {"image_size": CROPPED_IMAGE_SHAPE[0] * CROPPED_IMAGE_SHAPE[1],
                               "gaussian": __gaussian}


@TransceiverFunction("nest")
def camera_sensor_transmit():
    tf_vars = camera_sensor_transmit_vars

    # global robot_control_vars
    robot = robot_control_vars["robot_controller"]

    poisson_generator_sensor_objects_dps = JsonDataPack("poisson_generator_sensors_objects", "nest")

    # compute sensors_objects poisson generators rates for every object (i.e. color)
    # iterate over all objects (simple and special, intersection may be not empty)
    for object_id in itertools.chain(robot.simple_objects_list, robot.special_objects_list):
        object_info = robot.visible_objects_info_dict.get(object_id, None)
        normalized_object_area = (object_info["area"] if object_info is not None else 0.) / tf_vars[
            "image_size"]
        poisson_generator_sensor_objects_dps.data.append(
            {"rate": round(tf_vars["gaussian"](normalized_object_area),
                           ndigits=4)
             }
        )

    return [poisson_generator_sensor_objects_dps]
