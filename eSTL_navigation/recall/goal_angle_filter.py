from nrp_core import *

goal_angle_filter_vars = {'filtered_goal_angle': None,
                          'prev_goal_angle': None,
                          'filter_alpha': .5}


@TransceiverFunction("gazebo")  # nest
def goal_angle_filter():
    tf_vars = goal_angle_filter_vars

    robot = robot_control_vars["robot_controller"]

    if not isinstance(robot.state, (robot_states.MoveForwardState,
                                    robot_states.AlignWithDirectionState)):
        # SKIP IF robot is not aligning with direction
        return []

    goal_angle = robot.goal_direction_deg

    try:
        tf_vars['filtered_goal_angle'] = tf_vars["filter_alpha"] * goal_angle + (1 - tf_vars["filter_alpha"]) * tf_vars['prev_goal_angle']
        print(f"goal_angle_filter(): {goal_angle=}, filtered_goal_angle={tf_vars['filtered_goal_angle']:.2f}")
    except TypeError:
        # goal_angle or prev_goal_angle is None
        if tf_vars['prev_goal_angle'] is None:
            tf_vars['prev_goal_angle'] = goal_angle
    else:
        tf_vars['prev_goal_angle'] = tf_vars['filtered_goal_angle']

    return []
