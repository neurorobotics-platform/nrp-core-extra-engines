import numpy as np
from PIL import Image

from nrp_core import SimulationIteration, PreprocessingFunction, EngineDataPack, DataPackIdentifier

try:
    import color_detector
except ImportError:
    import sys
    import os

    # add resources to PYTHONPATH
    resources_path = os.path.join(os.getcwd(), "resources")
    sys.path.append(resources_path)

    import color_detector

CROPPED_IMAGE_SHAPE = (160, 320)  # rows, columns. Cropped from 240x320

image_processing_vars = {"show_image": False,
                         "show_decorated_image": False,
                         "detector": color_detector.create_detector(),
                         "black_mask": np.zeros(CROPPED_IMAGE_SHAPE, dtype=np.uint8)}


@SimulationIteration("sim_iteration")
@EngineDataPack(keyword='husky_camera',
                id=DataPackIdentifier('husky::eye_vision_camera::camera', 'gazebo'))
@PreprocessingFunction("gazebo")
def image_processing(husky_camera, sim_iteration):

    if not husky_camera.isUpdated():
        print("image_processing(): NO NEW image ")
        return []

    try:
        camera_data = husky_camera.data
    except AttributeError:
        print("image_processing(): NO image ")
        return []

    def compute_target_color_info(target_keypoint, target_color):
        azimuth, _ = robot.camera.pixel2angle(target_keypoint.pt[0], target_keypoint.pt[1])
        return {"color": target_color,
                "azimuth": int(azimuth),
                "area": color_detector.keypoint_area(target_keypoint) if target_keypoint else None}

    tf_vars = image_processing_vars

    # global robot_control_vars
    robot = robot_control_vars["robot_controller"]

    # Reshape to proper size
    image = np.frombuffer(camera_data.imageData, np.uint8).reshape((camera_data.imageHeight,
                                                                    camera_data.imageWidth,
                                                                    3))  # RGB
    rows, cols = CROPPED_IMAGE_SHAPE

    cv_image = image[:rows]  # 160 rows, we crop the robot from the image
    robot.camera.set_image_size(cols, rows)

    if tf_vars["show_image"]:
        try:
            img = Image.fromarray(cv_image, mode="RGB")
            img.save(f"images/camera/husky_camera_{sim_iteration}.png")
        except Exception as e:
            print(f"image_processing(): Error while saving image camera image {e}")

    all_colors = color_detector.COLOR_THRESHOLDS.keys()

    try:
        # Detect color keypoints ordered by area
        # (a proxy measure of the distance, since objects differ only in color)
        colors_to_keypoints = color_detector.detect_colors(tf_vars["detector"],
                                                           cv_image,
                                                           all_colors,
                                                           color_detector.COLOR_THRESHOLDS,
                                                           black_mask=tf_vars["black_mask"],
                                                           process_mask=True)
    except Exception as e:
        print(f"image_processing(): Color detection failed {e}")
        return []

    robot.visible_objects_info_dict = {c: compute_target_color_info(kp, c)
                                       for c, kp in colors_to_keypoints.items()}

    print(
        f"image_processing(): {[(c, c_info['area']) for c, c_info in robot.visible_objects_info_dict.items()]}")

    # return decorated image if publish_decorated_image flag is true
    if tf_vars["show_decorated_image"]:
        try:
            target_x_coord, _ = robot.camera.angle2pixel(robot.goal_object["info"]["azimuth"], 0.)
            target_x_coord = round(target_x_coord)
        except TypeError:
            target_x_coord = None

        decorated_cv_image = color_detector.decorate_image(cv_image,
                                                           colors_to_keypoints,
                                                           target_x_coord)

        try:
            img = Image.fromarray(decorated_cv_image, mode="RGB")
            img.save(f"images/decorated/husky_decorated_{sim_iteration}.png")
        except Exception as e:
            print(f"image_processing(): Error while saving decorated camera image {e}")

    return []
