# This TF computes velocity command to be sent as datapacks to gazebo
# so to reach the goal described by filtered_goal_angle
import math
import utils
import robot_states

motor_control_from_direction_vars = {"integral_goal_angle_error": 0,
                                     "rotate_on_self": False,
                                     "ROTATE_ON_SELF_ANGULAR_SPEED": 90.0,  # deg/s,
                                     "FORWARD_LINEAR_SPEED": 1.5,
                                     "pi_gains": {"P": 8., "I": 1.}}

@TransceiverFunction("gazebo")
def motor_control_from_direction():
    # global robot_control_vars
    robot = robot_control_vars["robot_controller"]
    tf_vars = motor_control_from_direction_vars

    if isinstance(robot.state, robot_states.AlignWithDirectionState):
        forward_speed = 0
    elif isinstance(robot.state, robot_states.MoveForwardState):
        forward_speed = tf_vars["FORWARD_LINEAR_SPEED"]
    else:
        # SKIP IF robot is not aligning with direction or moving forward
        return []

    filtered_goal_angle = goal_angle_filter_vars["filtered_goal_angle"]

    try:
        orientation = robot.pose["orientation"]
    except (TypeError, AttributeError):
        print(f"robot or robot.pose is None")
        # robot or robot.pose is None
        return []

    curr_yaw_deg = math.degrees(utils.yaw_euler_from_quaternion(*orientation))

    try:
        goal_angle_error = utils.angle_difference_deg(filtered_goal_angle, curr_yaw_deg)
    except TypeError:
        print(f"motor_control_from_direction(): filtered_goal_angle is None")
        return []

    turn_speed, tf_vars["integral_goal_angle_error"] = utils.pi(goal_angle_error,
                                                                tf_vars["integral_goal_angle_error"],
                                                                gains=tf_vars["pi_gains"])
    print(
        f"motor_control_from_direction(): {filtered_goal_angle=:.2f} curr_yaw={curr_yaw_deg:.2f} {goal_angle_error=:.2f}\n"
        f"                                forward_speed={forward_speed}, {turn_speed=:.2f}")

    return utils.create_vel_datapacks(forward_speed=forward_speed,
                                      turn_speed=turn_speed)
