# This TF computes velocity commands for the robot
# so to reach the goal described by filtered_goal_object_info

from nrp_core import TransceiverFunction

try:
    import utils
    import robot_states
except ImportError:
    import sys
    import os

    # add resources to PYTHONPATH
    resources_path = os.path.join(os.getcwd(), "resources")
    sys.path.append(resources_path)
    import utils
    import robot_states

motor_control_from_image_vars = {"integral_goal_error": 0,
                                 "ROTATE_ON_SELF_ANGULAR_SPEED": 90.0,  # deg/s,
                                 "FORWARD_LINEAR_SPEED": 1.5,
                                 "pi_gains": {"P": 10., "I": 1.}
}


@TransceiverFunction("gazebo")
def motor_control_from_image():
    tf_vars = motor_control_from_image_vars

    # global robot_control_vars
    robot = robot_control_vars["robot_controller"]

    # robot has exited: Don't move
    if robot.has_exited:
        return utils.create_vel_datapacks()

    if not isinstance(robot.state, robot_states.MoveToObjectState):
        # SKIP IF not moving towards object
        return []

    # global object_goal_filter_vars
    filtered_goal_object_info = object_goal_filter_vars["filtered_goal_object_info"]

    def compute_command_msg(object_info):
        # The PD controller is applied if the keypoint is detected
        # Otherwise, rotate the robot until the desired color keypoint is found again
        if object_info is None:
            # ROTATE_ON_SELF_MSG
            vel_msg = utils.create_vel_datapacks(turn_speed=tf_vars["ROTATE_ON_SELF_ANGULAR_SPEED"])
        else:
            goal_error = object_info["azimuth"]  # target object azimuth (signed angle from center)

            turn_speed, tf_vars["integral_goal_error"] = utils.pi(goal_error,
                                                                  tf_vars["integral_goal_error"],
                                                                  gains=tf_vars["pi_gains"])

            signed_turn_speed = turn_speed if goal_error < 180. else (turn_speed - 360.)
            forward_speed = tf_vars["FORWARD_LINEAR_SPEED"]

            print(
                f"motor_control_from_image(): {goal_error=}, {signed_turn_speed=:.2f}\n"
                f"                            {forward_speed=:.2f}, {turn_speed=:.2f}")

            vel_msg = utils.create_vel_datapacks(forward_speed=forward_speed,
                                                 turn_speed=signed_turn_speed)
        return vel_msg

    return compute_command_msg(filtered_goal_object_info)
