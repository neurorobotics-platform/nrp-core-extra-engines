# This TF filters out spurious None values from object goals.
# It outputs filtered_goal_object_info for motor_control to use

from nrp_core import TransceiverFunction

try:
    import utils
    import robot_states
except ImportError:
    import sys
    import os

    # add resources to PYTHONPATH
    resources_path = os.path.join(os.getcwd(), "resources")
    sys.path.append(resources_path)
    import utils
    import robot_states

COLOR_LOCKING_TIMESTEPS = 20  # COLOR_LOCKING_TIMESTEPS * dt = locking_time
object_goal_filter_vars = {"filtered_goal_object_info": None,
                           "prev_goal_object_info": None,
                           "color_locking_timer": utils.Counter(COLOR_LOCKING_TIMESTEPS)}


@TransceiverFunction("gazebo")
def object_goal_filter():
    tf_vars = object_goal_filter_vars

    # global robot_control_vars
    robot = robot_control_vars["robot_controller"]

    if not isinstance(robot.state, robot_states.MoveToObjectState):
        # SKIP IF not moving towards object
        return []

    try:
        goal_object = robot.goal_object
    except AttributeError:
        # robot_controller.value is None
        return []

    try:
        # goal_object available
        goal_info = goal_object["info"]
    except TypeError:
        # goal_object not available -> ROTATE:
        tf_vars["filtered_goal_object_info"] = None
        print("object_goal_filter(): ROTATE")
        return []

    def none_timed_filter(new, previous, locking_timer):
        """Filter out None values until locking_timer expires"""
        if new is None:
            if not locking_timer.expired:
                # keep lock on previous
                new = previous
                locking_timer.count()
            else:
                # stop locking
                locking_timer.unset()
        else:
            # valid new, clear timer
            locking_timer.clear()

        return new

    #  filter occasional loss of detection, set the goal and update its previous value
    tf_vars["prev_goal_object_info"] = tf_vars["filtered_goal_object_info"] = none_timed_filter(
        goal_info, tf_vars["prev_goal_object_info"], tf_vars["color_locking_timer"])

    print(f'object_goal_filter(): {tf_vars["filtered_goal_object_info"]}')

    return []
