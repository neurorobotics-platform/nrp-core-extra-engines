# This TF monitor place_cells for spiking.
# When one of them spikes it sets the relative robot_controller property
#
# The "first_place_cell_id" has to be set to the NEST ID of the first neuron
# of the place cells population, see the brain script.
# It's required to compute indexes relative to the start of said population.

from nrp_core import *

try:
    import utils
except ImportError:
    import sys
    import os

    # add resources to PYTHONPATH
    resources_path = os.path.join(os.getcwd(), "resources")
    sys.path.append(resources_path)
    import utils

# NOTE check first_place_cell_id in case the brain network gets changed
place_cells_monitor_vars = {"first_place_cell_id": 42}


@EngineDataPack(keyword='spike_recorder_place_cells_dp',
                id=DataPackIdentifier('spike_recorder_place_cells', 'nest'))
@TransceiverFunction("nest")  # nest
def place_cells_monitor(spike_recorder_place_cells_dp):
    tf_vars = place_cells_monitor_vars

    try:
        last_five_spike_ids = spike_recorder_place_cells_dp.data[0]["events"]["senders"][-5:]
        last_five_spike_ids_mean = round(sum(last_five_spike_ids) / len(last_five_spike_ids))
        cell_index = last_five_spike_ids_mean - tf_vars["first_place_cell_id"]
        print(
            f"place_cells_monitor(): f: CELL-SECTOR_INDEX: {cell_index}")
    except (IndexError, ZeroDivisionError):
        return []
    robot = robot_control_vars["robot_controller"]
    robot.goal_direction_sector = cell_index

    return []
