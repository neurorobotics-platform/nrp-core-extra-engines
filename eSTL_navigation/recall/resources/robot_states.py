from __future__ import annotations

import abc
import math

try:
    import utils
    import robot_controller
except ImportError:
    import sys
    import os

    # add resources to PYTHONPATH
    resources_path = os.path.join(os.getcwd(), "resources")
    sys.path.append(resources_path)
    import utils
    import robot_controller


class RobotState(abc.ABC):

    def __init__(self, robot: robot_controller.robot, logger=None):
        self.robot = robot
        self.logger = logger or print

    @abc.abstractmethod
    def step(self, t_ns):
        pass

    def logger_info(self, message):
        log = f'{self.__class__.__name__}: {message}'

        self.logger(log)


# RECALL states

class MoveForwardState(RobotState):
    # move forward until a place cell is spiking

    def step(self, t_ns):
        # no sector to go towards to <-- no place cell has spiked
        if self.robot.goal_direction_sector is None:
            self.robot.goal_direction_deg = 0.  # go forward
        else:
            # a place_cell has spiked, align with its relative sector
            self.robot.transition_to_state(AlignWithDirectionState(self.robot, logger=self.logger))


class AlignWithDirectionState(RobotState):
    def step(self, t_ns):

        if self.robot.goal_direction_deg is None:
            self.logger_info("NO Direction to align with")
            return

        self.logger_info(f"Goal Direction is {self.robot.goal_direction_deg:.2f} degrees")

        yaw_deg = math.degrees(
            math.fmod(utils.yaw_euler_from_quaternion(*self.robot.pose["orientation"]),
                      2 * math.pi))

        if abs(utils.angle_difference_deg(self.robot.goal_direction_deg, yaw_deg)) < (
                self.robot.sector_span_deg / 2):
            # Aligned to self.robot.goal_direction_deg
            # Find the closest object to navigate to
            self.robot.transition_to_state(FindClosestObjectState(self.robot, logger=self.logger))


class FindClosestObjectState(RobotState):

    def step(self, t_ns):
        closest_regular_object_info = self.robot.closest_visible_regular_object_info

        if closest_regular_object_info is None:
            self.logger_info("Can't see any object")
            return

        self.robot.goal_object = closest_regular_object_info["color"]
        self.logger_info(f'Navigation Goal is "{self.robot.goal_object["color"]}"')
        self.robot.transition_to_state(MoveToObjectState(self.robot,
                                                         next_state="ObjectReachedRecallState",
                                                         logger=self.logger))


class ObjectReachedRecallState(RobotState):

    def step(self, t_ns):
        # check which object reached
        if (goal_object := self.robot.goal_object) is None:
            self.logger_info("No object goal available")
            # TODO Fail?
            return

        goal_object_id = goal_object["color"]
        if self.robot.is_exit_object(goal_object_id):
            # exit object: saving of the weights is performed in TF
            self.robot.transition_to_state(FinalState(self.robot,
                                                      logger=self.logger,
                                                      message="Exit FOUND!"))
        else:
            # normal object. find closest obj
            self.robot.transition_to_state(
                AlignWithDirectionState(self.robot, logger=self.logger))


# LEARNING states

class FinalState(RobotState):
    DISPLAY_PERIOD_S = 2  # secs

    def __init__(self, robot: robot_controller.Robot, logger=None, message=""):
        super().__init__(robot, logger)
        self.message = message

    def step(self, t_ns):
        self.logger_info(self.message)


class FailState(FinalState):
    pass


class MoveToObjectState(RobotState):

    def __init__(self, robot: robot_controller.Robot, logger=None, next_state=None,
                 **next_state_kwargs):
        super().__init__(robot, logger)

        if next_state is None:
            next_state = "FailState"
            next_state_kwargs = {"message": "No next state specified"}

        self.next_state = next_state
        self.next_state_instance = globals()[next_state](self.robot, logger=self.logger,
                                                         **next_state_kwargs)

    def step(self, t_ns):

        if (goal := self.robot.goal_object) is None:
            self.logger_info(f"No object goal available")
            # TODO Fail?
            return

        if goal["reached"]:  # set in TF set_goal_reached
            self.logger_info(f"Object ({goal['color']}) Reached!")

            self.robot.transition_to_state(self.next_state_instance)
        else:
            self.logger_info(f'Goal is {goal["color"]}. Moving...')
