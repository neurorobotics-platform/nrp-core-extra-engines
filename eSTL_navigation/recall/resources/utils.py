import math

from nrp_core.data.nrp_protobuf import GazeboJointDataPack


class Counter:
    def __init__(self, limit) -> None:
        self.limit = limit
        self.counter = 0

    def count(self) -> None:
        self.counter = self.counter + 1

    def clear(self) -> None:
        self.counter = 0

    def unset(self) -> None:
        self.counter = self.limit

    @property
    def expired(self) -> bool:
        return self.counter >= self.limit


def angle_difference_rad(a: float, b: float) -> float:
    return math.fmod(a - b + math.pi, 2. * math.pi) - math.pi


def angle_difference_deg(a: float, b: float) -> float:
    return math.fmod(a - b + 180., 360.) - 180.


def create_vel_datapacks(model_name="husky", forward_speed=0., turn_speed=0.):
    model_joints = {"L": ["front_left_joint", "back_left_joint"],
                    "R": ["front_right_joint", "back_right_joint"]}

    turn_speed_rad = math.radians(turn_speed)
    velocities = {"L": (forward_speed - turn_speed_rad * 0.275) * 5,  # 5 empirically found gain
                  "R": (forward_speed + turn_speed_rad * 0.275) * 5}  # 0.275 is half of husky's wheel_separation

    # print(f'compute_command_msg(): {velocities}')

    dp_list = []
    for side, joints in model_joints.items():
        for joint in joints:
            dp = GazeboJointDataPack(f"{model_name}::{joint}", "gazebo")
            dp.data.velocity = velocities[side]
            dp_list.append(dp)

    return dp_list


def yaw_euler_from_quaternion(x, y, z, w):
    return math.atan2(2.0 * (w * z + x * y), 1.0 - 2.0 * (y ** 2 + z ** 2))


def yaw_to_cell_index(yaw_rad, num_cells):
    sector_span = 360 / num_cells
    offset = sector_span // 2  # e.g. sector 0 is [-10, 10) instead of [0, 20)
    angle = math.degrees(yaw_rad) + offset
    return math.floor((angle % 360) / sector_span) % num_cells


def cell_index_to_yaw(cell_index, num_cells):
    sector_span = 360 / num_cells
    # offset = sector_span // 2
    angle_degrees = (cell_index * sector_span)
    # return mid-sector in degrees
    return angle_degrees


cardinal_directions = ["N", "E", "S", "W"]


def angle_to_cardinal_direction(angle_rad):
    sector = round((math.fmod(angle_rad, 2 * math.pi)) / (math.pi / 2)) % 4  # 4 sectors -> 90 degs
    return cardinal_directions[sector]


def opposite_cardinal_direction(cardinal_direction: str):
    opposite_idx = (cardinal_directions.index(cardinal_direction) + 2) % 4
    return cardinal_directions[opposite_idx]


def pd(goal_error, prev_goal_error, gains={"P": 1., "D": 1.}, dt=0.02):
    """
    :param goal_error: current_value - goal_value
    :param prev_goal_error: previous goal_error
    :param gains: PD gains as dict
    :param dt: simulation timestep (default is 0.02)
    :return: An angular value in degree which should reduce the errors
    """
    goal_error = math.fmod(goal_error, 360)  # current_value - goal_value
    # Delta value for calculating the D part of the controller
    derivative_error = (goal_error - prev_goal_error) / dt

    return gains["P"] * goal_error + gains["D"] * derivative_error


def pi(goal_error, prev_integral_error, gains={"P": 1., "I": 1.}, dt=0.02):
    """
    :param goal_error: current_value - goal_value
    :param prev_integral_error: previous integral_error
    :param gains: PI gains as dict
    :param dt: simulation timestep (default is 0.02)
    :return: A tuple (angular value in degree which should reduce the errors, integral_error)
    """

    integral_error = (goal_error + prev_integral_error) * dt

    return gains["P"] * goal_error + gains["I"] * integral_error, integral_error


class Camera:
    """
    Utility class for converting between measures in the Field of View
    """

    def __init__(self, width=320, height=240):
        # sx and sy denote the image size (px)
        # note: not to be confused with the number of pixels per millimeter on the camera sensor.
        self._calib_sx = 320
        self._calib_sy = 240
        # fx and fy denote the focal length (px)
        # (physical focal length (mm) * pixel per mm on the sensor in axis x/y (px/mm))
        # if the pixels are square (which we assume being the case) the two values are equivalent.
        self._calib_fx = 160
        self._calib_fy = 160
        # cx and cy denote the pixel coordinates of the center pixel on the camera (px)
        self._calib_cx = 160
        self._calib_cy = 120

        # cx, cy, fx and fy are published on the camera_info topic of the camera at the "K" entry.
        # K denotes the camera matrix and is structured as follows
        # | fx  0.  cx |
        # | 0.  fy  cy |
        # | 0.  0.  1. |

        self._curr_cx = self._curr_cy = None
        self._curr_fx = self._curr_fy = None
        self._curr_sx = self._curr_sy = None

        self.set_image_size(width, height)

    def set_image_size(self, w, h):
        """
        Sets the image size

        :param w: image's new width
        :param h: image's new height
        """
        if w <= 0 or h <= 0:
            return False
        self._curr_sx = w
        self._curr_sy = h
        scale_x = self._curr_sx / self._calib_sx
        scale_y = self._curr_sy / self._calib_sy
        self._curr_fx = self._calib_fx * scale_x
        self._curr_cx = self._calib_cx * scale_x
        self._curr_fy = self._calib_fy * scale_y
        self._curr_cy = self._calib_cy * scale_y
        return True

    def pixel2metric(self, u, v):
        """
        Converts a pixel coordinate on the image to a metric distance from its center.

        :param u: the x coordinate (column) of the pixel (px)
        :param v: the y coordinate (row) of the pixel (px)
        :return: a pair (xm, ym) denoting the metric distance from the center of the image
            (pure number)
        """
        return (u - self._curr_cx) / self._curr_fx, (v - self._curr_cy) / self._curr_fy

    def metric2pixel(self, xm, ym):
        """
        Converts a metric distance from the image center to a pixel coordinate.

        :param xm: the x distance from the center (pure number)
        :param ym: the y distance from the center (pure number)
        :return: a pair (u, v) denoting a pixel's coordinates (px)
        """
        return xm * self._curr_fx + self._curr_cx, ym * self._curr_fy + self._curr_cy

    @staticmethod
    def metric2angle(xm, ym):
        """
        Converts a metric distance from the image's center to an (azimuth, elevation) pair.

        :param xm: the x distance from the center (pure number)
        :param ym: the y distance from the center (pure number)
        :return: a pair (a, e) denoting the azimuth and the elevation, the center being (0, 0)
            (deg)
        """
        _rho = math.sqrt(xm ** 2 + ym ** 2 + 1)
        a = -math.atan(xm) * 180 / math.pi  # azimuth
        e = -math.asin(ym / _rho) * 180 / math.pi  # elevation
        return a, e

    @staticmethod
    def angle2metric(a, e):
        """
        Converts an (azimuth, elevation) pair to a metric distance from the image's center.

        :param a: the azimuth angle from the image's center (deg)
        :param e: the elevation angle from the image's center (deg)
        :return: a pair (xm, ym) denoting the metric distance from the center of the image
            (pure number)
        """
        xm = -math.tan(a * math.pi / 180)
        ym = -math.tan(e * math.pi / 180) * math.sqrt(xm ** 2 + 1)
        return xm, ym

    def pixel2angle(self, u, v):
        """
        Converts a pixel coordinate on the image to an (azimuth, elevation) pair.

        :param u: the x coordinate (column) of the pixel (px)
        :param v: the y coordinate (row) of the pixel (px)
        :return: a pair (a, e) denoting the azimuth and the elevation, the center being (0, 0)
            (deg)
        """
        xm, ym = self.pixel2metric(u, v)
        return Camera.metric2angle(xm, ym)  # a, e

    def angle2pixel(self, a, e):
        """
        Converts an (azimuth, elevation) pair to a pixel coordinate on the image.

        :param a: the azimuth angle from the image's center (deg)
        :param e: the elevation angle from the image's center (deg)
        :return: a pair (u, v) denoting a pixel's coordinates (px)
        """
        xm, ym = Camera.angle2metric(a, e)
        u, v = self.metric2pixel(xm, ym)
        return u, v

    def pixel2norm(self, u, v):
        """
        Converts a pixel coordinate on the image to a normalized distance from its center.

        :param u: the x coordinate (column) of the pixel (px)
        :param v: the y coordinate (row) of the pixel (px)
        :return: a pair (x, y) denoting the distance from the center of the image
            (pure number in range [-1, 1] x [-1, 1])
        """
        return 2 * u / self._curr_sx - 1, 2 * v / self._curr_sy - 1  # x, y

    def norm2pixel(self, x, y):
        """
        Converts a normalized distance from the image's center to the corresponding pixel.

        :param x: the x coordinate of the distance from the center (pure number in range [-1, 1])
        :param y: the y coordinate of the distance from the center (pure number in range [-1, 1])
        :return: a pair (u, v) denoting a pixel's coordinates (px)
        """
        return self._curr_sx * (x + 1) / 2, self._curr_sy * (y + 1) / 2  # u, v

    def norm2angle(self, x, y):
        """
        Converts a normalized distance from the image's center to the corresponding
            (azimuth, elevation) pair.

        :param x: the x coordinate of the distance from the center (pure number in range [-1, 1])
        :param y: the y coordinate of the distance from the center (pure number in range [-1, 1])
        :return: a pair (a, e) denoting the azimuth and the elevation, the center being (0, 0)
            (deg)
        """
        u, v = self.norm2pixel(x, y)
        return self.pixel2angle(u, v)  # a, e

    def norm2metric(self, x, y):
        """
        Converts a normalized distance from the image's center to a metric distance from its center

        :param x: the x coordinate of the distance from the center (pure number in range [-1, 1])
        :param y: the y coordinate of the distance from the center (pure number in range [-1, 1])
        :return: a pair (xm, ym) denoting the metric distance from the center of the image
            (pure number)
        """
        u, v = self.norm2pixel(x, y)
        return self.pixel2metric(u, v)  # xm, ym

    @property
    def height(self):
        """
        Gets the image's height (px)
        """
        return self._curr_sy

    @property
    def width(self):
        """
        Gets the image's width (px)
        """
        return self._curr_sx

    @property
    def cal_height(self):
        """
        Gets the image's height computed during calibration (px)
        """
        return self._calib_sy

    @property
    def cal_width(self):
        """
        Gets the image's width computed during calibration (px)
        """
        return self._calib_sx

    @property
    def fx(self):
        """
        Gets the image's x focal length (px)
        """
        return self._curr_fx

    @property
    def fy(self):
        """
        Gets the image's y focal length (px)
        """
        return self._curr_fy

    @property
    def cx(self):
        """
        Gets the image center's x coordinate (px)
        """
        return self._curr_cx

    @property
    def cy(self):
        """
        Gets the image center's y coordinate (px)
        """
        return self._curr_cy

    @property
    def cal_fx(self):
        """
        Gets the image's x focal length computed during calibration (px)
        """
        return self._calib_fx

    @property
    def cal_fy(self):
        """
        Gets the image's y focal length computed during calibration (px)
        """
        return self._calib_fy

    @property
    def cal_cx(self):
        """
        Gets the image center's x coordinate computed during calibration (px)
        """
        return self._calib_cx

    @property
    def cal_cy(self):
        """
        Gets the image center's y coordinate computed during calibration (px)
        """
        return self._calib_cy
