from nrp_core import PreprocessingFunction, SimulationTime

try:
    import robot_controller as robot_controller_module
except ImportError:
    import sys
    import os

    # add resources to PYTHONPATH
    resources_path = os.path.join(os.getcwd(), "resources")
    sys.path.append(resources_path)

    import robot_controller as robot_controller_module

NUM_PLACE_CELLS = 18

OBJECTS = {
    "SIMPLE_OBJECTS": ["RED", "BLUE", "YELLOW", "CYAN", "ORANGE", "GREEN"],
    "EXIT_OBJECT": "GREEN",
    "START_OBJECT": "MAGENTA",
    "DEAD_END_OBJECT": "PURPLE"
}

robot_control_vars = {
    "dt_s": 0.02,
    "robot_controller": robot_controller_module.make_recall_robot(
    simple_objects=OBJECTS["SIMPLE_OBJECTS"],
    start_obj=OBJECTS["START_OBJECT"],
    dead_end_obj=OBJECTS["DEAD_END_OBJECT"],
    exit_obj=OBJECTS["EXIT_OBJECT"],
    num_place_cells=NUM_PLACE_CELLS,
    logger=print)}

print("robot_controller: INITIALIZING RECALL ROBOT CONTROL")


@SimulationTime(keyword="t_ns")
@PreprocessingFunction("gazebo")
def robot_control(t_ns):
    robot = robot_control_vars["robot_controller"]

    print(f"========== t: {t_ns/1e9:.2f} ==================")

    robot.step(t_ns)

    print(f"robot_control(): "
          f" OBJECT GOAL: {robot.goal_object['color'] if robot.goal_object is not None and not robot.goal_object['reached'] else 'N/A'}"
          f" | DIRECTION GOAL: {robot.goal_direction_deg if robot.goal_direction_deg is not None else 'N/A'}")

    return []
