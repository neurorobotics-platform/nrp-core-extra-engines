# This TF set the "reached" key of the current goal_object
# when its pixel count is less than a threshold

from nrp_core import TransceiverFunction

set_object_goal_reached_vars = {"prev_goal_direction_deg": None,
                                "goal_direction_agv_deg": 0,
                                "goal_direction_agv_deg_samples_count": 0,
                                "MAX_AREA": 1200}


@SimulationTime(keyword="t_ns")
@TransceiverFunction("gazebo")
def set_object_goal_reached(t_ns):
    tf_vars = set_object_goal_reached_vars
    
    # global robot_control_vars
    robot = robot_control_vars["robot_controller"]

    print(
        f"set_object_goal_reached(): Goal direction average {tf_vars['goal_direction_agv_deg']:.2f}")

    try:
        # Cumulative average of robot.goal_direction_deg
        avg = tf_vars["goal_direction_agv_deg"]
        n = tf_vars["goal_direction_agv_deg_samples_count"]

        tf_vars["goal_direction_agv_deg"] = (robot.goal_direction_deg + n * avg) / (n + 1)
        tf_vars["goal_direction_agv_deg_samples_count"] += 1
    except TypeError:
        return []

    if (goal := robot.goal_object) is None or (goal_info := goal["info"]) is None:
        return []

    # consider a goal reached when the object is big enough on image AND
    # the goal direction has "recently" changed more than one sector span OR it the exit object
    is_object_close = goal_info["area"] > tf_vars["MAX_AREA"]
    is_next_direction_available = abs(
        tf_vars["goal_direction_agv_deg"] - robot.goal_direction_deg) > robot.sector_span_deg
    is_goal_object_an_exit = robot.is_exit_object(robot.goal_object["color"])

    if is_object_close and (is_next_direction_available or is_goal_object_an_exit):
        goal["reached"] = True
        tf_vars["goal_direction_agv_deg"] = robot.goal_direction_deg
        tf_vars["goal_direction_agv_deg_samples_count"] = 1
        print(f"{t_ns/1e9=:.2f}: {goal['color']}: REACHED. RESET statistics.")

    return []
