# Visual Saliency Model Engine

![camera image](camera_image.png) ![saliency map](saliency_map.png)

This experiments in `husky_braitenberg_saliency` showcases a Tensorflow-based Visual Saliency Model, developed in the context of HBP WP3, employed in the customary Husky Braitenberg experiment. 

The model is integrated in the NRP as a JSON/gRPC engine so that it can be easily reused in other experiments.

The Engine takes as input the image to process via the `input_image` datapack and outputs the saliency maps in a datapack named `saliency_image`.
Some of its aspects are configurable via the `EngineExtraConfigs` JSON object in the simulation configuration file (see the engine script for more info).

Both a JSON and a gRPC based version of the engine is provided as an example of integration of a third-party library (e.g. Tensorflow, as in this case).
The gRPC one is more performant thanks to the more efficient RPC framework employed (vs. REST/JSON).

The experiment shows how to interact with the engine: a TF `forward_image_to_engine_(grpc/json).py` sends the robot camera images to it and the `save_saliency_map_(grpc/json).py` saves the computed saliency maps on files.

## Engine DataPacks and parameters

The saliency engine uses the following datapacks:

Input:

- `input_image`: `nrpgenericproto_pb2.Image`: The image of which to compute the saliency map.

Output:
    
- `saliency_image`: `nrpgenericproto_pb2.Image`: `input_image`'s saliency map.

Engine configuration parameters (`EngineExtraConfigs`):

- `TensorflowDevice`: `string` : `"cpu"`/`"cuda"` the device to be used by tensorflow for inference.
- `ShowImages`: `bool` : Whether to show in/output images during the experiment.
- `LogImages`: `bool` : Whether to save in/output images on disk.
- `InputImagesLogPath`: `string` : the path where to save the input images as PNG file.
- `OutputImagesLogPath` `string` : the path where to save the saliency maps as PNG file.


## Dependencies
The experiments require nrp-core version >= 1.4.1 and the following python packages:

```shell
# GPU acceleration requires CUDA support for tensorflow (~8x inference speedup)
pip install tensorflow==2.13.1 # supports cuda 11
```

## Run the experiment
Use the following commands:
```shell
# gRPC
$ NRPCoreSim -c simulation_config_grpc.json 2>log.err # redirect std-err to log.err, nest-server is very noisy
# or JSON
$ NRPCoreSim -c simulation_config_json.json 2>log.err
# In a separate terminal, start optional gazebo visualization 
$ gzclient
```

If `save_saliency_map` TF's argument `IsActive` is set to `true`,
the generated saliency maps will be available as `png` files in `images/saliency_maps`.
Beware that is incompatible with the engine's `LogImages` configuration parameter set to `true` since they both try to save the same `saliency_map`.
