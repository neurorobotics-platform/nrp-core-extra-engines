# This TF convert and forwards gazebo camera images to the saliency engine via protobuf datapacks

import cv2
import numpy as np
from nrp_core import *
from nrp_core.data.nrp_protobuf import NrpGenericProtoImageDataPack
from nrp_protobuf import nrpgenericproto_pb2


@EngineDataPack(keyword='camera_dp',
                id=DataPackIdentifier('husky::eye_vision_camera::camera', 'gazebo'))
@TransceiverFunction("tensorflow_python_grpc_engine")
def forward_image_to_engine(camera_dp):
    if camera_dp.isEmpty():
        return []

    camera_dp_proto = camera_dp.data

    camera_image = np.frombuffer(camera_dp_proto.imageData,
                                 dtype=np.uint8).reshape(camera_dp_proto.imageHeight,
                                                         camera_dp_proto.imageWidth,
                                                         camera_dp_proto.imageDepth)
    camera_image_rgb = cv2.cvtColor(camera_image, cv2.COLOR_BGR2RGB)

    # create and prepare datapack
    input_image_datapack = NrpGenericProtoImageDataPack("input_image",
                                                        "tensorflow_python_grpc_engine")
    input_image_datapack_proto = input_image_datapack.data
    input_image_datapack_proto.height = camera_image_rgb.shape[0]
    input_image_datapack_proto.width = camera_image_rgb.shape[1]
    input_image_datapack_proto.type = nrpgenericproto_pb2.IMAGE_TYPE.RGB8
    input_image_datapack_proto.data = camera_image_rgb.tobytes()

    return [input_image_datapack]
