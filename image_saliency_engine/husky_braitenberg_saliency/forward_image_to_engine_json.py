# This TF convert and forwards gazebo camera images to the saliency engine via JSON datapacks

import numpy as np
import cv2
from nrp_core import *
from nrp_core.data.nrp_json import *
from nrp_core.data.nrp_protobuf import *


@EngineDataPack(keyword='camera_dp',
                id=DataPackIdentifier('husky::eye_vision_camera::camera', 'gazebo'))
@TransceiverFunction("tensorflow_python_json_engine")
def forward_image_to_engine(camera_dp):
    if camera_dp.isEmpty():
        return []

    camera_dp_proto = camera_dp.data

    camera_image = np.frombuffer(camera_dp_proto.imageData,
                                 dtype=np.uint8).reshape(camera_dp_proto.imageHeight,
                                                         camera_dp_proto.imageWidth,
                                                         camera_dp_proto.imageDepth)
    camera_image_rgb = cv2.cvtColor(camera_image, cv2.COLOR_BGR2RGB)

    # create and prepare datapack
    input_image_datapack = JsonDataPack("input_image",
                                        "tensorflow_python_json_engine")

    input_image_datapack.data['imageHeight'] = camera_image_rgb.shape[0]
    input_image_datapack.data['imageWidth'] = camera_image_rgb.shape[1]
    input_image_datapack.data['channels'] = 3  # RGB Image
    input_image_datapack.data['image'] = camera_image_rgb.flatten().tolist()

    return [input_image_datapack]
