import numpy as np
import cv2
import os
from nrp_core import *
from nrp_core.data.nrp_json import *
from nrp_core.data.nrp_protobuf import *

save_saliency_map_vars = {
    "saliency_images_path": "images/saliency_maps",
    "saliency_image_filename_format": "saliency_map_{time_ms}.png"
}


@EngineDataPack(keyword='saliency_image_dp',
                id=DataPackIdentifier('saliency_image', 'tensorflow_python_json_engine'))
@SimulationTime(keyword="t_ns")
@TransceiverFunction("tensorflow_python_json_engine")
def save_saliency_map(t_ns, saliency_image_dp):
    if (saliency_image_dp.isEmpty() or
            not saliency_image_dp.isUpdated() or
            saliency_image_dp.data.json_type() == "null"):
        return []

    t_ms = round(t_ns / 1e6)

    tf_vars = save_saliency_map_vars
    sal_img_data = saliency_image_dp.data

    saliency_image = np.array(sal_img_data['image'],
                              dtype=np.uint8).reshape(sal_img_data['image_height'],
                                                      sal_img_data['image_width'],
                                                      sal_img_data['channels'])

    saliency_image_filename = tf_vars["saliency_image_filename_format"].format(time_ms=t_ms)
    saliency_image_path = os.path.join(tf_vars["saliency_images_path"],
                                       saliency_image_filename)

    cv2.imwrite(saliency_image_path, saliency_image)

    return []
