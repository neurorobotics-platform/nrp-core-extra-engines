"""
This Engine implements an image saliency maps generator.
It uses a tensorflow model developed in the context of HBP WP3

Input DP:
    - "input_image": JSON Image: The image of which to compute the saliency map
Output DP:
    - "saliency_image": JSON Image: input_image's saliency map

JSON Image format is :
{"image_height": int,
 "image_width": int,
 "channels": int,
 "image": array}

Engine configuration parameters (EngineExtraConfigs):

TensorflowDevice: "cpu"/"cuda" # the device to be used by tensorflow for inference
ShowImages: bool # Whether to show in/output images during the experiment
LogImages: bool # Whether to save in/output images on disk
InputImagesLogPath: string # the path where to save the input images
OutputImagesLogPath string # the path where to save the output images

"""

import sys
import os
import time

try:
    # tested with tensorflow==2.13.1
    import tensorflow.compat.v1 as tf

except ImportError as e:
    print(f"Can't Import Tensorflow. Please install it. {e}")
    sys.exit()

import cv2
import numpy as np

from nrp_core.engines.python_json import EngineScript

DEFAULT_DEVICE = "cpu"
MODEL_PATH = "tensorflow_model/model_salicon_{}.pb"

DEFAULT_IMAGE_SHOW = False
DEFAULT_IMAGE_LOG = False
DEFAULT_INPUT_IMAGE_LOG_PATH = "images/gazebo"
DEFAULT_OUTPUT_IMAGE_LOG_PATH = "images/saliency_maps"

SUPPORTED_IMAGE_SIZE = (240, 320)


class Script(EngineScript):
    def _load_configuration(self):
        # LOAD engine-specific configuration
        print("Loading engine configuration")
        self.engine_conf = self._config["EngineExtraConfigs"]

        # show the input and computed saliency map in a window
        self.show_images = self.engine_conf.get("ShowImages", DEFAULT_IMAGE_SHOW)
        # input images logging
        self.log_images = self.engine_conf.get("LogImages", DEFAULT_IMAGE_LOG)
        self.log_input_images_path = self.engine_conf.get("InputImagesLogPath",
                                                          DEFAULT_INPUT_IMAGE_LOG_PATH)
        self.log_input_images_filename_format = "gazebo_{time_ms}.png"

        # output images logging
        self.log_output_images_path = self.engine_conf.get("OutputImagesLogPath",
                                                           DEFAULT_OUTPUT_IMAGE_LOG_PATH)
        self.log_output_images_filename_format = "saliency_map_{time_ms}.png"

        # Tensorflow device
        self.tf_device = self.engine_conf.get("TensorflowDevice", DEFAULT_DEVICE)

        self.model_path = MODEL_PATH.format(self.tf_device)

    def initialize(self):
        print("Visual Saliency JSON Engine is initializing")

        self._load_configuration()

        print("Registering datapack...")
        self._registerDataPack("saliency_image")
        self._registerDataPack("input_image")

        # Load Tensorflow model
        if self.tf_device == "cpu":
            tf.config.experimental.set_visible_devices([], 'GPU')

        tf.disable_v2_behavior()

        graph_def = tf.GraphDef()

        # load tensorflow trained model
        with tf.gfile.Open(self.model_path, "rb") as pb_file:
            graph_def.ParseFromString(pb_file.read())

        self.input_placeholder = tf.placeholder(tf.float32, (None, None, None, 3))

        [self.predicted_maps] = tf.import_graph_def(graph_def,
                                                    input_map={"input": self.input_placeholder},
                                                    return_elements=["output:0"])
        self.tf_session = tf.Session()

        if self.show_images:
            # Initialize a window to display the images
            cv2.namedWindow('Camera + Saliency map', cv2.WINDOW_AUTOSIZE)

    def runLoop(self, timestep_ns):
        """Receive input_image and output image_saliency at every timestep"""

        image_dp = self._getDataPack("input_image")

        # deserialize camera image (RGB8)
        try:
            camera_image_rgb = np.array(image_dp['image'],
                                        dtype=np.uint8).reshape(image_dp['imageHeight'],
                                                                image_dp['imageWidth'],
                                                                image_dp['channels'])
        except TypeError:
            # image_dp is None
            return

        if camera_image_rgb.shape[:2] != SUPPORTED_IMAGE_SIZE:
            # saliency model works with (320, 240) images
            camera_image_rgb = cv2.resize(camera_image_rgb, SUPPORTED_IMAGE_SIZE)

        image_rgb_tf = camera_image_rgb[np.newaxis, :, :, :]

        # compute saliency map from  camera image
        start = time.perf_counter_ns()
        saliency_image = self.tf_session.run(self.predicted_maps,
                                             feed_dict={self.input_placeholder: image_rgb_tf})
        print(f'Saliency map computation time on device "{self.tf_device}":'
              f' {(time.perf_counter_ns() - start) / 1e6:.2f} ms')

        saliency_image = saliency_image.squeeze()
        saliency_image_rgb = np.uint8(cv2.cvtColor(saliency_image, cv2.COLOR_GRAY2RGB) * 255)

        # Setting the datapack with the processed image
        saliency_image_rgb_dict = {"image_height": saliency_image_rgb.shape[0],
                                   "image_width": saliency_image_rgb.shape[1],
                                   "channels": saliency_image_rgb.shape[2],
                                   "image": saliency_image_rgb.flatten().tolist()}

        self._setDataPack("saliency_image", saliency_image_rgb_dict)

        # SHOW and LOG if configured
        if self.show_images:
            cv2.imshow('Camera + Saliency map',
                       np.concatenate((camera_image_rgb, saliency_image_rgb), axis=1))
            cv2.waitKey(1)  # wait 1ms and run the GUI loop

        if self.log_images:
            time_ms = round(self._time_ns / 1e6)
            # camera image
            self.save_image(camera_image_rgb,
                            self.log_input_images_path,
                            self.log_input_images_filename_format, time_ms=time_ms)

            # saliency map
            self.save_image(saliency_image_rgb,
                            self.log_output_images_path,
                            self.log_output_images_filename_format, time_ms=time_ms)

    def save_image(self, image, image_path, image_filename_format, **format_kwargs):
        # camera image
        image_path = os.path.join(image_path,
                                  image_filename_format.format(**format_kwargs))

        # NOTE: image_path is assumed to be valid, i.e. whole directory hierarchy already created.
        cv2.imwrite(image_path, image)

    def shutdown(self):
        if self.show_images:
            cv2.destroyAllWindows()

        self.tf_session.close()
        print("Tensorflow Python JSON Engine is shutting down")
