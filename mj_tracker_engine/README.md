# Minimum-jerk online trajectory generator Engine

This Engine implements an online trajectory generator to be used, upstream to an inverse kinematic solver, in a robotic control pipeline. 
It leverages the _mj_se3_tracker_ library implemented by the [Scuola Superiore Sant'Anna](https://www.santannapisa.it/en/) [Biorobotics Institute](https://www.santannapisa.it/en/institute/biorobotics) [BRIAR Lab](https://www.santannapisa.it/en/institute/biorobotics/brair-lab) the context of Work Package 3 of the [HBP](https://www.humanbrainproject.eu/en/). 

For information about _mj_se3_tracker_ please read the enclosed _Readme.md_ (_resources/nrp-mj-tracker/README.md_)

It has been tested with nrp-core version 1.3.2.

## Installation

Since _mj_tracker_engine_ leverages the [PythonGRPCEngine](https://neurorobotics.net/Documentation/latest/nrp-core/page_python_grpc_engine.html), and using protobuf messages defined in the _proto_msgs_ directory, a code generation step is required (for details see [here](https://neurorobotics.net/Documentation/latest/nrp-core/page_tutorial_add_proto_definition.html#compiling-new-protobuf-message-definitions)).

    nrp_compile_protobuf.py --proto_files_path ./proto_msgs  # generate code from messages definitions

 The plotting engine leverages matplotlib. Please upgrade matplotlib with pip ([this](https://github.com/matplotlib/matplotlib/issues/22308) could happen otherwise).

    pip install --upgrade matplotlib

## Description

_mj_tracker_engine_ (_mj_tracker_engine.py_) is a PythonGRPCEngine exposing the _mj_se3_tracker_ library so to be employed in nrp-core simulations.
A plotter tool (_plotting_engine.py_) is also provided to visualize the convergence to the target.

Following the naming convention of the _mj_se3_tracker_ library,
the entity that tracks (e.g. a robot hand effector) a target object is called "tracker object".

To generate a trajectory for the tracker object to follow, _mj_tracker_engine_ requires the pose (datapacks named  "_target_pose_") of the stationary target.
The generated tracker trajectory is published, at every timestep, in two datapacks "_tracker_pose_ref_" (reference pose, i.e. pose at t+1) and "_tracker_velocity_ref_" (reference velocity, i.e. velocity at t+1).
Pose and velocity Datapacks have types, respectively, _Pose_ and _Twist_ (see proto_msgs/geometry_msgs.proto definitions)

Initial poses of the target and tracker are also required; they are specified with the configuration values:
- Target _TargetInitialPosition_, _TargetInitialOrientation_ 
- Tracker: _TrackerInitialPosition_,  _TrackerInitialOrientation_


## Demo experiments

To launch a demo simulation use one of the following command:

    # launch a demo with plotting
    NRPCoreSim -c simulation_config_plot.json
    
    # launch a demo without plotting
    NRPCoreSim -c simulation_config_no_plot.json

The demo shows how to use the engine to generate a trajectory so to make an end effector 
(i.e. a tracker object at _TrackerInitialPosition_ and _TrackerInitialOrientation_) reach a target at
_TargetInitialPosition_ _TargetInitialOrientation_ in _TrackingTime_ seconds (at least 0.15s).

When using _simulation_config_plot.json_, a plotter will show the tracker's and target's pose alongside info on convergence to target.
![plotter example](plotter.png "Plotter")