"""
This Engine implements an online trajectory generator.
It uses the mj_se3_tracker library implemented in the context of HBP WP3

Following the naming convention of the mj_se3_tracker library,
the entity that tracks (e.g. a robot hand effector) a target object is called "tracker object".

Input DP:
    - "target_pose": Pose: The Target's pose. (a Pose message)
Output DP:
    - "tracker_pose_ref": The computed Tracker's reference pose (i.e. pose at t+1) (a Pose message)
    - "tracker_velocity_ref": The computed Tracker's reference velocity (i.e. velocity at t+1) (a Twist message)
"""
try:
    import mj_se3_tracker as mjt
    import msgs_utils
    import tracker_utils

except ImportError:
    import sys
    import os

    # add resources to PYTHONPATH
    resources_path = os.path.join(os.getcwd(), "resources")
    sys.path.append(resources_path)

    # add nrp-mj-tracker
    sys.path.append(os.path.join(resources_path, "nrp-mj-tracker", "mj_se3_tracker"))
    import mj_se3_tracker as mjt

    import msgs_utils
    import tracker_utils

from typing import Optional, Dict

import numpy as np
from google.protobuf.message import Message
from nrp_core.engines.python_grpc import GrpcEngineScript
from nrpgeometrymsgs_pb2 import Pose, Twist


class Script(GrpcEngineScript):
    zero_se3_state_derivative: mjt.SE3StateDerivative = mjt.SE3StateDerivative()

    def __init__(self):
        super().__init__()
        #  self._config not available here. Only in initialize()
        self.dt: Optional[float] = None  # secs
        self.engine_conf: Optional[dict] = None

        # the tracker instance
        self.tracker: Optional[mjt.MJTracker] = None
        self.tracker_type: str = "MJTracker"

        # tracker initial pose
        self.tracker_initial_position: Optional[np.ndarray] = None
        self.tracker_initial_orientation: Optional[np.ndarray] = None
        
        # target initial pose
        self.target_initial_position: Optional[np.ndarray] = None
        self.target_initial_orientation: Optional[np.ndarray] = None

        # tracking time in nsecs
        self.t_track_ns: int = 0
        self.t_track_start_ns: int = 0

        # target state
        self.target_pose_msg: Optional[Pose] = None
        self.target_se3_pose: Optional[mjt.SE3State] = None

    def _registerDataPacks(self, datapacks_dict: Dict[str, Message]) -> None:
        for dp_name, protobuf_type in datapacks_dict.items():
            self._registerDataPack(dp_name, protobuf_type)

    def _load_configuration(self):
        # LOAD configuration
        self.dt = float(self._config["EngineTimestep"])  # secs
        self.engine_conf = self._config["EngineExtraConfigs"]

        # Tracker's initial pose (position and orientation).
        # Assumed stationary, no initial values of velocity and acceleration
        tracker_initial_position = self.engine_conf.get("TrackerInitialPosition", [1., 1., 0.])
        tracker_initial_orientation = self.engine_conf.get("TrackerInitialOrientation",
                                                           [1., 0., 1., 0.])

        self.tracker_initial_position: np.ndarray = np.array(tracker_initial_position, dtype=np.float64)
        # note: quaternion should be normalized
        self.tracker_initial_orientation: np.ndarray = mjt.normalize(np.array(tracker_initial_orientation,
                                                                              dtype=np.float64))
        # Target's initial pose (position and orientation).
        # Assumed stationary, no initial values of velocity and acceleration
        target_initial_position = self.engine_conf.get("TargetInitialPosition", [0., 0., 0.])
        target_initial_orientation = self.engine_conf.get("TargetInitialOrientation",
                                                          [0., 0., 1., 0.])

        self.target_initial_position: np.ndarray = np.array(target_initial_position,
                                                            dtype=np.float64)
        self.target_initial_orientation: np.ndarray = mjt.normalize(
            np.array(target_initial_orientation, dtype=np.float64))

        # total time to reach the target in nsecs.
        self.t_track_ns = int(float(self.engine_conf.get("TrackingTime", "1.5")) * 1e9)

        self.tracker_type = self.engine_conf.get("TrackerType", "MJTracker")
        # END configuration

    def _initialize(self):
        if self.tracker_type == "ClampedMJTracker":
            vectors = ["vp",  # Linear Velocity
                       "vo",  # Angular Velocity
                       "ap",  # Linear Acceleration
                       "ao"]  # Angular Acceleration
            # max velocity and acceleration norms
            norms = {key: val
                     for key in [f"{v}_norm" for v in vectors]
                     if (val := self.engine_conf.get(key)) != 0.}
        else:
            norms = {}

        print("Initializing the trajectory generator...")
        # Initialize tracker object and its state
        self.tracker = getattr(mjt, self.tracker_type)(
            se3=mjt.SE3State(self.tracker_initial_position, self.tracker_initial_orientation),
            se3d=Script.zero_se3_state_derivative,  # vp:(0,0,0) vo:(0,0,0)
            se3dd=Script.zero_se3_state_derivative,  # ap:(0,0,0) ao:(0,0,0)
            **norms
        )

        self.target_se3_pose = mjt.SE3State(self.target_initial_position,
                                            self.target_initial_orientation)

        print("Registering datapacks...")
        self._register_datapacks()

    def _register_datapacks(self):
        # tracker pose and velocity reference:
        # i.e. the tracker's next pose computed by the generator.
        tracker_output_datapacks: Dict[str, Message] = {"tracker_pose_ref": Pose,
                                                        "tracker_velocity_ref": Twist}

        # target's current state, an input from a simulator (e.g. gazebo)
        # we assume that the target is stationary, so no need to read its velocity and accel
        target_input_datapacks: Dict[str, Message] = {"target_pose": Pose}

        for dp in [tracker_output_datapacks, target_input_datapacks]:
            self._registerDataPacks(dp)

    @property
    def initialized(self):
        return self.tracker is not None

    def initialize(self):
        print(f"{self._name} is initializing")
        print("Loading configuration...")
        self._load_configuration()
        self._initialize()
        print(f"{self._name} initialization completed.")

    def runLoop(self, timestep_ns):

        if not self.initialized:
            return

        # RETRIEVE new data packs values
        # - current target pose if changed
        # new valid message
        new_target_pose_msg = self._getDataPack("target_pose")

        if new_target_pose_msg is msgs_utils.most_recent_msg(new_target_pose_msg,
                                                             self.target_pose_msg):
            self.target_pose_msg = new_target_pose_msg
            self.target_se3_pose = mjt.SE3State(
                msgs_utils.point_to_ndarray(self.target_pose_msg.position),
                mjt.normalize(
                    msgs_utils.quaternion_to_ndarray(self.target_pose_msg.orientation))
            )

        # - time-to-go, i.e. time left to reach the target
        t_go_s: float = (self.t_track_ns - self._time_ns) / 1e9

        # Compute the jerk to apply to the system as a function of the target and the time-to-go
        # NOTE: If the target is moving, at every time step,
        #       pass the target's state at the current time
        se3ddd = self.tracker.compute_jerk(
            self.target_se3_pose,  # target's SE3 pose
            Script.zero_se3_state_derivative,
            # target's SE3 velocity, here left to 0 as the target is not moving
            Script.zero_se3_state_derivative,
            # target's SE3 acceleration, here left to 0 as the target is not moving
            Script.zero_se3_state_derivative,
            # target's SE3 jerk, here left to 0 as the target is not moving
            t_go_s,  # secs
            t_go_thresh=0.15,  # THRESHOLD OF THE TIME TO GO in secs
        )
        # Step the system by applying the jerk and integrating the state
        self.tracker.euler_integration(se3ddd, self.dt)

        # Update output datapack
        # - tracker_pose_ref
        self._setDataPack("tracker_pose_ref", tracker_utils.get_pose_msg(self.tracker))
        # - tracker_velocity_ref
        self._setDataPack("tracker_velocity_ref", tracker_utils.get_twist_msg(self.tracker))

    def shutdown(self):
        self.tracker = None
        print(f"Engine {self._name} is shutting down")

    def reset(self):
        print(f"Engine {self._name} is resetting")
        self._load_configuration()
        self._initialize()
