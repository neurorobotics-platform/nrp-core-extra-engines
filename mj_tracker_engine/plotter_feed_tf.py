from nrp_core import *

@EngineDataPack(keyword='target_pose_datapack', id=DataPackIdentifier('target_pose', 'mj_se3_tracker'))
@EngineDataPack(keyword='tracker_pose_ref_datapack', id=DataPackIdentifier('tracker_pose_ref', 'mj_se3_tracker'))
@TransceiverFunction("plotter")
def transceiver_function(tracker_pose_ref_datapack, target_pose_datapack):

    tracker_pose_ref_datapack.engine_name = "plotter"
    target_pose_datapack.engine_name = "plotter"

    return [tracker_pose_ref_datapack, target_pose_datapack]
