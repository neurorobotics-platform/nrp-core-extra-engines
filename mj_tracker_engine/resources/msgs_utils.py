from typing import TypeVar, List

import google.protobuf.message
import numpy as np


def most_recent_msg(new_msg, old_msg):
    """
    :return: new_msg if new_msg is valid and different from old_msg
    """
    if (new_msg is None or
            new_msg is old_msg or
            not is_valid_msg(new_msg) or
            equals_msg(new_msg, old_msg)):
        return old_msg

    return new_msg


def is_message(msg):
    return issubclass(msg.__class__, google.protobuf.message.Message)


def equals_msg(msg, other_msg):
    """
    Compare messages for equality
    Repeated fields are not supported.
    Floating point fields are compared for with numpy.isclose
    """

    if type(msg) is not type(other_msg):
        return False

    # simple field
    if not is_message(msg):
        return msg == other_msg if not isinstance(msg, float) else np.isclose(msg, other_msg)

    # message
    return all((equals_msg(f, other_f)
                for (f, other_f) in ((getattr(msg, f.name), getattr(other_msg, f.name))
                                     for f in msg.DESCRIPTOR.fields)))


def is_valid_msg(msg):
    """
    :return: True if at least one msg field is set
    """
    return msg is not None and len(msg.ListFields()) > 0


def _is_floating_number_type(field):
    return field.type == field.TYPE_DOUBLE or field.type == field.TYPE_FLOAT


M = TypeVar("M", bound=google.protobuf.message.Message)


def allclose_msg(msg: M, other: M):
    assert type(msg) is type(other)

    for field in msg.DESCRIPTOR.fields:
        msg_field = getattr(msg, field.name)
        other_field = getattr(other, field.name)

        if any(not _is_floating_number_type(f) for f in msg_field.DESCRIPTOR.fields):
            raise ValueError("Wrong field type: not a floating point")

        if not np.allclose(_msg_to_float_ndarray(msg_field), _msg_to_float_ndarray(other_field)):
            return False
    else:
        return True


def fill_msg(msg: M, data_fields: List) -> M:
    if msg is None or data_fields is None:
        raise ValueError(f"fill_msg(): {'msg' if msg is None else 'data_fields'} is None")

    if len(msg_fields := msg.DESCRIPTOR.fields) != len(data_fields):
        raise ValueError(f"(len({msg.DESCRIPTOR.name}.fields) != {len(data_fields)}")

    for i, field in enumerate([f for f in msg_fields]):
        if field.type != field.TYPE_MESSAGE:
            # a field, fill it
            setattr(msg, field.name, data_fields[i])
        else:
            fill_msg(getattr(msg, field.name), data_fields[i])

    return msg


# msg_to_ndarray
def _msg_to_float_ndarray(msg: google.protobuf.message.Message, dtype=np.float64) -> np.ndarray:
    assert all([_is_floating_number_type(f) for f in msg.DESCRIPTOR.fields])

    return np.array([getattr(msg, f.name) for f in msg.DESCRIPTOR.fields], dtype=dtype)


point_to_ndarray = _msg_to_float_ndarray
quaternion_to_ndarray = _msg_to_float_ndarray
vector3_to_ndarray = _msg_to_float_ndarray
