import numpy as np
import mj_se3_tracker as mjt

# Initialize a tracker object and its state
# Initial pose
p0 = np.array((1, 1, 0))
# Note: quaternion should be normalized,
q0 = mjt.normalize(np.array((1, 0, 1, 0)))
# Init object
tracker = mjt.MJTracker(
    se3=mjt.SE3State(
        p0, q0
    ),  # default values of SE3State is pos:(0,0,0) orient:(1,0,0,0)
    se3d=mjt.SE3StateDerivative(),  # default values of SE3StateDerivative is vp:(0,0,0) vo:(0,0,0)
    se3dd=mjt.SE3StateDerivative(),
)

# MJTracker can also be initialized with defaults values, which sets its state
# to the position=(0,0,0), orientation=(1,0,0,0) and null velocity and acceleration
# tracker = mjt.MJTracker()

# Uncomment the following block of code to initialize a Clamped version of the tracker
# Works exactly in the same way as the regular tracker, but the values of
# the velocity and acceleration are clamped to the values specified at initialization
# tracker = mjt.ClampedMJTracker(
#     se3 =  mjt.SE3State(p0, q0),
#     se3d = mjt.SE3StateDerivative(),
#     se3dd= mjt.SE3StateDerivative(),
#     vp_norm=0.5, # max norm of linear velocity
#     vo_norm=0.3, # max norm of angular velocity
#     ap_norm=1.8, # max norm of linear acceleration
#     ao_norm=1.4, # max norm of angular acceleration
# )

# Change the current state of the tracker
# Position
tracker.se3.p = np.array((0, 3, 0))
# Orientation
tracker.se3.o = mjt.normalize(np.array((1, 0, 0, 1)))
# Linear Velocity
tracker.se3d.vp = np.array((1.2, 0, 0))
# Angular Velocity
tracker.se3d.vo = np.array((0, 0, 2))
# Linear Acceleration
tracker.se3dd.vp = np.array((0, 0, 0))
# Angular Acceleration
tracker.se3dd.vo = np.array((0, 0, 0.1))

### Target pose
p_des = np.array((0, 0, 0))
o_des = mjt.normalize(np.array((0, 0, 1, 0)))

### Set parameters for simulation
t = 0.0
# total time for simulation
tf = 2.0
# total time to reach the target
t_track = 1.5
# time step for integration
dt = 0.03

history = {"p": [], "o": [], "t": []}

### Simulate

np.set_printoptions(precision=3)

while t <= tf:
    # Save the current time and state, e.g. for plotting
    history["t"].append(t)
    history["p"].append(tracker.se3.p)
    history["o"].append(tracker.se3.o)

    # Update the time-to-go
    t_go = t_track - t

    # Compute the jerk to apply to the system, as a function of the target and the time-to-go.
    # NOTE: If the target is moving, at every time step pass the target's state at the current time
    se3ddd = tracker.compute_jerk(
        mjt.SE3State(p_des, o_des),  # target SE3 pose
        mjt.SE3StateDerivative(),  # target SE3 velocity, here left to 0 as the target is not moving
        mjt.SE3StateDerivative(),  # target SE3 acceleration, here left to 0 as the target is not moving
        mjt.SE3StateDerivative(),  # target SE3 jerk, here left to 0 as the target is not moving
        t_go,
        t_go_thresh=0.15,  # THRESHOLD OF THE TIME TO GO
    )

    # Step the system by applying the jerk and integrating the state
    tracker.euler_integration(se3ddd, dt)

    t += dt

    print(f"\n===========\nt: {t:.3f}")
    print(tracker)
