import matplotlib.pyplot as plt
import numpy as np

import mj_se3_tracker as mjt
from plotters import PlotterStaticTarget

### Initialize a tracker object and its state
# Initial pose
p0 = np.array((1, 1, 0))
# Note: quaternion should be normalized,
q0 = mjt.normalize(np.array((1, 0, 1, 0)))
# Init object
tracker = mjt.MJTracker(
    se3=mjt.SE3State(
        p0, q0
    ),  # default values of SE3State is pos:(0,0,0) orient:(1,0,0,0)
    se3d=mjt.SE3StateDerivative(),
    # velocities. default values of SE3StateDerivative is vp:(0,0,0) vo:(0,0,0)
    se3dd=mjt.SE3StateDerivative(),
    # accelerations. default values of SE3StateDerivative is vp:(0,0,0) vo:(0,0,0)
)

### Target
p_des = np.array((0, 0, 0))
o_des = mjt.normalize(np.array((0, 0, 1, 0)))
# Initialize target object
target = mjt.ThirdOrderSE3State(
    se3=mjt.SE3State(p_des, o_des),
)
# The target will move by following a random walk on the jerk.
# Here, init random generator
SEED = 1234
rng = np.random.default_rng(SEED)

### Set parameters for simulation
t = 0.0
# total time for simulation
tf = 2.2
# total time to reach the target
# (i.e. initial value of the time-to-go)
t_track = 2.0
# time step for integration
dt = 0.03

history = {"p": [], "o": [], "t": [], "ep": [], "eo": [], "p_des": [], "o_des": []}

### Simulate

np.set_printoptions(precision=3)

plotter = PlotterStaticTarget(
    0.0, tf, tracker.se3.p, tracker.se3.o, target.se3.p, target.se3.o
)

plt.show(block=False)

while t <= tf:
    # Save the current time and state, e.g. for plotting
    history["t"].append(t)
    history["p"].append(tracker.se3.p)
    history["o"].append(tracker.se3.o)
    history["ep"].append(np.linalg.norm(target.se3.p - tracker.se3.p))
    history["eo"].append(mjt.diq(target.se3.o, tracker.se3.o))
    history["p_des"].append(target.se3.p)
    history["o_des"].append(target.se3.o)

    plotter.update_data(
        {
            "t": np.array([t]),
            "p": tracker.se3.p,
            "o": tracker.se3.o,
            "p_des": target.se3.p,
            "o_des": target.se3.o,
        }
    )

    plotter.update_fig()
    plt.pause(dt)

    # Update the time-to-go
    t_go = t_track - t

    # Compute the jerk to apply to the system, as a function of the target and the time-to-go.
    # NOTE: If the target is moving, at every time step pass the target's state at the current time
    se3ddd = tracker.compute_jerk(
        target.se3,  # target SE3 pose
        target.se3d,  # target SE3 velocity
        target.se3dd,  # target SE3 acceleration
        target.se3ddd,  # target SE3 jerk
        t_go,
        t_go_thresh=0.15,  # THRESHOLD OF THE TIME TO GO
    )

    # Step the system by applying the jerk and integrating the state
    tracker.euler_integration(se3ddd, dt)

    # Set a random jerk for the target, to simulate a moving target
    target.se3ddd.vp = rng.normal(0, 10.0, 3)
    target.se3ddd.vo = rng.normal(0, 5.0, 3)
    # And integrate
    target.se3.euler_integration(target.se3d, dt)
    target.se3d.euler_integration(target.se3dd, dt)
    target.se3dd.euler_integration(target.se3ddd, dt)

    # Step the time
    t += dt

    print(f"\n===========\nt: {t:.3f}")
    print(tracker)

# Convert history to numpy array for easier plotting
for k in history.keys():
    history[k] = np.array(history[k])

plt.close(plotter.fig)

