""" Minimum-Jerk Tracker class and related classes

SecondOrderSE3State: class to store the state of a point in SE3 plus its derivatives up to the second order (acceleration).
ThirdOrderSE3State: class to store the state of a point in SE3 plus its derivatives up to the third order (jerk).

MJTracker:
    This class possess an internal ThirdOrderSE3State state that represents the current state of the trajectory that the tracker generates.
    At each time step:
    - given the state of the target to reach, the tracker computes the jerk to apply, to reach the target in a minimum-jerk fashion.
    - this jerk is then integrated to provide the next state of the trajectory.

Example:
    let's consider that the tracker is supposed to generate the trajectory for the end-effector of a robot so that it reaches a fixed target pose.
    In this case, the jerk to apply is computed by always passing the same state of the target, as it does not move.
    Moreover, the velocity, acceleration, and jerk of the target are zero.
            
    At each time step the tracker will evolve its state (via jerk computation + integration) so to reach the target.
    At each time step the state of the tracker can be passed to an Inverse Kinematics solver, so that the robot end-effector 
    follows a minimum-jerk trajectory to reach the target pose.
"""

import copy
import warnings

import numpy as np
from mj_se3_tracker.quaternion_ops import normalize, vec, mulq, conjugate, diq
from mj_se3_tracker.states import SE3State, SE3StateDerivative


class SecondOrderSE3State:
    """Class to store the state of a point in SE3 and its derivatives up to the second order (acceleration)."""

    def __init__(
            self,
            se3: SE3State = None,
            se3d: SE3StateDerivative = None,
            se3dd: SE3StateDerivative = None,
    ):
        # Init state
        # Position, orientation
        if se3 is None:
            self.__se3 = SE3State()
        else:
            # use property setter, so it also does type-checking
            self.se3 = se3

        # Velocity, linear and angular
        if se3d is None:
            self.__se3d = SE3StateDerivative()
        else:
            # use property setter, so it also does type-checking
            self.se3d = se3d

        # Acceleration, linear and angular
        if se3dd is None:
            self.__se3dd = SE3StateDerivative()
        else:
            # use property setter, so it also does type-checking
            self.se3dd = se3dd

    @property
    def se3(self):
        return self.__se3

    @se3.setter
    def se3(self, se3: SE3State):
        if isinstance(se3, SE3State):
            self.__se3 = se3.copy()
        else:
            raise TypeError

    @property
    def se3d(self):
        return self.__se3d

    @se3d.setter
    def se3d(self, se3d: SE3StateDerivative):
        if isinstance(se3d, SE3StateDerivative):
            self.__se3d = se3d.copy()
        else:
            raise TypeError

    @property
    def se3dd(self):
        return self.__se3dd

    @se3dd.setter
    def se3dd(self, se3dd: SE3StateDerivative):
        if isinstance(se3dd, SE3StateDerivative):
            self.__se3dd = se3dd.copy()
        else:
            raise TypeError

    def __repr__(self):
        return (
            f"SecondOrderSE3State (x: (p: "
            f"{self.__se3.p}"
            f" o: "
            f"{self.__se3.o}"
            f")\n"
            f"                    xd: (p: "
            f"{self.__se3d.vp}"
            f" o: "
            f"{self.__se3d.vo}"
            f")\n"
            f"                   xdd: (p: "
            f"{self.__se3dd.vp}"
            f" o: "
            f"{self.__se3dd.vo})"
        )

    def copy(self):
        return copy.copy(self)

    def deepcopy(self):
        return copy.deepcopy(self)


class ThirdOrderSE3State(SecondOrderSE3State):
    """Class to store the state of a point in SE3 and its derivatives up to the third order (jerk)."""

    def __init__(
            self,
            se3: SE3State = None,
            se3d: SE3StateDerivative = None,
            se3dd: SE3StateDerivative = None,
            se3ddd: SE3StateDerivative = None,
    ) -> None:
        super().__init__(se3, se3d, se3dd)
        # Jerk, linear and angular
        if se3ddd is None:
            self.__se3ddd = SE3StateDerivative()
        else:
            # use property setter, so it also does type-checking
            self.se3ddd = se3ddd

    @property
    def se3ddd(self):
        return self.__se3ddd

    @se3ddd.setter
    def se3ddd(self, se3ddd: SE3StateDerivative):
        if isinstance(se3ddd, SE3StateDerivative):
            self.__se3ddd = se3ddd.copy()
        else:
            raise TypeError

    def __repr__(self):
        return (
            "ThirdOrderSE3State (x: (p: "
            f"{self.se3.p}"
            f" o: "
            f"{self.se3.o}"
            f")\n"
            f"                   xd: (p: "
            f"{self.se3d.vp}"
            f" o: "
            f"{self.se3d.vo}"
            f")\n"
            f"                  xdd: (p: "
            f"{self.se3dd.vp}"
            f" o: "
            f"{self.se3dd.vo}"
            f")"
            f"                 xddd: (p: "
            f"{self.__se3ddd.vp}"
            f" o: "
            f"{self.__se3ddd.vo}"
            f")"
        )


class MJTracker(SecondOrderSE3State):
    """Tracker object, which has an internal state corresponding to the current state of the trajectory.

    At each time-step, the tracker methods should be used to compute and integrate the jerk to apply to the trajectory
    in order to reach some target in a minimum-jerk fashion.
    """

    def __init__(self, se3=None, se3d=None, se3dd=None) -> None:
        super().__init__(se3, se3d, se3dd)

    def __repr__(self):
        return (
            "MJTracker(x:   (p: "
            f"{self.se3.p}"
            f" o: "
            f"{self.se3.o}"
            f")\n"
            f"          xd:  (p: "
            f"{self.se3d.vp}"
            f" o: "
            f"{self.se3d.vo}"
            f")\n"
            f"          xdd: (p: "
            f"{self.se3dd.vp}"
            f" o: "
            f"{self.se3dd.vo}"
            f")"
        )

    def compute_jerk(
            self,
            se3_des: SE3State,
            se3d_des: SE3StateDerivative,
            se3dd_des: SE3StateDerivative,
            se3ddd_des: SE3StateDerivative,
            t_go: float,
            t_go_thresh: float = None,
    ):
        if t_go_thresh is None:
            t_go = np.clip(t_go, 0.2, np.inf)
        elif t_go_thresh <= 0.0:
            raise ValueError("The threshold of the time-to-go must be greater than 0")
        elif t_go_thresh < 0.1:
            warnings.warn(
                "The threshold of the time-to-go is low (<0.1), and can cause instability",
                UserWarning,
            )
        t_go = np.clip(t_go, t_go_thresh, np.inf)

        # position error
        e_p = se3_des.p - self.se3.p
        # TODO: check if it works in the same way by using e = se3 - se3_des
        # or if there's any problem due to quaternion sign or similar things
        # orientation error, properly scaled
        e_o = vec(mulq(se3_des.o, conjugate(self.se3.o)))
        scale = 1.0 / np.sinc(diq(self.se3.o, se3_des.o) / (2 * np.pi))
        e_o = scale * e_o

        # Store in SE3StateDerivative because we now just have R3 values for both orientation and position
        # and we can use the arithmetic implemented in that class for readability
        e = SE3StateDerivative(e_p, e_o)

        # higher derivatives of the error
        ed = se3d_des - self.se3d
        edd = se3dd_des - self.se3dd

        # jerk to be applied to the system for tracking
        return se3ddd_des + (
                (e * 60) / (t_go ** 3) + (36 * ed) / (t_go ** 2) + (9 * edd) / t_go
        )

    def euler_integration(self, se3ddd: SE3StateDerivative, dt: float):
        # order of euler integration from lowest order to highest order derivative
        # a change of jerk takes 3 time step to propagate to a change in position
        self.se3.euler_integration(self.se3d, dt)
        self.se3d.euler_integration(self.se3dd, dt)
        self.se3dd.euler_integration(se3ddd, dt)


class ClampedMJTracker(MJTracker):
    """Clamped Tracker. Equivalent to MJTracker but with limited velocity and acceleration
    Note that the limits can impose a different behaviour than the one expected from the minimum jerk trajectory.
    """

    def __init__(
            self,
            vp_norm: float = None,
            vo_norm: float = None,
            ap_norm: float = None,
            ao_norm: float = None,
            se3: SE3State = None,
            se3d: SE3StateDerivative = None,
            se3dd: SE3StateDerivative = None,
    ) -> None:
        super().__init__(se3, se3d, se3dd)
        self.__vp_norm = vp_norm
        self.__vo_norm = vo_norm
        self.__ap_norm = ap_norm
        self.__ao_norm = ao_norm

    @property
    def vp_norm(self) -> float:
        """Max norm of the linear velocity allowed for the system"""
        return self.__vp_norm

    @vp_norm.setter
    def vp_norm(self, value: float):
        if value >= 0.0:
            self.__vp_norm = value
        else:
            raise ValueError(
                "The max norm of linear velocity should be >= 0.0 Set to None to remove the limit."
            )

    @property
    def vo_norm(self) -> float:
        """Max norm of the angular velocity allowed for the system"""
        return self.__vo_norm

    @vo_norm.setter
    def vo_norm(self, value: float):
        if value >= 0.0:
            self.__vo_norm = value
        else:
            raise ValueError(
                "The max norm of angular velocity should be >= 0.0. Set to None to remove the limit."
            )

    @property
    def ap_norm(self) -> float:
        """Max norm of the acceleration allowed for the system"""
        return self.__ap_norm

    @ap_norm.setter
    def ap_norm(self, value: float):
        if value >= 0.0:
            self.__ap_norm = value
        else:
            raise ValueError(
                "The max norm of linear acceleration should be >= 0.0. Set to None to remove the limit."
            )

    @property
    def ao_norm(self) -> float:
        """Max norm of the acceleration allowed for the system"""
        return self.__ao_norm

    @ao_norm.setter
    def ao_norm(self, value: float):
        if value >= 0.0:
            self.__ao_norm = value
        else:
            raise ValueError(
                "The max norm of angular acceleration should be >= 0.0. Set to None to remove the limit."
            )

    def euler_integration(self, se3ddd: SE3StateDerivative, dt: float):
        # Order of euler integration from the lowest order to the highest order derivative.
        # Thus, a change of jerk takes 3 time step to propagate to a change in position

        self.se3.euler_integration(self.se3d, dt)
        self.se3d.euler_integration(self.se3dd, dt)
        self.se3dd.euler_integration(se3ddd, dt)

        # linear velocity clipping
        if not (self.__vp_norm is None):
            if np.linalg.norm(self.se3d.vp) > self.__vp_norm:
                self.se3d.vp = normalize(self.se3d.vp) * self.__vp_norm

        # angular velocity clipping
        if not (self.__vo_norm is None):
            if np.linalg.norm(self.se3d.vo) > self.__vo_norm:
                self.se3d.vo = normalize(self.se3d.vo) * self.__vo_norm

        # linear acceleration clipping
        if not (self.__ap_norm is None):
            if np.linalg.norm(self.se3dd.vp) > self.__ap_norm:
                self.se3dd.vp = normalize(self.se3dd.vp) * self.__ap_norm

        # angular acceleration clipping
        if not (self.__ao_norm is None):
            if np.linalg.norm(self.se3dd.vo) > self.__ao_norm:
                self.se3dd.vo = normalize(self.se3dd.vo) * self.__ao_norm
