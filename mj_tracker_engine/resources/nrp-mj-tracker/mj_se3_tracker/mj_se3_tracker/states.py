""" Module to store the state of a point in the SE3 space, i.e. the 6D space composed by 3D positions + 3D orientations. 

There are two classes:
    - SE3State: stores the position and orientation of a point in 3D. Quaternion is considered ordered as (w, x, y, z), where w is the scalar part.
    - SE3StateDerivative: stores the velocity and angular velocity of a point in 3D.

These two classes also provide methods to:
- combine them (subtraction and addition), e.g. useful to compute a SE3State as the difference between two SE3States.
- perform Euler integration (e.g. a quaternion can evolve only on the unitary R^4 sphere)
- access and assign their elements (e.g. a quaternion representing a rotation should always be of unitary norm (i.e. belong the the unitary R^4 sphere))
    
Note: the SE3StateDerivative class can be used to store any 6D vector in R^6, not necessarily the velocity of a point in SE3.
As an example, it can be used to store also the acceleration of a point in SE3.
"""
from __future__ import annotations

import copy

import numpy as np
from mj_se3_tracker.quaternion_ops import normalize, is_unitary, mulq, conjugate, vec_to_zquat, expq
from numpy.typing import ArrayLike


class SE3State:
    """Class to store position and orientation of a point in 3D. Quaternion is considered ordered as (w, x, y, z)"""

    def __init__(self, p: ArrayLike = None, o: ArrayLike = None) -> None:
        if p is None:
            # R3 position
            self.__p = np.zeros(3)
        else:
            self.p = p
        if o is None:
            # SO3 Quaternion for orientation. Scalar part is assumed to be the first element of the array.
            self.__o = np.array((1.0, 0.0, 0.0, 0.0))
        else:
            self.o = o

    @property
    def p(self) -> np.ndarray:
        return self.__p

    @p.setter
    def p(self, p):
        self.check_p(p)
        self.__p = np.array(p, dtype=np.float64)

    @property
    def o(self):
        return self.__o

    @o.setter
    def o(self, o):
        o = normalize(np.array(o, dtype=np.float64))
        self.check_o(o)
        self.__o = o

    def copy(self):
        return copy.copy(self)

    def deepcopy(self):
        return copy.deepcopy(self)

    def check_p(self, p):
        try:
            if len(p) == 3:
                pass
            else:
                raise ValueError("The position should be an iterable of length 3")
        except TypeError:
            raise TypeError("The position should be an iterable of length 3")

    def check_o(self, o):
        try:
            if len(o) == 4:
                if is_unitary(np.array(o)):
                    pass
                else:
                    raise ValueError(
                        f"The orientation should be an iterable of length 4, representing a (w,x,y,z) quaternion with unit norm\ninput had norm of{np.linalg.norm(np.array(o))}"
                    )
            else:
                raise ValueError(
                    "The orientation should be an iterable of length 4, representing a (w,x,y,z) quaternion with unit norm"
                )
        except TypeError:
            raise TypeError(
                "The orientation should be an iterable of length 4, representing a (w,x,y,z) quaternion with unit norm"
            )

    def __add__(self, se3):
        """Add another SE3 transform to this one.
        Positions are added linearly. The output quaternions considers first the rotation about self.o,
        and then the rotation around the added transform. This operation is not commutative.

        Args:
            se3 (SE3State): SE3 state to add

        Returns:
            SE3State
        """
        return SE3State(p=self.p + se3.p, o=mulq(se3.o, self.o))

    def __sub__(self, se3):
        """Subtract another SE3 transform to this one.
        Positions are subtracted linearly. The output quaternions considers first the rotation about self.o,
        and then the rotation around the conjugate of the transform being subtracted to this one. This operation is not commutative.

        Args:
            se3 (SE3State): SE3 state to subtract

        Returns:
            SE3State
        """
        return SE3State(p=self.p - se3.p, o=mulq(conjugate(se3.o), self.o))

    def __repr__(self):
        return "SE3State(p: " + str(self.__p) + ", o: " + str(self.__o) + ")"

    def __getitem__(self, t):
        # transform to a numpy.ndarray as
        # ((px,py,pz,NaN),(ow,ox,oy,oz))
        # and slice it
        return np.array(
            (
                (self.__p[0], self.__p[1], self.__p[2], np.NaN),
                (
                    self.__o[0],
                    self.__o[1],
                    self.__o[2],
                    self.__o[3],
                ),
            )
        )[t]

    def euler_integration(self, se3d: SE3StateDerivative, dt: float) -> None:
        """One-step Euler integration.
        Quaternion integration is based on the exponentiation of the angular speed,
        see https://www.ashwinnarayan.com/post/how-to-integrate-quaternions/

        Args:
            se3d (SE3StateDerivative): derivative of the SE3State to use for integration
            dt (float): time-step
        """
        self.__p = self.__p + dt * se3d.vp
        # integration with expq should preserve exactly unitary norm,
        # but still a normalize() is not a bad idea, to avoid rounding errors
        dq = expq(dt * vec_to_zquat(se3d.vo))
        self.__o = normalize(mulq(dq, self.__o))


class SE3StateDerivative:
    def __init__(self, vp=None, vo=None) -> None:
        if vp is None:
            self.__vp = np.zeros(3)
        else:
            self.check_vector(vp)
            self.__vp = vp
        if vo is None:
            self.__vo = np.zeros(3)
        else:
            self.check_vector(vo)
            self.__vo = vo

    @property
    def vp(self):
        return self.__vp

    @vp.setter
    def vp(self, vp):
        self.check_vector(vp)
        self.__vp = np.array(vp)

    @property
    def vo(self):
        return self.__vo

    @vo.setter
    def vo(self, vo):
        self.check_vector(vo)
        self.__vo = np.array(vo)

    def copy(self):
        return copy.copy(self)

    def deepcopy(self):
        return copy.deepcopy(self)

    def check_vector(self, v):
        try:
            if len(v) == 3:
                pass
            else:
                raise ValueError(
                    "The velocity (both linear and angular) should be an iterable of length 3"
                )
        except TypeError:
            raise TypeError(
                "The velocity (both linear and angular) should be an iterable of length 3"
            )

    def __add__(self, se3d):
        return SE3StateDerivative(vp=self.vp + se3d.vp, vo=self.vo + se3d.vo)

    def __sub__(self, se3d):
        return SE3StateDerivative(vp=self.vp - se3d.vp, vo=self.vo - se3d.vo)

    def __mul__(self, value):
        return SE3StateDerivative(vp=self.vp * value, vo=self.vo * value)

    def __rmul__(self, value):
        return SE3StateDerivative(vp=self.vp * value, vo=self.vo * value)

    def __truediv__(self, value):
        return SE3StateDerivative(vp=self.vp / value, vo=self.vo / value)

    def __rtruediv__(self, value):
        return SE3StateDerivative(vp=self.vp / value, vo=self.vo / value)

    def __repr__(self):
        return "SE3StateDerivative(p: " + str(self.__vp) + " o: " + str(self.__vo) + ")"

    def __getitem__(self, t):
        # transform to a numpy.ndarray as
        # ((vpx,vpy,vpz),(vox,voy,voz))
        # and slice it
        return np.array(
            (
                (self.__vp[0], self.__vp[1], self.__vp[2]),
                (self.__vo[0], self.__vo[1], self.__vo[2]),
            )
        )[t]

    def euler_integration(self, se3d, dt) -> None:
        """One-step Euler integration.

        Args:
            se3d (SE3StateDerivative): derivative of the SE3StateDerivative to use for integration
            dt (float): time-step
        """
        self.__vp = self.__vp + dt * se3d.vp
        self.__vo = self.__vo + dt * se3d.vo
