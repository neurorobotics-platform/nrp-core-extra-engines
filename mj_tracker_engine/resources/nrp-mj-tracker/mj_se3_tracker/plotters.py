import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from matplotlib.gridspec import GridSpec
from mpl_toolkits.mplot3d import Axes3D

import mj_se3_tracker as mjt


class PlotterStaticTarget:
    def __init__(
            self,
            t0: float,
            tf: float,
            p0: np.ndarray,
            o0: np.ndarray,
            p_des: np.ndarray,
            o_des: np.ndarray,
            max_stored_timesteps: int = 1000,
    ) -> None:
        self.plt = plt
        self.__max_stored_timesteps = max_stored_timesteps

        ### Setup the figure
        self.fig = plt.figure(figsize=plt.figaspect(0.6))
        self.fig.canvas.manager.set_window_title('Minimum Jerk Tracker Plotting Tool')
        self.fig.suptitle("Convergence to Target")
        # Increase vertical space between the two 2D plots
        self.fig.subplots_adjust(hspace=0.5)
        # Grid spec for the layout of the subplots
        gs = GridSpec(2, 2, figure=self.fig)
        # Add subplots and store their axis in a dictionary for easier retrieval
        self.axs_dict = {
            "ep": self.fig.add_subplot(gs[0, 0]),
            "eo": self.fig.add_subplot(gs[1, 0]),
            "3d": self.fig.add_subplot(gs[:, 1], projection="3d"),
        }

        # Set axis labels and titles
        self.axs_dict["ep"].set(xlabel="Time [s]", ylabel="Lin. Distance")
        self.axs_dict["ep"].title.set_text("Linear distance to target")

        self.axs_dict["eo"].set(xlabel="Time [s]", ylabel="Ang. Distance [rad]")
        self.axs_dict["eo"].title.set_text("Angular distance to target")

        self.axs_dict["3d"].set_title("3D trajectory")

        ### Init attribute that will contain the data to be plotted at every update of the figure
        self.plotting_data = {
            "t": np.array([t0]).reshape(
                1,
            ),  # dim=(time-steps stored,)
            "ep": np.linalg.norm(p_des - p0).reshape(
                1,
            ),  # dim=(time-steps stored,)
            "eo": mjt.diq(o_des, o0).reshape(
                1,
            ),  # dim=(time-steps stored,)
            "p": p0,  # dim=(3,)
            "p_des": p_des,  # dim=(3,)
            "R": mjt.q_to_R(o0),  # dim=(3,3)
            "R_des": mjt.q_to_R(o_des),  # dim=(3,3)
        }

        ### Set limits for the x axis of the 2d plots, and for all axis of 3d
        # limits for 2D
        self.axs_dict["ep"].set_xlim(t0, tf)
        self.axs_dict["ep"].set_ylim(0.0, 1.1 * self.plotting_data["ep"].max())
        self.axs_dict["eo"].set_xlim(t0, tf)
        self.axs_dict["eo"].set_ylim(0.0, 1.1 * self.plotting_data["eo"].max())

        # limits for 3D
        # NOTE 1: to properly display the same scale for each axis, the ranges should be all equal on three axis
        # NOTE 2: self.range_max is kept stored, because it will be used to determine the length
        #         of the lines representing the orientation of the reference frames
        self.range_max = np.abs(p0 - p_des).max()
        min_3d = np.minimum(p0, p_des)

        self.axs_dict["3d"].set_xlim3d(
            min_3d[0] - self.range_max * 0.1, min_3d[0] + self.range_max * 1.1
        )
        self.axs_dict["3d"].set_ylim3d(
            min_3d[1] - self.range_max * 0.1, min_3d[1] + self.range_max * 1.1
        )
        self.axs_dict["3d"].set_zlim3d(
            min_3d[2] - self.range_max * 0.1, min_3d[2] + self.range_max * 1.1
        )

        ### Show figure, and store the cleaned background for later update with blitting
        plt.show(block=False)
        plt.pause(0.1)
        self.clean_background = self.fig.canvas.copy_from_bbox(self.fig.bbox)

        ### Plot the initial configuration

        # while saving the actors, to later update the fig
        self.__actors_dict = {
            "ep": None,
            "eo": None,
            # one actor for the position, plus one for each quiver/arrow of the reference frame
            "3d": {"R": {"x": None, "y": None, "z": None}, "p": None},
            # one actor for the position, plus one for each quiver/arrow of the reference frame
            "3d_des": {"R": {"x": None, "y": None, "z": None}, "p": None},
        }

        # Plot linear distance to target
        (self.__actors_dict["ep"],) = self.axs_dict["ep"].plot(
            self.plotting_data["t"],
            self.plotting_data["ep"],
            label="Linear distance to target",
            animated=True,
        )

        # Plot angular distance to target
        (self.__actors_dict["eo"],) = self.axs_dict["eo"].plot(
            self.plotting_data["t"],
            self.plotting_data["eo"],
            label="Angular distance to target",
            animated=True,
        )

        # Plot 3D x-y-z lines representing the axis of the target and tracker reference frames
        # NOTE: one line/actor for each axis of the reference frame
        for i, (k, c) in enumerate(zip(["x", "y", "z"], ["r", "g", "b"])):
            (self.__actors_dict["3d"]["R"][k],) = self.axs_dict["3d"].plot(
                xs=[
                    self.plotting_data["p"][0],
                    self.plotting_data["p"][0] + self.plotting_data["R"][
                        i, 0] * self.range_max * 0.2,
                ],
                ys=[
                    self.plotting_data["p"][1],
                    self.plotting_data["p"][1] + self.plotting_data["R"][
                        i, 1] * self.range_max * 0.2,
                ],
                zs=[
                    self.plotting_data["p"][2],
                    self.plotting_data["p"][2] + self.plotting_data["R"][
                        i, 2] * self.range_max * 0.2,
                ],
                color=c,
                animated=True,
            )
            (self.__actors_dict["3d_des"]["R"][k],) = self.axs_dict["3d"].plot(
                xs=[
                    self.plotting_data["p_des"][0],
                    self.plotting_data["p_des"][0]
                    + self.plotting_data["R_des"][i, 0] * self.range_max * 0.2,
                ],
                ys=[
                    self.plotting_data["p_des"][1],
                    self.plotting_data["p_des"][1]
                    + self.plotting_data["R_des"][i, 1] * self.range_max * 0.2,
                ],
                zs=[
                    self.plotting_data["p_des"][2],
                    self.plotting_data["p_des"][2]
                    + self.plotting_data["R_des"][i, 2] * self.range_max * 0.2,
                ],
                color=c,
                animated=True,
            )
        # Plot 3D position of target and tracker
        (self.__actors_dict["3d"]["p"],) = self.axs_dict["3d"].plot(
            xs=self.plotting_data["p"][0],
            ys=self.plotting_data["p"][1],
            zs=self.plotting_data["p"][2],
            label="Tracker Trajectory",
            c="green",
            linestyle="",
            marker="o",
            animated=True,
        )
        (self.__actors_dict["3d_des"]["p"],) = self.axs_dict["3d"].plot(
            xs=self.plotting_data["p_des"][0],
            ys=self.plotting_data["p_des"][1],
            zs=self.plotting_data["p_des"][2],
            label="Target Trajectory",
            c="red",
            linestyle="",
            marker="o",
            animated=True,
        )

        ### Draw the animated artists
        self.draw_artists()
        # show the result to the screen, this pushes the updated RGBA buffer from the
        # renderer to the GUI framework, so you can see it
        self.fig.canvas.blit(self.fig.bbox)

    @property
    def actors_dict(self):
        return self.__actors_dict

    @property
    def max_stored_timesteps(self):
        return self.__max_stored_timesteps

    def draw_artists(self):
        self.axs_dict["ep"].draw_artist(self.__actors_dict["ep"])
        self.axs_dict["eo"].draw_artist(self.__actors_dict["eo"])
        self.axs_dict["3d"].draw_artist(self.__actors_dict["3d"]["p"])
        self.axs_dict["3d"].draw_artist(self.__actors_dict["3d_des"]["p"])
        for k in ["x", "y", "z"]:
            self.axs_dict["3d"].draw_artist(self.__actors_dict["3d"]["R"][k])
            self.axs_dict["3d"].draw_artist(self.__actors_dict["3d_des"]["R"][k])

    def update_data(self, datum) -> None:
        """Update the data stored for plotting with provided datum"""

        # add datum to self.plotting_data
        # - concatenate the time.
        self.plotting_data["t"] = np.append(self.plotting_data["t"],
                                            datum["t"].reshape(1),
                                            axis=0)
        # - concatenate the linear distance to target
        ep = np.linalg.norm(datum["p_des"] - datum["p"]).reshape(1)
        self.plotting_data["ep"] = np.append(self.plotting_data["ep"], ep, axis=0)
        # - concatenate the angular distance to target
        eo = mjt.diq(datum["o_des"], datum["o"]).reshape(1)
        self.plotting_data["eo"] = np.append(self.plotting_data["eo"], eo, axis=0)
        # - update position of tracker and target
        self.plotting_data["p"] = datum["p"]
        self.plotting_data["p_des"] = datum["p_des"]
        # - update rotation matrix of tracker and target
        self.plotting_data["R"] = mjt.q_to_R(datum["o"])
        self.plotting_data["R_des"] = mjt.q_to_R(datum["o_des"])

        ### if the amount of elements stored exceed the maximum configured size,
        # remove the oldest ones (which are at the start of the array)
        if self.plotting_data["t"].shape[0] > self.max_stored_timesteps:
            # remove the oldest elements
            self.plotting_data["t"] = self.plotting_data["t"][
                (self.plotting_data["t"].shape[0] - self.max_stored_timesteps)]
            self.plotting_data["ep"] = self.plotting_data["ep"][
                (self.plotting_data["ep"].shape[0] - self.max_stored_timesteps)]
            self.plotting_data["eo"] = self.plotting_data["eo"][
                (self.plotting_data["eo"].shape[0] - self.max_stored_timesteps)]

    def update_actors(self) -> None:
        """Update the figure by updating the data relative to each actor."""
        # Update linear distance to target
        self.__actors_dict["ep"].set_data(
            self.plotting_data["t"], self.plotting_data["ep"]
        )
        # Update angular distance to target
        self.__actors_dict["eo"].set_data(
            self.plotting_data["t"], self.plotting_data["eo"]
        )
        # Update x-y-z lines of tracker and target reference frames
        for i, k in enumerate(["x", "y", "z"]):
            self.__actors_dict["3d"]["R"][k].set_data_3d(
                [
                    self.plotting_data["p"][0],
                    self.plotting_data["p"][0]
                    + self.plotting_data["R"][i, 0] * self.range_max * 0.2,
                ],
                [
                    self.plotting_data["p"][1],
                    self.plotting_data["p"][1]
                    + self.plotting_data["R"][i, 1] * self.range_max * 0.2,
                ],
                [
                    self.plotting_data["p"][2],
                    self.plotting_data["p"][2]
                    + self.plotting_data["R"][i, 2] * self.range_max * 0.2,
                ],
            )
            self.__actors_dict["3d_des"]["R"][k].set_data_3d(
                [
                    self.plotting_data["p_des"][0],
                    self.plotting_data["p_des"][0]
                    + self.plotting_data["R_des"][i, 0] * self.range_max * 0.2,
                ],
                [
                    self.plotting_data["p_des"][1],
                    self.plotting_data["p_des"][1]
                    + self.plotting_data["R_des"][i, 1] * self.range_max * 0.2,
                ],
                [
                    self.plotting_data["p_des"][2],
                    self.plotting_data["p_des"][2]
                    + self.plotting_data["R_des"][i, 2] * self.range_max * 0.2,
                ],
            )
        # Update 3D position of target and tracker
        self.__actors_dict["3d"]["p"].set_data_3d(
            [
                self.plotting_data["p"][0],
            ],
            [
                self.plotting_data["p"][1],
            ],
            [
                self.plotting_data["p"][2],
            ],
        )
        self.__actors_dict["3d_des"]["p"].set_data_3d(
            [
                self.plotting_data["p_des"][0],
            ],
            [
                self.plotting_data["p_des"][1],
            ],
            [
                self.plotting_data["p_des"][2],
            ],
        )

    def update_fig(self):
        self.fig.canvas.restore_region(self.clean_background)

        # update artists
        self.update_actors()

        # redraw artists after update
        self.draw_artists()

        self.fig.canvas.blit(self.fig.bbox)
        self.fig.canvas.flush_events()

    def close(self):
        plt.close(self.fig)

