import mj_se3_tracker as mjt
import msgs_utils

from nrpgeometrymsgs_pb2 import Pose, Twist, Accel


def get_pose_msg(tracker: mjt.MJTracker) -> Pose:
    return msgs_utils.fill_msg(Pose(),
                               [tracker.se3.p, tracker.se3.o])


def get_twist_msg(tracker: mjt.MJTracker) -> Twist:
    return msgs_utils.fill_msg(Twist(),
                               [tracker.se3d.vp, tracker.se3d.vo])


def get_accel_msg(tracker: mjt.MJTracker) -> Accel:
    return msgs_utils.fill_msg(Accel(),
                               [tracker.se3dd.ap, tracker.se3dd.ao])
